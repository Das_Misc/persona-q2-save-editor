﻿using System.ComponentModel;

namespace Persona_Q2_Save_Editor.Models
{
    public class Character : INotifyPropertyChanged
    {
        private short id, maxHP, maxSP, skill1, skill2, skill3, skill4, skill5, skill6;
        private byte ag, en, lu, ma, st;
        private string baseName, lastName;
        private int currentHP, currentSP, exp, level;
        private Item equippedAccessory, equippedArmour, equippedWeapon;
        private Persona persona, subPersona;
        private bool subPersonaEquipped;

        public byte Ag
        {
            get { return ag; }

            set
            {
                if (ag == value) return;
                ag = value;
                NotifyPropertyChanged(nameof(Ag));
            }
        }

        public string BaseName
        {
            get { return baseName; }
            set { baseName = value; }
        }

        public int CurrentHP
        {
            get { return currentHP; }
            set
            {
                if (currentHP == value) return;
                currentHP = value;
                NotifyPropertyChanged(nameof(CurrentHP));
            }
        }

        public int CurrentSP
        {
            get { return currentSP; }
            set
            {
                if (currentSP == value) return;
                currentSP = value;
                NotifyPropertyChanged(nameof(CurrentSP));
            }
        }

        public byte En
        {
            get { return en; }

            set
            {
                if (en == value) return;
                en = value;
                NotifyPropertyChanged(nameof(En));
            }
        }

        public Item EquippedAccessory
        {
            get { return equippedAccessory; }
            set { equippedAccessory = value; }
        }

        public Item EquippedArmour
        {
            get { return equippedArmour; }
            set { equippedArmour = value; }
        }

        public Item EquippedWeapon
        {
            get { return equippedWeapon; }
            set { equippedWeapon = value; }
        }

        public int Exp
        {
            get { return exp; }
            set
            {
                if (exp == value) return;
                exp = value;
                NotifyPropertyChanged(nameof(Exp));
            }
        }

        public short Id { get { return id; } }

        public string Image
        {
            get
            {
                string file;

                if (id == 0)
                    file = "P4";
                else if (id == 1)
                    file = "P3M";
                else if (id == 18)
                    file = "P3F";
                else if (id == 19)
                    file = "P5";
                else
                    file = Lists.Characters[id].Replace(" ", "_");

                return "/Resources/Characters/" + file + ".png";
            }
        }

        public string LastName
        {
            get { return lastName; }
            set { lastName = value; }
        }

        public int Level
        {
            get { return level; }

            set
            {
                if (level == value) return;
                level = value;
                NotifyPropertyChanged(nameof(Level));
            }
        }

        public byte Lu
        {
            get { return lu; }

            set
            {
                if (lu == value) return;
                lu = value;
                NotifyPropertyChanged(nameof(Lu));
            }
        }

        public byte Ma
        {
            get { return ma; }

            set
            {
                if (ma == value) return;
                ma = value;
                NotifyPropertyChanged(nameof(Ma));
            }
        }

        public short MaxHP
        {
            get { return maxHP; }

            set
            {
                if (maxHP == value) return;
                maxHP = value;
                NotifyPropertyChanged(nameof(MaxHP));
            }
        }

        public short MaxSP
        {
            get { return maxSP; }

            set
            {
                if (maxSP == value) return;
                maxSP = value;
                NotifyPropertyChanged(nameof(MaxSP));
            }
        }

        public string Name { get { return Lists.Characters[id]; } }

        public Persona Persona
        {
            get { return persona; }
            set
            {
                if (persona == value) return;
                persona = value;
                NotifyPropertyChanged(nameof(Persona));
            }
        }

        public short Skill1
        {
            get { return skill1; }
            set
            {
                skill1 = value;
                NotifyPropertyChanged(nameof(Skill1));
            }
        }

        public short Skill2
        {
            get { return skill2; }
            set
            {
                skill2 = value;
                NotifyPropertyChanged(nameof(Skill2));
            }
        }

        public short Skill3
        {
            get { return skill3; }
            set
            {
                skill3 = value;
                NotifyPropertyChanged(nameof(Skill3));
            }
        }

        public short Skill4
        {
            get { return skill4; }
            set
            {
                skill4 = value;
                NotifyPropertyChanged(nameof(Skill4));
            }
        }

        public short Skill5
        {
            get { return skill5; }
            set
            {
                skill5 = value;
                NotifyPropertyChanged(nameof(Skill5));
            }
        }

        public short Skill6
        {
            get { return skill6; }
            set
            {
                skill6 = value;
                NotifyPropertyChanged(nameof(Skill6));
            }
        }

        public byte St
        {
            get { return st; }

            set
            {
                if (st == value) return;
                st = value;
                NotifyPropertyChanged(nameof(St));
            }
        }

        public Persona SubPersona
        {
            get { return subPersona; }
            set
            {
                if (subPersona == value) return;
                subPersona = value;
                NotifyPropertyChanged(nameof(SubPersona));
            }
        }

        public bool SubPersonaEquipped
        {
            get { return subPersonaEquipped; }
            set
            {
                if (subPersonaEquipped == value) return;
                subPersonaEquipped = value;
                NotifyPropertyChanged(nameof(subPersonaEquipped));
            }
        }

        public Character(short id)
        {
            this.id = id;
        }

        #region INotifyPropertyChanged implementation

        public event PropertyChangedEventHandler PropertyChanged;

        public void NotifyPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion INotifyPropertyChanged implementation
    }
}