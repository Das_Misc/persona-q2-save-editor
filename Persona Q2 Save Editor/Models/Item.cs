﻿using System.ComponentModel;

namespace Persona_Q2_Save_Editor.Models
{
    public class Item : INotifyPropertyChanged
    {
        private static Item[] list;
        private string description, name;
        private int id;

        public static Item[] List
        {
            get { return list; }
            set
            {
                list = value;
                foreach (var item in list)
                    item.UpdateData();
            }
        }

        public string Description
        {
            get { return description; }
        }

        public int ID
        {
            get { return id; }
            set
            {
                if (id == value) return;
                id = value;
                UpdateData();
            }
        }

        public string Name
        {
            get { return name; }
        }

        internal Item(int id, string name, string description)
        {
            this.id = id;
            this.name = name;
            this.description = description.Replace("/P4/", Lists.Characters[0]).Replace("/P3M/", Lists.Characters[1])
                .Replace("/P3F/", Lists.Characters[18]).Replace("/P5/", Lists.Characters[19]);
        }

        private void UpdateData()
        {
            var itemData = Lists.GetItemData(id);
            name = itemData.Item1;
            description = itemData.Item2.Replace("/P4/", Lists.Characters[0]).Replace("/P3M/", Lists.Characters[1])
                .Replace("/P3F/", Lists.Characters[18]).Replace("/P5/", Lists.Characters[19]);

            NotifyPropertyChanged(nameof(ID));
            NotifyPropertyChanged(nameof(Name));
            NotifyPropertyChanged(nameof(Description));
        }

        #region INotifyPropertyChanged implementation

        public event PropertyChangedEventHandler PropertyChanged;

        protected void NotifyPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion INotifyPropertyChanged implementation
    }
}
