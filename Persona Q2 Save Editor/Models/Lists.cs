﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Persona_Q2_Save_Editor.Models
{
    internal class Lists
    {
        #region Characters

        internal static string[] Characters =
        {
            "P4",
            "P3M",
            "Yosuke Hanamura",
            "Chie Satonaka",
            "Yukiko Amagi",
            "Rise Kujikawa",
            "Kanji Tatsumi",
            "Naoto Shirogane",
            "Teddie",
            "Yukari Takeba",
            "Junpei Iori",
            "Akihiko Sanada",
            "Mitsuru Kirijo",
            "Fuuka Yamagishi",
            "Aigis",
            "Ken Amada",
            "Koromaru",
            "Shinjiro Aragaki",
            "P3F",
            "P5",
            "Ryuji Sakamoto",
            "Ann Takamaki",
            "Morgana",
            "Yusuke Kitagawa",
            "Makoto Niijima",
            "Futaba Sakura",
            "Haru Okumura",
            "Goro Akechi"
        };

        #endregion Characters

        #region Personas

        #region PersonasChariot

        internal static Dictionary<int, Tuple<string, string>> PersonasChariot = new Dictionary<int, Tuple<string, string>>()
        {
            { 0, new Tuple<string, string>("EMPTY", "") },

            { 0x27, new Tuple<string, string>("Mezuki", "Chariot") },
            { 0x2C, new Tuple<string, string>("Kingu", "Chariot") },
            { 0x3F, new Tuple<string, string>("Nata Taishi", "Chariot") },
            { 0x40, new Tuple<string, string>("Chimera", "Chariot") },
            { 0x41, new Tuple<string, string>("Eligor", "Chariot") },
            { 0x44, new Tuple<string, string>("Oumitsunu", "Chariot") },
            { 0x45, new Tuple<string, string>("Triglav", "Chariot") },
            { 0x46, new Tuple<string, string>("Thor", "Chariot") },
            { 0x48, new Tuple<string, string>("Atavaka", "Chariot") },
            { 0x49, new Tuple<string, string>("Futsunushi", "Chariot") },
            { 0x7B, new Tuple<string, string>("Pale Rider", "Chariot") },
            { 0x9B, new Tuple<string, string>("Kartikeya", "Chariot") },
            { 0xCA, new Tuple<string, string>("Yoshitsune", "Chariot") },
            { 0xCB, new Tuple<string, string>("Helel", "Chariot") },
            { 0xD9, new Tuple<string, string>("Haraedo-no-Okami", "Chariot") },
            { 0xF6, new Tuple<string, string>("Tomoe", "Chariot") },
            { 0xF7, new Tuple<string, string>("Suzuka Gongen", "Chariot") },
            { 0x10C, new Tuple<string, string>("Palladion", "Chariot") },
            { 0x10D, new Tuple<string, string>("Athena", "Chariot") },
            { 0x118, new Tuple<string, string>("Captain Kidd", "Chariot") },
            { 0x119, new Tuple<string, string>("Seiten Taisei", "Chariot") },
        };

        #endregion PersonasChariot

        #region PersonasDeath

        internal static Dictionary<int, Tuple<string, string>> PersonasDeath = new Dictionary<int, Tuple<string, string>>()
        {
            { 0, new Tuple<string, string>("EMPTY", "") },

            { 0x33, new Tuple<string, string>("Flauros", "Death") },
            { 0x5A, new Tuple<string, string>("Empusa", "Death") },
            { 0x77, new Tuple<string, string>("Matador", "Death") },
            { 0x78, new Tuple<string, string>("White Rider", "Death") },
            { 0x7A, new Tuple<string, string>("Mot", "Death") },
            { 0x7C, new Tuple<string, string>("Alice", "Death") },
            { 0x7D, new Tuple<string, string>("Mahakala", "Death") },
            { 0x87, new Tuple<string, string>("Incubus", "Death") },
            { 0x88, new Tuple<string, string>("Succubus", "Death") },
            { 0x89, new Tuple<string, string>("Lilith", "Death") },
            { 0x8A, new Tuple<string, string>("Belphegor", "Death") },
            { 0x8B, new Tuple<string, string>("Belial", "Death") },
            { 0x8C, new Tuple<string, string>("Astaroth", "Death") },
            { 0x8D, new Tuple<string, string>("Beelzebub", "Death") },
            { 0xAA, new Tuple<string, string>("Yatagarasu", "Death") },
            { 0xB0, new Tuple<string, string>("Red Rider", "Death") },
            { 0xD0, new Tuple<string, string>("Thanatos", "Death") },
            { 0x172, new Tuple<string, string>("Bugs", "Death") },
            { 0x175, new Tuple<string, string>("Balor", "Death") },
            { 0x184, new Tuple<string, string>("Thanatos Picaro", "Death") },
        };

        #endregion PersonasDeath

        #region PersonasDevil

        internal static Dictionary<int, Tuple<string, string>> PersonasDevil = new Dictionary<int, Tuple<string, string>>()
        {
            { 0, new Tuple<string, string>("EMPTY", "") },

            { 0x4, new Tuple<string, string>("Legion", "Devil") },
            { 0x38, new Tuple<string, string>("Alp", "Devil") },
            { 0x62, new Tuple<string, string>("Pazuzu", "Devil") },
            { 0x63, new Tuple<string, string>("Sandman", "Devil") },
            { 0xBE, new Tuple<string, string>("Turdak", "Devil") },
            { 0xDD, new Tuple<string, string>("Legion", "Devil") },
            { 0xDE, new Tuple<string, string>("Masakado", "Devil") },
            { 0xDF, new Tuple<string, string>("Alice", "Devil") },
            { 0x16B, new Tuple<string, string>("Baphomet", "Devil") },
            { 0x176, new Tuple<string, string>("Vouivre", "Devil") },
        };

        #endregion PersonasDevil

        #region PersonasEmperor

        internal static Dictionary<int, Tuple<string, string>> PersonasEmperor = new Dictionary<int, Tuple<string, string>>()
        {
            { 0, new Tuple<string, string>("EMPTY", "") },

            { 0x24, new Tuple<string, string>("Oberon", "Emperor") },
            { 0x25, new Tuple<string, string>("King Frost", "Emperor") },
            { 0x28, new Tuple<string, string>("Oukuninushi", "Emperor") },
            { 0x29, new Tuple<string, string>("Thoth", "Emperor") },
            { 0x2D, new Tuple<string, string>("Barong", "Emperor") },
            { 0x2E, new Tuple<string, string>("Odin", "Emperor") },
            { 0x2F, new Tuple<string, string>("Anzu", "Emperor") },
            { 0x4B, new Tuple<string, string>("Power", "Emperor") },
            { 0x99, new Tuple<string, string>("Ganesha", "Emperor") },
            { 0xC5, new Tuple<string, string>("Cu Chulainn", "Emperor") },
            { 0xD7, new Tuple<string, string>("Takeji Zaiten", "Emperor") },
            { 0xFC, new Tuple<string, string>("Take-Mikazuchi", "Emperor") },
            { 0xFD, new Tuple<string, string>("Rokuten Maou", "Emperor") },
            { 0x106, new Tuple<string, string>("Polydeuces", "Emperor") },
            { 0x107, new Tuple<string, string>("Caesar", "Emperor") },
            { 0x11E, new Tuple<string, string>("Goemon", "Emperor") },
            { 0x11F, new Tuple<string, string>("Kamu Susano-o", "Emperor") },
            { 0x17B, new Tuple<string, string>("Baal", "Emperor") },
            { 0x181, new Tuple<string, string>("Huang Di", "Emperor") },
        };

        #endregion PersonasEmperor

        #region PersonasEmpress

        internal static Dictionary<int, Tuple<string, string>> PersonasEmpress = new Dictionary<int, Tuple<string, string>>()
        {
            { 0, new Tuple<string, string>("EMPTY", "") },

            { 0x3, new Tuple<string, string>("Ame-no-Uzume", "Empress") },
            { 0x19, new Tuple<string, string>("Kikuri-Hime", "Empress") },
            { 0x1D, new Tuple<string, string>("Yaksini", "Empress") },
            { 0x1E, new Tuple<string, string>("Titania", "Empress") },
            { 0x21, new Tuple<string, string>("Skadi", "Empress") },
            { 0x22, new Tuple<string, string>("Mother Harlot", "Empress") },
            { 0x23, new Tuple<string, string>("Laksmi", "Empress") },
            { 0x6A, new Tuple<string, string>("Kali", "Empress") },
            { 0xBC, new Tuple<string, string>("Alilat", "Empress") },
            { 0xC3, new Tuple<string, string>("Siegfried", "Empress") },
            { 0xC4, new Tuple<string, string>("Oberon", "Empress") },
            { 0x108, new Tuple<string, string>("Penthesilea", "Empress") },
            { 0x109, new Tuple<string, string>("Artemisia", "Empress") },
            { 0x124, new Tuple<string, string>("Milady", "Empress") },
            { 0x125, new Tuple<string, string>("Astarte", "Empress") },
            { 0x163, new Tuple<string, string>("Mayahuel", "Empress") },
            { 0x17C, new Tuple<string, string>("Xi Wangmu", "Empress") },
        };

        #endregion PersonasEmpress

        #region PersonasFool

        internal static Dictionary<int, Tuple<string, string>> PersonasFool = new Dictionary<int, Tuple<string, string>>()
        {
            { 0, new Tuple<string, string>("EMPTY", "") },

            { 0x2, new Tuple<string, string>("Slime", "Fool") },
            { 0x5, new Tuple<string, string>("Ose", "Fool") },
            { 0x6, new Tuple<string, string>("Ose", "Fool") },
            { 0x7, new Tuple<string, string>("Decarabia", "Fool") },
            { 0x8, new Tuple<string, string>("Shiki-Ouji", "Fool") },
            { 0x53, new Tuple<string, string>("Mothman", "Fool") },
            { 0x79, new Tuple<string, string>("Samael", "Fool") },
            { 0xB7, new Tuple<string, string>("Ardha", "Fool") },
            { 0xD3, new Tuple<string, string>("Orpheus Telos", "Fool") },
            { 0xF0, new Tuple<string, string>("Izanagi", "Fool") },
            { 0xF2, new Tuple<string, string>("Orpheus", "Fool") },
            { 0xF3, new Tuple<string, string>("Orpheus Telos", "Fool") },
            { 0x114, new Tuple<string, string>("Orpheus (improved?)", "Fool") },
            { 0x116, new Tuple<string, string>("Arsene", "Fool") },
            { 0x117, new Tuple<string, string>("Satanael", "Fool") },
            { 0x15B, new Tuple<string, string>("Bifrons", "Fool") },
            { 0x16E, new Tuple<string, string>("Bai Suzhen", "Fool") },
            { 0x182, new Tuple<string, string>("Orpheus Picaro", "Fool") },
            { 0x185, new Tuple<string, string>("Izanagi Picaro", "Fool") },
        };

        #endregion PersonasFool

        #region PersonasFortune

        internal static Dictionary<int, Tuple<string, string>> PersonasFortune = new Dictionary<int, Tuple<string, string>>()
        {
            { 0, new Tuple<string, string>("EMPTY", "") },

            { 0x2B, new Tuple<string, string>("Pabilsag", "Fortune") },
            { 0x5C, new Tuple<string, string>("Fortuna", "Fortune") },
            { 0x5D, new Tuple<string, string>("Clotho", "Fortune") },
            { 0x5E, new Tuple<string, string>("Lachesis", "Fortune") },
            { 0x5F, new Tuple<string, string>("Ananta", "Fortune") },
            { 0x60, new Tuple<string, string>("Atropos", "Fortune") },
            { 0x61, new Tuple<string, string>("Norn", "Fortune") },
            { 0xDA, new Tuple<string, string>("Yamato Sumeragi", "Fortune") },
            { 0xFE, new Tuple<string, string>("Sukuna-Hikona", "Fortune") },
            { 0xFF, new Tuple<string, string>("Yamato Takeru", "Fortune") },
            { 0x154, new Tuple<string, string>("Mandrake", "Fortune") },
            { 0x157, new Tuple<string, string>("Kelpie", "Fortune") },
            { 0x15C, new Tuple<string, string>("Yuki Jyorou", "Fortune") },
            { 0x179, new Tuple<string, string>("Ouroboros", "Fortune") },
            { 0x188, new Tuple<string, string>("Ariadne", "Fortune") },
            { 0x189, new Tuple<string, string>("Ariadne Picaro", "Fortune") },
            { 0x18C, new Tuple<string, string>("Asterius", "Fortune") },
            { 0x18D, new Tuple<string, string>("Asterius Picaro", "Fortune") },
        };

        #endregion PersonasFortune

        #region PersonasHangedMan

        internal static Dictionary<int, Tuple<string, string>> PersonasHangedMan = new Dictionary<int, Tuple<string, string>>()
        {
            { 0, new Tuple<string, string>("EMPTY", "") },

            { 0x1F, new Tuple<string, string>("Gorgon", "Hanged Man") },
            { 0x6C, new Tuple<string, string>("Onmoraki", "Hanged Man") },
            { 0x6D, new Tuple<string, string>("Berith", "Hanged Man") },
            { 0x6E, new Tuple<string, string>("Ikusa", "Hanged Man") },
            { 0x6F, new Tuple<string, string>("Orthrus", "Hanged Man") },
            { 0x70, new Tuple<string, string>("Yatsufusa", "Hanged Man") },
            { 0x72, new Tuple<string, string>("Hell Biker", "Hanged Man") },
            { 0x73, new Tuple<string, string>("Vasuki", "Hanged Man") },
            { 0x74, new Tuple<string, string>("Attis", "Hanged Man") },
            { 0x177, new Tuple<string, string>("Moloch", "Hanged Man") },
        };

        #endregion PersonasHangedMan

        #region PersonasHermit

        internal static Dictionary<int, Tuple<string, string>> PersonasHermit = new Dictionary<int, Tuple<string, string>>()
        {
            { 0, new Tuple<string, string>("EMPTY", "") },

            { 0x51, new Tuple<string, string>("Azumi", "Hermit") },
            { 0x52, new Tuple<string, string>("Ippon-Datara", "Hermit") },
            { 0x54, new Tuple<string, string>("Hitokotonusi", "Hermit") },
            { 0x55, new Tuple<string, string>("Kurama Tengu", "Hermit") },
            { 0x56, new Tuple<string, string>("Niddhoggr", "Hermit") },
            { 0x57, new Tuple<string, string>("Nebiros", "Hermit") },
            { 0x58, new Tuple<string, string>("Arahabaki", "Hermit") },
            { 0x59, new Tuple<string, string>("Ongyo-ki", "Hermit") },
            { 0x97, new Tuple<string, string>("Nue", "Hermit") },
            { 0xCE, new Tuple<string, string>("Jack Frost", "Hermit") },
            { 0xCF, new Tuple<string, string>("Thor", "Hermit") },
            { 0x122, new Tuple<string, string>("Necronomicon", "Hermit") },
            { 0x123, new Tuple<string, string>("Prometheus", "Hermit") },
            { 0x161, new Tuple<string, string>("Caladrius", "Hermit") },
        };

        #endregion PersonasHermit

        #region PersonasHierophant

        internal static Dictionary<int, Tuple<string, string>> PersonasHierophant = new Dictionary<int, Tuple<string, string>>()
        {
            { 0, new Tuple<string, string>("EMPTY", "") },

            { 0x30, new Tuple<string, string>("Shiisaa", "Hierophant") },
            { 0x31, new Tuple<string, string>("Unicorn", "Hierophant") },
            { 0x34, new Tuple<string, string>("Daisoujou", "Hierophant") },
            { 0x36, new Tuple<string, string>("Kohryu", "Hierophant") },
            { 0xA3, new Tuple<string, string>("Mishaguji", "Hierophant") },
            { 0xA6, new Tuple<string, string>("Baal Zebul", "Hierophant") },
            { 0xC6, new Tuple<string, string>("Asura", "Hierophant") },
            { 0xC7, new Tuple<string, string>("Loki", "Hierophant") },
            { 0x112, new Tuple<string, string>("Castor", "Hierophant") },
            { 0x113, new Tuple<string, string>("Castor (improved?)", "Hierophant") },
            { 0x155, new Tuple<string, string>("Halphas", "Hierophant") },
            { 0x15F, new Tuple<string, string>("Koppa Tengu", "Hierophant") },
            { 0x168, new Tuple<string, string>("Dantalian", "Hierophant") },
            { 0x16D, new Tuple<string, string>("Cherub", "Hierophant") },
            { 0x17E, new Tuple<string, string>("Kresnik", "Hierophant") },
        };

        #endregion PersonasHierophant

        #region PersonasJudgement

        internal static Dictionary<int, Tuple<string, string>> PersonasJudgement = new Dictionary<int, Tuple<string, string>>()
        {
            { 0, new Tuple<string, string>("EMPTY", "") },

            { 0x47, new Tuple<string, string>("Ophanim", "Judgement") },
            { 0x76, new Tuple<string, string>("Turdak", "Judgement") },
            { 0xB1, new Tuple<string, string>("Anubis", "Judgement") },
            { 0xB2, new Tuple<string, string>("Black Rider", "Judgement") },
            { 0xB3, new Tuple<string, string>("Trumpeter", "Judgement") },
            { 0xB4, new Tuple<string, string>("Michael", "Judgement") },
            { 0xB5, new Tuple<string, string>("Satan", "Judgement") },
            { 0xB8, new Tuple<string, string>("Lucifer", "Judgement") },
            { 0xB9, new Tuple<string, string>("Warrior Zeus", "Judgement") },
            { 0xBA, new Tuple<string, string>("Zeus", "Judgement") },
            { 0x115, new Tuple<string, string>("Messiah", "Judgement") },
            { 0x167, new Tuple<string, string>("Berserker", "Judgement") },
            { 0x178, new Tuple<string, string>("Prometheus", "Judgement") },
            { 0x183, new Tuple<string, string>("Messiah Picaro", "Judgement") },
        };

        #endregion PersonasJudgement

        #region PersonasJustice

        internal static Dictionary<int, Tuple<string, string>> PersonasJustice = new Dictionary<int, Tuple<string, string>>()
        {
            { 0, new Tuple<string, string>("EMPTY", "") },

            { 0x26, new Tuple<string, string>("Setanta", "Justice") },
            { 0x4A, new Tuple<string, string>("Angel", "Justice") },
            { 0x4C, new Tuple<string, string>("Virtue", "Justice") },
            { 0x4D, new Tuple<string, string>("Throne", "Justice") },
            { 0x4E, new Tuple<string, string>("Uriel", "Justice") },
            { 0x4F, new Tuple<string, string>("Melchizedek", "Justice") },
            { 0xB6, new Tuple<string, string>("Metatron", "Justice") },
            { 0xBB, new Tuple<string, string>("Archangel", "Justice") },
            { 0xBD, new Tuple<string, string>("Dominion", "Justice") },
            { 0xCC, new Tuple<string, string>("Black Frost", "Justice") },
            { 0xCD, new Tuple<string, string>("Surt", "Justice") },
            { 0x10E, new Tuple<string, string>("Nemesis", "Justice") },
            { 0x10F, new Tuple<string, string>("Kala-Nemi", "Justice") },
            { 0x126, new Tuple<string, string>("Robin Hood", "Justice") },
            { 0x127, new Tuple<string, string>("Robin Hood (improved?)", "Justice") },
            { 0x160, new Tuple<string, string>("Principality", "Justice") },
            { 0x17A, new Tuple<string, string>("Jeanne d'Arc", "Justice") },
            { 0x180, new Tuple<string, string>("Seraph", "Justice") },
        };

        #endregion PersonasJustice

        #region PersonasLovers

        internal static Dictionary<int, Tuple<string, string>> PersonasLovers = new Dictionary<int, Tuple<string, string>>()
        {
            { 0xFFFF, new Tuple<string, string>("EMPTY", "") },

            { 0x16, new Tuple<string, string>("High Pixie", "Lovers") },
            { 0x37, new Tuple<string, string>("Pixie", "Lovers") },
            { 0x3A, new Tuple<string, string>("Queen Mab", "Lovers") },
            { 0x3B, new Tuple<string, string>("Leanan Sidhe", "Lovers") },
            { 0x3C, new Tuple<string, string>("Raphael", "Lovers") },
            { 0x3D, new Tuple<string, string>("Cybele", "Lovers") },
            { 0x3E, new Tuple<string, string>("Ishtar", "Lovers") },
            { 0xC8, new Tuple<string, string>("Okuninushi", "Lovers") },
            { 0xC9, new Tuple<string, string>("Odin", "Lovers") },
            { 0xD5, new Tuple<string, string>("Kouzeon", "Lovers") },
            { 0xFA, new Tuple<string, string>("Himiko", "Lovers") },
            { 0xFB, new Tuple<string, string>("Kanzeon", "Lovers") },
            { 0x102, new Tuple<string, string>("Io", "Lovers") },
            { 0x103, new Tuple<string, string>("Isis", "Lovers") },
            { 0x11A, new Tuple<string, string>("Carmen", "Lovers") },
            { 0x11B, new Tuple<string, string>("Hecate", "Lovers") },
            { 0x156, new Tuple<string, string>("Heqet", "Lovers") },
            { 0x165, new Tuple<string, string>("Narcissus", "Lovers") },
            { 0x16A, new Tuple<string, string>("Vivian", "Lovers") },
            { 0x174, new Tuple<string, string>("Lorelei", "Lovers") },
        };

        #endregion PersonasLovers

        #region PersonasMagician

        internal static Dictionary<int, Tuple<string, string>> PersonasMagician = new Dictionary<int, Tuple<string, string>>()
        {
            { 0, new Tuple<string, string>("EMPTY", "") },

            { 0xA, new Tuple<string, string>("Agathion", "Magician") },
            { 0xB, new Tuple<string, string>("Orobas", "Magician") },
            { 0xC, new Tuple<string, string>("Jack Frost", "Magician") },
            { 0xD, new Tuple<string, string>("Hua Po", "Magician") },
            { 0xE, new Tuple<string, string>("Pyro Jack", "Magician") },
            { 0xF, new Tuple<string, string>("Dis", "Magician") },
            { 0x10, new Tuple<string, string>("Rangda", "Magician") },
            { 0x11, new Tuple<string, string>("Jinn", "Magician") },
            { 0x12, new Tuple<string, string>("Surt", "Magician") },
            { 0x13, new Tuple<string, string>("Mada", "Magician") },
            { 0xBF, new Tuple<string, string>("Basilisk", "Magician") },
            { 0xC0, new Tuple<string, string>("Ouyamatsumi", "Magician") },
            { 0xD8, new Tuple<string, string>("Takehaya Susano-o", "Magician") },
            { 0xF4, new Tuple<string, string>("Jiraiya", "Magician") },
            { 0xF5, new Tuple<string, string>("Susano-o", "Magician") },
            { 0x104, new Tuple<string, string>("Hermes", "Magician") },
            { 0x105, new Tuple<string, string>("Trismegistus", "Magician") },
            { 0x11C, new Tuple<string, string>("Zorro", "Magician") },
            { 0x11D, new Tuple<string, string>("Mercurius", "Magician") },
            { 0x17D, new Tuple<string, string>("Kudlak", "Magician") },
        };

        #endregion PersonasMagician

        #region PersonasMoon

        internal static Dictionary<int, Tuple<string, string>> PersonasMoon = new Dictionary<int, Tuple<string, string>>()
        {
            { 0, new Tuple<string, string>("EMPTY", "") },

            { 0x9E, new Tuple<string, string>("Andras", "Moon") },
            { 0x9F, new Tuple<string, string>("Nozuchi", "Moon") },
            { 0xA0, new Tuple<string, string>("Orochi", "Moon") },
            { 0xA1, new Tuple<string, string>("Alraune", "Moon") },
            { 0xA2, new Tuple<string, string>("Girimehkala", "Moon") },
            { 0xA4, new Tuple<string, string>("Chernobog", "Moon") },
            { 0xA5, new Tuple<string, string>("Seth", "Moon") },
            { 0xA7, new Tuple<string, string>("Sandalphon", "Moon") },
            { 0xD1, new Tuple<string, string>("Kaguya", "Moon") },
            { 0xE8, new Tuple<string, string>("Gabriel", "Moon") },
            { 0xE9, new Tuple<string, string>("Raphael", "Moon") },
            { 0x150, new Tuple<string, string>("Saki Mitama", "Moon") },
            { 0x159, new Tuple<string, string>("Sudama", "Moon") },
            { 0x164, new Tuple<string, string>("Hare of Inaba", "Moon") },
            { 0x16F, new Tuple<string, string>("Fenrir", "Moon") },
            { 0x187, new Tuple<string, string>("Kaguya Picaro", "Moon") },
            { 0x18A, new Tuple<string, string>("Tsukiyomi", "Moon") },
            { 0x18B, new Tuple<string, string>("Tsukiyomi Picaro", "Moon") },
        };

        #endregion PersonasMoon

        #region PersonasPriestess

        internal static Dictionary<int, Tuple<string, string>> PersonasPriestess = new Dictionary<int, Tuple<string, string>>()
        {
            { 0, new Tuple<string, string>("EMPTY", "") },

            { 0x15, new Tuple<string, string>("Sarasvati", "Priestess") },
            { 0x17, new Tuple<string, string>("Ganga", "Priestess") },
            { 0x18, new Tuple<string, string>("Parvati", "Priestess") },
            { 0x1A, new Tuple<string, string>("Hariti", "Priestess") },
            { 0x1B, new Tuple<string, string>("Tzitzimitl", "Priestess") },
            { 0x1C, new Tuple<string, string>("Scathach", "Priestess") },
            { 0x20, new Tuple<string, string>("Gabriel", "Priestess") },
            { 0x39, new Tuple<string, string>("Moh Shuvuu", "Priestess") },
            { 0xC1, new Tuple<string, string>("Warrior Zeus", "Priestess") },
            { 0xC2, new Tuple<string, string>("Zeus", "Priestess") },
            { 0xD6, new Tuple<string, string>("Sumeo-Okami", "Priestess") },
            { 0xF8, new Tuple<string, string>("Konohana Sakuya", "Priestess") },
            { 0xF9, new Tuple<string, string>("Amaterasu", "Priestess") },
            { 0x10A, new Tuple<string, string>("Lucia", "Priestess") },
            { 0x10B, new Tuple<string, string>("Juno", "Priestess") },
            { 0x120, new Tuple<string, string>("Johanna", "Priestess") },
            { 0x121, new Tuple<string, string>("Anat", "Priestess") },
            { 0x169, new Tuple<string, string>("Dzelarhons", "Priestess") },
            { 0x17F, new Tuple<string, string>("Black Maria", "Priestess") },
        };

        #endregion PersonasPriestess

        #region PersonasStar

        internal static Dictionary<int, Tuple<string, string>> PersonasStar = new Dictionary<int, Tuple<string, string>>()
        {
            { 0, new Tuple<string, string>("EMPTY", "") },

            { 0x14, new Tuple<string, string>("Koropokkur", "Star") },
            { 0x50, new Tuple<string, string>("Sraosha", "Star") },
            { 0x7F, new Tuple<string, string>("Xiezhai", "Star") },
            { 0x96, new Tuple<string, string>("Kaiwan", "Star") },
            { 0x98, new Tuple<string, string>("Neko Shogun", "Star") },
            { 0x9A, new Tuple<string, string>("Garuda", "Star") },
            { 0x9C, new Tuple<string, string>("Saturnus", "Star") },
            { 0x9D, new Tuple<string, string>("Helel", "Star") },
            { 0xAE, new Tuple<string, string>("Suparna", "Star") },
            { 0xD4, new Tuple<string, string>("Kamui-Moshiri", "Star") },
            { 0xE4, new Tuple<string, string>("Trumpeter", "Star") },
            { 0xE5, new Tuple<string, string>("Lilith", "Star") },
            { 0xE6, new Tuple<string, string>("Michael", "Star") },
            { 0xE7, new Tuple<string, string>("Uriel", "Star") },
            { 0x100, new Tuple<string, string>("Kintoki-Douji", "Star") },
            { 0x101, new Tuple<string, string>("Kamui", "Star") },
            { 0x151, new Tuple<string, string>("Kushi Mitama", "Star") },
            { 0x15D, new Tuple<string, string>("Gemori", "Star") },
            { 0x166, new Tuple<string, string>("Sleipnir", "Star") },
        };

        #endregion PersonasStar

        #region PersonasStrength

        internal static Dictionary<int, Tuple<string, string>> PersonasStrength = new Dictionary<int, Tuple<string, string>>()
        {
            { 0, new Tuple<string, string>("EMPTY", "") },

            { 0x2A, new Tuple<string, string>("Ouyamatsumi", "Strength") },
            { 0x32, new Tuple<string, string>("Gozuki", "Strength") },
            { 0x64, new Tuple<string, string>("Valkyrie", "Strength") },
            { 0x65, new Tuple<string, string>("Rakshasa", "Strength") },
            { 0x66, new Tuple<string, string>("Tsuchigumo", "Strength") },
            { 0x67, new Tuple<string, string>("Oni", "Strength") },
            { 0x68, new Tuple<string, string>("Hanuman", "Strength") },
            { 0x69, new Tuple<string, string>("Siegfried", "Strength") },
            { 0x6B, new Tuple<string, string>("Zaou-Gongen", "Strength") },
            { 0x75, new Tuple<string, string>("Mokoi", "Strength") },
            { 0x110, new Tuple<string, string>("Cerberus", "Strength") },
            { 0x111, new Tuple<string, string>("Cerberus (improved?)", "Strength") },
            { 0x153, new Tuple<string, string>("Ara Mitama", "Strength") },
        };

        #endregion PersonasStrength

        #region PersonasSun

        internal static Dictionary<int, Tuple<string, string>> PersonasSun = new Dictionary<int, Tuple<string, string>>()
        {
            { 0, new Tuple<string, string>("EMPTY", "") },

            { 0x35, new Tuple<string, string>("Hachiman", "Sun") },
            { 0x71, new Tuple<string, string>("Basilisk", "Sun") },
            { 0xA8, new Tuple<string, string>("Cu Sith", "Sun") },
            { 0xA9, new Tuple<string, string>("Phoenix", "Sun") },
            { 0xAB, new Tuple<string, string>("Narasimha", "Sun") },
            { 0xAC, new Tuple<string, string>("Tam Lin", "Sun") },
            { 0xAD, new Tuple<string, string>("Jatayu", "Sun") },
            { 0xAF, new Tuple<string, string>("Asura", "Sun") },
            { 0x162, new Tuple<string, string>("Pele", "Sun") },
            { 0x16C, new Tuple<string, string>("Lugh", "Sun") },
            { 0x170, new Tuple<string, string>("Hathor", "Sun") },
        };

        #endregion PersonasSun

        #region PersonasTemperance

        internal static Dictionary<int, Tuple<string, string>> PersonasTemperance = new Dictionary<int, Tuple<string, string>>()
        {
            { 0, new Tuple<string, string>("EMPTY", "") },

            { 0x7E, new Tuple<string, string>("Apsaras", "Temperance") },
            { 0x80, new Tuple<string, string>("Mithra", "Temperance") },
            { 0x81, new Tuple<string, string>("Genbu", "Temperance") },
            { 0x82, new Tuple<string, string>("Seiryu", "Temperance") },
            { 0x83, new Tuple<string, string>("Suzaku", "Temperance") },
            { 0x84, new Tuple<string, string>("Byakko", "Temperance") },
            { 0x85, new Tuple<string, string>("Vishnu", "Temperance") },
            { 0xDB, new Tuple<string, string>("Shiva", "Temperance") },
            { 0xDC, new Tuple<string, string>("Vishnu", "Temperance") },
            { 0x158, new Tuple<string, string>("Silky", "Temperance") },
            { 0x15A, new Tuple<string, string>("Bicorn", "Temperance") },
            { 0x171, new Tuple<string, string>("Mithras", "Temperance") },
        };

        #endregion PersonasTemperance

        #region PersonasTower

        internal static Dictionary<int, Tuple<string, string>> PersonasTower = new Dictionary<int, Tuple<string, string>>()
        {
            { 0, new Tuple<string, string>("EMPTY", "") },

            { 0x43, new Tuple<string, string>("Ares", "Tower") },
            { 0x5B, new Tuple<string, string>("Raiju", "Tower") },
            { 0x86, new Tuple<string, string>("Ukobach", "Tower") },
            { 0x8E, new Tuple<string, string>("Cu Chulainn", "Tower") },
            { 0x8F, new Tuple<string, string>("Abaddon", "Tower") },
            { 0x90, new Tuple<string, string>("Mara", "Tower") },
            { 0x91, new Tuple<string, string>("Seiten Taisei", "Tower") },
            { 0x92, new Tuple<string, string>("Yoshitsune", "Tower") },
            { 0x93, new Tuple<string, string>("Masakado", "Tower") },
            { 0x94, new Tuple<string, string>("Shiva", "Tower") },
            { 0x95, new Tuple<string, string>("Chi You", "Tower") },
            { 0xD2, new Tuple<string, string>("Magatsu-Izanagi", "Tower") },
            { 0xE0, new Tuple<string, string>("Kohryu", "Tower") },
            { 0xE1, new Tuple<string, string>("Sandalphon", "Tower") },
            { 0xE2, new Tuple<string, string>("Beelzebub", "Tower") },
            { 0xE3, new Tuple<string, string>("Satan", "Tower") },
            { 0x152, new Tuple<string, string>("Nigi Mitama", "Tower") },
            { 0x153, new Tuple<string, string>("Ammut", "Tower") },
            { 0x173, new Tuple<string, string>("Longinus", "Tower") },
            { 0x186, new Tuple<string, string>("M. Izanagi Picaro", "Tower") },
        };

        #endregion PersonasTower

        #region PersonasWorld

        internal static Dictionary<int, Tuple<string, string>> PersonasWorld = new Dictionary<int, Tuple<string, string>>()
        {
            { 0, new Tuple<string, string>("EMPTY", "") },

            { 0xF1, new Tuple<string, string>("Izanagi-no-Okami", "World") },
        };

        #endregion PersonasWorld

        #endregion Personas

        #region FullLists

        #region ItemsFullList

        internal static Dictionary<int, Tuple<string, string>> itemsFullList;

        internal static Tuple<string, string> GetItemData(int id)
        {
            InitializeItemsFullList();

            return itemsFullList.ContainsKey(id) ? itemsFullList[id] : null;
        }

        private static void InitializeItemsFullList()
        {
            if (itemsFullList == null)
            {
                itemsFullList = new Dictionary<int, Tuple<string, string>>();

                var accessories = Accessories.Concat(AccessoriesFusion).GroupBy(x => x.Key).ToDictionary(x => x.Key, x => x.FirstOrDefault().Value);

                var armours = ArmourUnisex.Concat(ArmourMales).Concat(ArmourFemales).Concat(ArmourMorgana).Concat(ArmourKoromaru).Concat(ArmourAigis)
                    .GroupBy(x => x.Key).ToDictionary(x => x.Key, x => x.FirstOrDefault().Value);

                // Materials offset: 0x701

                var weapons = WeaponsP3F.Concat(WeaponsP3M).Concat(WeaponsP4Junpei).Concat(WeaponsAkihikoMakoto).Concat(WeaponsMitsuru).Concat(WeaponsAigis).Concat(WeaponsKen).Concat(WeaponsYosukeKoromaru)
                    .Concat(WeaponsShinjiro).Concat(WeaponsYukari).Concat(Reserved)
                    .Concat(WeaponsChie).Concat(WeaponsYukiko).Concat(WeaponsKanji).Concat(WeaponsNaoto).Concat(WeaponsTeddie).Concat(WeaponsShinjiro)
                    .Concat(WeaponsP5).Concat(WeaponsRyuji).Concat(WeaponsAnn).Concat(WeaponsMorgana).Concat(WeaponsYusuke).Concat(WeaponsHaru).Concat(WeaponsAkechi)
                    .GroupBy(x => x.Key).ToDictionary(x => x.Key, x => x.FirstOrDefault().Value);

                itemsFullList = accessories.Concat(armours).Concat(Consumables)
                    .Concat(weapons)
                    .GroupBy(x => x.Key).ToDictionary(x => x.Key, x => x.FirstOrDefault().Value);
            }
        }

        #endregion ItemsFullList

        #region PersonasFullList

        internal static Dictionary<int, Tuple<string, string>> personasFullList;

        internal static Tuple<string, string> GetPersonaData(int id)
        {
            InitializePersonasFullList();

            return personasFullList.ContainsKey(id) ? personasFullList[id] : null;
        }

        private static void InitializePersonasFullList()
        {
            if (personasFullList == null)
            {
                personasFullList = new Dictionary<int, Tuple<string, string>>();

                var personas = PersonasChariot.Concat(PersonasDeath).Concat(PersonasDevil).Concat(PersonasEmperor).Concat(PersonasEmpress).Concat(PersonasFool).Concat(PersonasFortune).Concat(PersonasHangedMan)
                    .Concat(PersonasHermit)
                    .Concat(PersonasHierophant).Concat(PersonasJudgement).Concat(PersonasJustice).Concat(PersonasLovers).Concat(PersonasMagician).Concat(PersonasMoon).Concat(PersonasPriestess).Concat(PersonasStar)
                    .Concat(PersonasStrength)
                    .Concat(PersonasSun).Concat(PersonasTemperance).Concat(PersonasTower).Concat(PersonasWorld)
                    .GroupBy(x => x.Key).ToDictionary(x => x.Key, x => x.FirstOrDefault().Value);

                var skills = SkillsAilment.Concat(SkillsAlmighty).Concat(SkillsAuto).Concat(SkillsBless).Concat(SkillsBuff).Concat(SkillsCircle).Concat(SkillsCurse).Concat(SkillsElectric).Concat(SkillsEnemy)
                    .Concat(SkillsFire)
                    .Concat(SkillsHealing).Concat(SkillsIce).Concat(SkillsLinks).Concat(SkillsNaviBattle).Concat(SkillsNaviDungeon).Concat(SkillsNuclear).Concat(SkillsPhys).Concat(SkillsPsychochinesis)
                    .Concat(SkillsUnison).Concat(SkillsWind)
                    .GroupBy(x => x.Key).ToDictionary(x => x.Key, x => x.FirstOrDefault().Value);

                personasFullList = personas.Concat(skills)
                    .GroupBy(x => x.Key).ToDictionary(x => x.Key, x => x.FirstOrDefault().Value);
            }
        }

        #endregion PersonasFullList

        #endregion FullLists

        #region Reserved

        internal static Dictionary<int, Tuple<string, string>> Reserved = new Dictionary<int, Tuple<string, string>>()
        {
            { 0x2C2, new Tuple<string, string>("Mic", "Rise's weapon") },
            { 0x3F7, new Tuple<string, string>("Rise Armor", "Rise's armour") },
            
            { 0x2C3, new Tuple<string, string>("Armband", "Fuuka's weapon") },
            { 0x3F8, new Tuple<string, string>("Fuuka Armor", "Fuuka's armour") },

            { 0x2C1, new Tuple<string, string>("Laptop", "Futaba's weapon") },
            { 0x3F6, new Tuple<string, string>("Mods Coat", "Futaba's armour") },
        };

        #endregion Reserved

        #region Enepedia
        internal static byte[] Enepedia =
        {
            0xBB, 0xFF, 0xBB, 0xFF, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xBB, 0xFF, 0x00, 0x00, 0x00,
            0x00, 0xBB, 0xFF, 0x00, 0x00, 0xBB, 0xFF, 0x00, 0x00, 0x00, 0x00, 0xBB, 0xFF, 0x00, 0x00, 0xBB, 0xFF, 0x00, 0x00, 0x00, 0x00,
            0xBB, 0xFF, 0x00, 0x00, 0xBB, 0xFF, 0x00, 0x00, 0xBB, 0xFF, 0x00, 0x00, 0x00, 0x00, 0xBB, 0xFF, 0xBB, 0xFF, 0x00, 0x00, 0x00,
            0x00, 0xBB, 0xFF, 0x00, 0x00, 0xBB, 0xFF, 0xBB, 0xFF, 0x00, 0x00, 0xBB, 0xFF, 0xBB, 0xFF, 0x00, 0x00, 0x00, 0x00, 0xBB, 0xFF,
            0xBB, 0xFF, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xBB, 0xFF, 0xBB, 0xFF, 0xBB, 0xFF, 0xBB, 0xFF, 0xBB, 0xFF, 0xBB,
            0xFF, 0xBB, 0xFF, 0xBB, 0xFF, 0x00, 0x00, 0xBB, 0xFF, 0xBB, 0xFF, 0x00, 0x00, 0x00, 0x00, 0xBB, 0xFF, 0x00, 0x00, 0x00, 0x00,
            0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xBB,
            0xFF, 0x00, 0x00, 0xBF, 0xFF, 0x00, 0x00, 0xBB, 0xFF, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
            0x00, 0x00, 0x00, 0x00, 0xBB, 0xFF, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xBB, 0xFF, 0x00, 0x00, 0xBB, 0xFF, 0xBB, 0xFF, 0xBB,
            0xFF, 0xBB, 0xFF, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xBB, 0xFF, 0x00, 0x00, 0x00, 0x00, 0xBB, 0xFF, 0xBB, 0xFF, 0x00, 0x00,
            0xBB, 0xFF, 0x00, 0x00, 0x00, 0x00, 0xBB, 0xFF, 0xBB, 0xFF, 0x00, 0x00, 0xBB, 0xFF, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
            0x00, 0xBB, 0xFF, 0x00, 0x00, 0x00, 0x00, 0xBB, 0xFF, 0x00, 0x00, 0xBB, 0xFF, 0x00, 0x00, 0x00, 0x00, 0xBB, 0xFF, 0x00, 0x00,
            0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xBB, 0xFF, 0x00, 0x00, 0xBB, 0xFF, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xBB,
            0xFF, 0x00, 0x00, 0xBB, 0xFF, 0xBB, 0xFF, 0x00, 0x00, 0xBB, 0xFF, 0x00, 0x00, 0xBB, 0xFF, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
            0x00, 0x00, 0x00, 0x00, 0xBB, 0xFF, 0x00, 0x00, 0xBB, 0xFF, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xBB, 0xFF, 0x00, 0x00, 0x00,
            0x00, 0x00, 0x00, 0xBB, 0xFF, 0x00, 0x00, 0xBB, 0xFF, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xBB, 0xFF, 0x00, 0x00, 0xBB, 0xFF,
            0x00, 0x00, 0xBB, 0xFF, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xBB, 0xFF, 0x00, 0x00, 0x00, 0x00, 0xBB, 0xFF, 0xBB, 0xFF, 0xBB,
            0xFF, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xBB, 0xFF, 0x00, 0x00, 0x00, 0x00,
            0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xBB, 0xFF, 0x00, 0x00, 0xBB, 0xFF, 0xBB, 0xFF, 0x00,
            0x00, 0x00, 0x00, 0x00, 0x00, 0xBB, 0xFF, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xBB, 0xFF, 0xBB, 0xFF, 0xBB, 0xFF,
            0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xBB, 0xFF, 0x00, 0x00, 0x00, 0x00, 0xBB, 0xFF, 0xBB, 0xFF, 0xBB, 0xFF, 0xBB,
            0xFF, 0xBB, 0xFF, 0xBB, 0xFF, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xBB, 0xFF,
            0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xBB, 0xFF, 0xBB, 0xFF, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
            0x00, 0xBB, 0xFF, 0x00, 0x00, 0x00, 0x00, 0xBB, 0xFF, 0x00, 0x00, 0x00, 0x00, 0xBB, 0xFF, 0xBB, 0xFF, 0x00, 0x00, 0x00, 0x00,
            0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xBB, 0xFF, 0xBB, 0xFF, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xBB, 0xFF, 0xBB,
            0xFF, 0xBB, 0xFF, 0xBB, 0xFF, 0xBB, 0xFF, 0x00, 0x00, 0xBB, 0xFF, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xBB, 0xFF, 0x00, 0x00,
            0x00, 0x00, 0x00, 0x00, 0xBB, 0xFF, 0x00, 0x00, 0xBB, 0xFF, 0xBB, 0xFF, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
            0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xBB, 0xFF, 0xBB, 0xFF, 0xBB, 0xFF, 0xBB, 0xFF, 0x00, 0x00, 0xBB, 0xFF, 0xBB, 0xFF,
            0xBB, 0xFF, 0xBB, 0xFF, 0xBB, 0xFF, 0xBB, 0xFF, 0xBB, 0xFF, 0xBB, 0xFF, 0x00, 0x00, 0xBB, 0xFF, 0xBB, 0xFF, 0xBB, 0xFF, 0xBB,
            0xFF, 0xBB, 0xFF, 0x00, 0x00, 0xBB, 0xFF, 0xBB, 0xFF, 0xBB, 0xFF, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
            0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xBB, 0xFF, 0xBB, 0xFF, 0x00, 0x00, 0x00, 0x00, 0x00,
            0x00, 0x00, 0x00, 0xBB, 0xFF, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
            0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
            0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
            0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
            0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
            0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
            0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
            0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
            0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
            0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
            0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
            0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xBB, 0xFF, 0xBB, 0xFF, 0xBB,
            0xFF, 0xBB, 0xFF, 0xBB, 0xFF, 0xBB, 0xFF, 0xBB, 0xFF, 0xBB, 0xFF, 0x00, 0x00, 0xBB, 0xFF, 0xBB, 0xFF, 0xBB, 0xFF, 0xBB, 0xFF,
            0xBB, 0xFF, 0xBB, 0xFF, 0xBB, 0xFF, 0xBB, 0xFF, 0xBB, 0xFF, 0xBB, 0xFF, 0xBB, 0xFF, 0xBB, 0xFF, 0xBB, 0xFF, 0xBB, 0xFF, 0xBB,
            0xFF, 0xBB, 0xFF, 0xBB, 0xFF, 0xBB, 0xFF, 0xBB, 0xFF, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
            0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
            0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
        };

        #endregion Enepedia

        #region Accessories

        internal static Dictionary<int, Tuple<string, string>> Accessories = new Dictionary<int, Tuple<string, string>>()
        {
            { 0, new Tuple<string, string>("EMPTY", "") },

            { 0x401, new Tuple<string, string>("Monocle", "Slightly raise accuracy") },
            { 0x402, new Tuple<string, string>("Anti-Fire Charm", "Slightly raise Fire evasion") },
            { 0x403, new Tuple<string, string>("Anti-Ice Charm", "Slightly raise Ice evasion") },
            { 0x404, new Tuple<string, string>("Anti-Elec Charm", "Slightly raise Elec evasion") },
            { 0x405, new Tuple<string, string>("Anti-Wind Charm", "Slightly raise Wind evasion") },
            { 0x406, new Tuple<string, string>("Red Ring", "Max HP +5%/St +1") },
            { 0x407, new Tuple<string, string>("Red Bangle", "Max SP +5%/St +1") },
            { 0x408, new Tuple<string, string>("Black Ring", "Max HP +5%/Ma +1") },
            { 0x409, new Tuple<string, string>("Black Bangle", "Max SP +5%/Ma +1") },
            { 0x40A, new Tuple<string, string>("White Ring", "Max HP +5%/En +1") },
            { 0x40B, new Tuple<string, string>("White Bangle", "Max SP +5%/En +1") },
            { 0x40C, new Tuple<string, string>("Blue Ring", "Max HP +5%/Ag +1") },
            { 0x40D, new Tuple<string, string>("Blue Bangle", "Max SP +5%/Ag +1") },
            { 0x40E, new Tuple<string, string>("Gold Ring", "Max HP +5%/Lu +1") },
            { 0x40F, new Tuple<string, string>("Gold Bangle", "Max SP +5%/Lu +1") },
            { 0x411, new Tuple<string, string>("Scarlet Ring", "Max HP +5%/St +3") },
            { 0x412, new Tuple<string, string>("Scarlet Bangle", "Max SP +5%/St +3") },
            { 0x413, new Tuple<string, string>("Ebon Ring", "Max HP +5%/Ma +3") },
            { 0x414, new Tuple<string, string>("Ebon Bangle", "Max SP +5%/Ma +3") },
            { 0x415, new Tuple<string, string>("Ivory Ring", "Max HP +5%/En +3") },
            { 0x416, new Tuple<string, string>("Ivory Bangle", "Max SP +5%/En +3") },
            { 0x417, new Tuple<string, string>("Azure Ring", "Max HP +5%/Ag +3") },
            { 0x418, new Tuple<string, string>("Azure Bangle", "Max SP +5%/Ag +3") },
            { 0x419, new Tuple<string, string>("Golden Ring", "Max HP +5%/Lu +3") },
            { 0x41A, new Tuple<string, string>("Golden Bangle", "Max SP +5%/Lu +3") },
            { 0x41C, new Tuple<string, string>("X Fire Ring", "Max HP +5%/Slightly raise Fire evasion") },
            { 0x41D, new Tuple<string, string>("X Fire Bangle", "Max SP +5%/Slightly raise Fire evasion") },
            { 0x41E, new Tuple<string, string>("X Ice Ring", "Max HP +5%/Slightly raise Ice evasion") },
            { 0x41F, new Tuple<string, string>("X Ice Bangle", "Max SP +5%/Slightly raise Ice evasion") },
            { 0x420, new Tuple<string, string>("X Wind Ring", "Max HP +5%/Slightly raise Wind evasion") },
            { 0x421, new Tuple<string, string>("X Wind Bangle", "Max SP +5%/Slightly raise Wind evasion") },
            { 0x422, new Tuple<string, string>("X Elec Ring", "Max HP +5%/Slightly raise Elec evasion") },
            { 0x423, new Tuple<string, string>("X Elec Bangle", "Max SP +5%/Slightly raise Elec evasion") },
            { 0x425, new Tuple<string, string>("Red Circlet", "Max HP +10%/St +1") },
            { 0x426, new Tuple<string, string>("Red Pin", "Max SP +10%/St +1") },
            { 0x427, new Tuple<string, string>("Black Circlet", "Max HP +10%/Ma +1") },
            { 0x428, new Tuple<string, string>("Black Pin", "Max SP +10%/Ma +1") },
            { 0x429, new Tuple<string, string>("White Circlet", "Max HP +10%/En +1") },
            { 0x42A, new Tuple<string, string>("White Pin", "Max SP +10%/En +1") },
            { 0x42B, new Tuple<string, string>("Blue Circlet", "Max HP +10%/Ag +1") },
            { 0x42C, new Tuple<string, string>("Blue Pin", "Max SP +10%/Ag +1") },
            { 0x42D, new Tuple<string, string>("Gold Circlet", "Max HP +10%/Lu +1") },
            { 0x42E, new Tuple<string, string>("Gold Pin", "Max SP +10%/Lu +1") },
            { 0x430, new Tuple<string, string>("Crimson Circlet", "Max HP +10%/St +3") },
            { 0x431, new Tuple<string, string>("Crimson Pin", "Max SP +10%/St +3") },
            { 0x432, new Tuple<string, string>("Ebon Circlet", "Max HP +10%/Ma +3") },
            { 0x433, new Tuple<string, string>("Ebon Pin", "Max SP +10%/Ma +3") },
            { 0x434, new Tuple<string, string>("Ivory Circlet", "Max HP +10%/En +3") },
            { 0x435, new Tuple<string, string>("Ivory Pin", "Max SP +10%/En +3") },
            { 0x436, new Tuple<string, string>("Azure Circlet", "Max HP +10%/Ag +3") },
            { 0x437, new Tuple<string, string>("Azure Pin", "Max SP +10%/Ag +3") },
            { 0x438, new Tuple<string, string>("Golden Circlet", "Max HP +10%/Lu +3") },
            { 0x439, new Tuple<string, string>("Golden Pin", "Max SP +10%/Lu +3") },
            { 0x43B, new Tuple<string, string>("X Fire Circlet", "Max HP +10%/Slightly raise Fire evasion") },
            { 0x43C, new Tuple<string, string>("X Fire Pin", "Max SP +10%/Slightly raise Fire evasion") },
            { 0x43D, new Tuple<string, string>("X Ice Circlet", "Max HP +10%/Slightly raise Ice evasion") },
            { 0x43E, new Tuple<string, string>("X Ice Pin", "Max SP +10%/Slightly raise Ice evasion") },
            { 0x43F, new Tuple<string, string>("X Wind Circlet", "Max HP +10%/Slightly raise Wind evasion") },
            { 0x440, new Tuple<string, string>("X Wind Pin", "Max SP +10%/Slightly raise Wind evasion") },
            { 0x441, new Tuple<string, string>("X Elec Circlet", "Max HP +10%/Slightly raise Elec evasion") },
            { 0x442, new Tuple<string, string>("X Elec Pin", "Max SP +10%/Slightly raise Elec evasion") },
            { 0x444, new Tuple<string, string>("Crimson Sash", "Max HP +15%/St +3") },
            { 0x445, new Tuple<string, string>("Crimson Charm", "Max SP +15%/St +3") },
            { 0x446, new Tuple<string, string>("Ebon Sash", "Max HP +15%/Ma +3") },
            { 0x447, new Tuple<string, string>("Ebon Charm", "Max SP +15%/Ma +3") },
            { 0x448, new Tuple<string, string>("Ivory Sash", "Max HP +15%/En +3") },
            { 0x449, new Tuple<string, string>("Ivory Charm", "Max SP +15%/En +3") },
            { 0x44A, new Tuple<string, string>("Azure Sash", "Max HP +15%/Ag +3") },
            { 0x44B, new Tuple<string, string>("Azure Charm", "Max SP +15%/Ag +3") },
            { 0x44C, new Tuple<string, string>("Golden Sash", "Max HP +15%/Lu +3") },
            { 0x44D, new Tuple<string, string>("Golden Charm", "Max SP +15%/Lu +3") },
            { 0x44F, new Tuple<string, string>("Power Sash", "Max HP +15%/St +5") },
            { 0x450, new Tuple<string, string>("Power Charm", "Max SP +15%/St +5") },
            { 0x451, new Tuple<string, string>("Mystic Sash", "Max HP +15%/Ma +5") },
            { 0x452, new Tuple<string, string>("Mystic Charm", "Max SP +15%/Ma +5") },
            { 0x453, new Tuple<string, string>("Barrier Sash", "Max HP +15%/En +5") },
            { 0x454, new Tuple<string, string>("Barrier Charm", "Max SP +15%/En +5") },
            { 0x455, new Tuple<string, string>("Haste Sash", "Max HP +15%/Ag +5") },
            { 0x456, new Tuple<string, string>("Haste Charm", "Max SP +15%/Ag +5") },
            { 0x457, new Tuple<string, string>("Lucky Sash", "Max HP +15%/Lu +5") },
            { 0x458, new Tuple<string, string>("Lucky Charm", "Max SP +15%/Lu +5") },
            { 0x45A, new Tuple<string, string>("Crimson Patch", "Max HP +20%/St +3") },
            { 0x45B, new Tuple<string, string>("Crimson Ankh", "Max SP +20%/St +3") },
            { 0x45C, new Tuple<string, string>("Ebon Patch", "Max HP +20%/Ma +3") },
            { 0x45D, new Tuple<string, string>("Ebon Ankh", "Max SP +20%/Ma +3") },
            { 0x45E, new Tuple<string, string>("Ivory Patch", "Max HP +20%/En +3") },
            { 0x45F, new Tuple<string, string>("Ivory Ankh", "Max SP +20%/En +3") },
            { 0x460, new Tuple<string, string>("Azure Patch", "Max HP +20%/Ag +3") },
            { 0x461, new Tuple<string, string>("Azure Ankh", "Max SP +20%/Ag +3") },
            { 0x462, new Tuple<string, string>("Golden Patch", "Max HP +20%/Lu +3") },
            { 0x463, new Tuple<string, string>("Golden Ankh", "Max SP +20%/Lu +3") },
            { 0x465, new Tuple<string, string>("Power Patch", "Max HP +20%/St +5") },
            { 0x466, new Tuple<string, string>("Power Ankh", "Max SP +20%/St +5") },
            { 0x467, new Tuple<string, string>("Mystic Patch", "Max HP +20%/Ma +5") },
            { 0x468, new Tuple<string, string>("Mystic Ankh", "Max SP +20%/Ma +5") },
            { 0x469, new Tuple<string, string>("Barrier Patch", "Max HP +20%/En +5") },
            { 0x46A, new Tuple<string, string>("Barrier Ankh", "Max SP +20%/En +5") },
            { 0x46B, new Tuple<string, string>("Haste Patch", "Max HP +20%/Ag +5") },
            { 0x46C, new Tuple<string, string>("Haste Ankh", "Max SP +20%/Ag +5") },
            { 0x46D, new Tuple<string, string>("Lucky Patch", "Max HP +20%/Lu +5") },
            { 0x46E, new Tuple<string, string>("Lucky Ankh", "Max SP +20%/Lu +5") },
            { 0x470, new Tuple<string, string>("Crimson Bead", "Max HP +25%/St +3") },
            { 0x471, new Tuple<string, string>("Crimson Orb", "Max SP +25%/St +3") },
            { 0x472, new Tuple<string, string>("Ebon Bead", "Max HP +25%/Ma +3") },
            { 0x473, new Tuple<string, string>("Ebon Orb", "Max SP +25%/Ma +3") },
            { 0x474, new Tuple<string, string>("Ivory Bead", "Max HP +25%/En +3") },
            { 0x475, new Tuple<string, string>("Ivory Orb", "Max SP +25%/En +3") },
            { 0x476, new Tuple<string, string>("Azure Bead", "Max HP +25%/Ag +3") },
            { 0x477, new Tuple<string, string>("Azure Orb", "Max SP +25%/Ag +3") },
            { 0x478, new Tuple<string, string>("Golden Bead", "Max HP +25%/Lu +3") },
            { 0x479, new Tuple<string, string>("Golden Orb", "Max SP +25%/Lu +3") },
            { 0x47B, new Tuple<string, string>("Power Bead", "Max HP +25%/St +5") },
            { 0x47C, new Tuple<string, string>("Power Orb", "Max SP +25%/St +5") },
            { 0x47D, new Tuple<string, string>("Mystic Bead", "Max HP +25%/Ma +5") },
            { 0x47E, new Tuple<string, string>("Mystic Orb", "Max SP +25%/Ma +5") },
            { 0x47F, new Tuple<string, string>("Barrier Bead", "Max HP +25%/En +5") },
            { 0x480, new Tuple<string, string>("Barrier Orb", "Max SP +25%/En +5") },
            { 0x481, new Tuple<string, string>("Haste Bead", "Max HP +25%/Ag +5") },
            { 0x482, new Tuple<string, string>("Haste Orb", "Max SP +25%/Ag +5") },
            { 0x483, new Tuple<string, string>("Lucky Bead", "Max HP +25%/Lu +5") },
            { 0x484, new Tuple<string, string>("Lucky Orb", "Max SP +25%/Lu +5") },
            { 0x486, new Tuple<string, string>("X Nuke Ring", "Max HP +5%/Slightly raise Nuke evasion") },
            { 0x487, new Tuple<string, string>("X Psy Ring", "Max HP +5%/Slightly raise Psy evasion") },
            { 0x488, new Tuple<string, string>("X Nuke Bangle", "Max SP +5%/Slightly raise Nuke evasion") },
            { 0x489, new Tuple<string, string>("X Psy Bangle", "Max SP +5%/Slightly raise Psy evasion") },
            { 0x48A, new Tuple<string, string>("X Nuke Circlet", "Max HP +10%/Slightly raise Nuke evasion") },
            { 0x48B, new Tuple<string, string>("X Psy Circlet", "Max HP +10%/Slightly raise Psy evasion") },
            { 0x48C, new Tuple<string, string>("X Nuke Pin", "Max SP +10%/Slightly raise Nuke evasion") },
            { 0x48D, new Tuple<string, string>("X Psy Pin", "Max SP +10%/Slightly raise Psy evasion") },
            { 0x48F, new Tuple<string, string>("Festival Drum", "St +5") },
            { 0x490, new Tuple<string, string>("Witch Charm", "Ma +5") },
            { 0x491, new Tuple<string, string>("Guard Rosary", "En +5") },
            { 0x492, new Tuple<string, string>("Pegasus Strap", "Ag +5") },
            { 0x493, new Tuple<string, string>("Wooden Ebisu", "Lu +5") },
            { 0x494, new Tuple<string, string>("Courage Fob", "Max HP +10%") },
            { 0x495, new Tuple<string, string>("Soul Potpourri", "Max SP +10%") },
            { 0x496, new Tuple<string, string>("Ambition Fob", "Max HP +15%") },
            { 0x497, new Tuple<string, string>("Boon Potpourri", "Max SP +15%") },
            { 0x498, new Tuple<string, string>("Delta Drive", "Critical rate +10%") },
            { 0x499, new Tuple<string, string>("Universal Badge", "All stats +1") },
            { 0x49A, new Tuple<string, string>("Almighty Badge", "All stats +2") },
            { 0x49B, new Tuple<string, string>("(DLC)Yosuga Bead", "Double encounter rate") },
            { 0x49C, new Tuple<string, string>("(DLC)Shijima Bead", "Eliminate encounters") },
            { 0x49D, new Tuple<string, string>("Growth 1", "Allies in reserve get 25% EXP") },
            { 0x49E, new Tuple<string, string>("Growth 2", "Allies in reserve get 50% EXP") },
            { 0x49F, new Tuple<string, string>("Growth 3", "Allies in reserve get 100% EXP") },
            { 0x4A0, new Tuple<string, string>("(DLC)Speed Gloves", "Triple combat EXP") },
            { 0x4A1, new Tuple<string, string>("(DLC)Lucre Gloves", "Triple the money gained in combat") },
            { 0x4A2, new Tuple<string, string>("Omnipotent Orb", "Nullifies all damage except Almighty") },
            { 0x4A3, new Tuple<string, string>("Hip Glasses", "No effect") },
            { 0x4A4, new Tuple<string, string>("Suspenders", "No effect") },
            { 0x4A5, new Tuple<string, string>("Hairpin", "No effect") },
            { 0x4A6, new Tuple<string, string>("Silver Keyring", "No effect") },
            { 0x4A7, new Tuple<string, string>("Black Necktie", "No effect") },
            { 0x4A8, new Tuple<string, string>("Fanny Pack", "No effect") },
            { 0x4A9, new Tuple<string, string>("Music Player", "No effect") },
            { 0x4AA, new Tuple<string, string>("Dotted Tights", "No effect") },
            { 0x4AB, new Tuple<string, string>("Black Tights", "No effect") },
            { 0x4AC, new Tuple<string, string>("Wristwatch", "No effect") },
            { 0x4AD, new Tuple<string, string>("Aluminum Pin", "No effect") },
            { 0x4AE, new Tuple<string, string>("Hair Band", "No effect") },
            { 0x4AF, new Tuple<string, string>("Bear Ears", "No effect") },
            { 0x4B0, new Tuple<string, string>("Silver Locket", "No effect") },
            { 0x4B1, new Tuple<string, string>("Spiral Earrings", "No effect") },
            { 0x4B2, new Tuple<string, string>("Plain Ring", "No effect") },
            { 0x4B3, new Tuple<string, string>("Music Player", "No effect") },
            { 0x4B4, new Tuple<string, string>("Heart Choker", "No effect") },
            { 0x4B5, new Tuple<string, string>("Fancy Nails", "No effect") },
            { 0x4B6, new Tuple<string, string>("Wristband", "No effect") },
            { 0x4B7, new Tuple<string, string>("Beanie", "No effect") },
            { 0x4B8, new Tuple<string, string>("Red Ribbon", "No effect") },
            { 0x4B9, new Tuple<string, string>("Leather Gloves", "No effect") },
            { 0x4BA, new Tuple<string, string>("Silver Necklace", "No effect") },
            { 0x4BB, new Tuple<string, string>("Koromaru Wings", "No effect") },
            { 0x4BC, new Tuple<string, string>("Headphones", "No effect") },
            { 0x4BD, new Tuple<string, string>("Pearl Earrings", "No effect") },
            { 0x4BE, new Tuple<string, string>("Bead Necklace", "No effect") },
        };

        #region AccessoriesFusion

        internal static Dictionary<int, Tuple<string, string>> AccessoriesFusion = new Dictionary<int, Tuple<string, string>>()
        {
            { 0, new Tuple<string, string>("EMPTY", "") },

            { 0x4C1, new Tuple<string, string>("Healing Armlet", "Acquire the Dia skill") },
            { 0x4C2, new Tuple<string, string>("Flame Vow", "Acquire the Agi skill") },
            { 0x4C3, new Tuple<string, string>("Psy Vow", "Acquire the Psi skill") },
            { 0x4C4, new Tuple<string, string>("Lucky Cat", "Get 10% more money in battle") },
            { 0x4C5, new Tuple<string, string>("Ice-Fire Ankh", "Low chance to nullify Fire and Ice") },
            { 0x4C6, new Tuple<string, string>("Wind-Elec Ankh", "Low chance to nullify Elec and Wind") },
            { 0x4C7, new Tuple<string, string>("Super Lucky Cat", "Get 15% more money in battle/Lu +2") },
            { 0x4C8, new Tuple<string, string>("Barrier Tech", "Low chance to nullify Nuke and Psy") },
            { 0x4C9, new Tuple<string, string>("SP Adhesive 3", "Recover SP each turn") },
            { 0x4CA, new Tuple<string, string>("Rudra Ring", "Halves HP cost") },
            { 0x4CB, new Tuple<string, string>("Chakra Ring", "Halves SP cost") },
            { 0x4CC, new Tuple<string, string>("St-Ma Reverse", "Swap St and Ma stats") },
            { 0x4CD, new Tuple<string, string>("St-En Reverse", "Swap St and En stats") },
            { 0x4CE, new Tuple<string, string>("St-Ag Reverse", "Swap St and Ag stats") },
            { 0x4CF, new Tuple<string, string>("St-Lu Reverse", "Swap St and Lu stats") },
            { 0x4D0, new Tuple<string, string>("Ma-En Reverse", "Swap Ma and En stats") },
            { 0x4D1, new Tuple<string, string>("Ma-Ag Reverse", "Swap Ma and Ag stats") },
            { 0x4D2, new Tuple<string, string>("Ma-Lu Reverse", "Swap Ma and Lu stats") },
            { 0x4D3, new Tuple<string, string>("En-Ag Reverse", "Swap En and Ag stats") },
            { 0x4D4, new Tuple<string, string>("En-Lu Reverse", "Swap En and Lu stats") },
            { 0x4D5, new Tuple<string, string>("Ag-Lu Reverse", "Swap Ag and Lu stats") },
            { 0x4E2, new Tuple<string, string>("Omnipotent Orb", "Nullifies all damage except Almighty") },
            { 0x4E4, new Tuple<string, string>("Body-Soul Ring", "Swap Max HP and Max SP") },
            { 0x4EA, new Tuple<string, string>("Promise Bracer", "Nullifies 1 ailment and 1 Bind") },
            { 0x4EB, new Tuple<string, string>("Focus Bracer", "Triple the power of magic skills") },
            { 0x4EC, new Tuple<string, string>("Energy Bracer", "Triple the power of Phys skills") },
            { 0x4ED, new Tuple<string, string>("Dragon Bracer", "Greatly raise attack/Act first") },
            
            // Resumes at 0xA01, until 0xA6B
        };

        #endregion AccessoriesFusion

        #endregion Accessories

        #region Armours

        #region ArmourAigis

        internal static Dictionary<int, Tuple<string, string>> ArmourAigis = new Dictionary<int, Tuple<string, string>>()
        {
            { 0, new Tuple<string, string>("EMPTY", "") },

            { 0x3CE, new Tuple<string, string>("TD Full Shell", "DEF 82, Max HP +10%, Aigis") },
            { 0x3CF, new Tuple<string, string>("Aigis Armor v1", "DEF 94, St +1/Ag +2, Aigis") },
            { 0x3D0, new Tuple<string, string>("Aigis Armor v2", "DEF 112, St +2/Ag +2, Aigis") },
            { 0x3D1, new Tuple<string, string>("TD W Plate", "DEF 115, Max SP +10%, Aigis") },
            { 0x3D2, new Tuple<string, string>("Anti-Seal Armor", "DEF 122, Resist Instant Kill, Aigis") },
            { 0x3D3, new Tuple<string, string>("TD Full Body", "DEF 137, All stats +1, Aigis") },
            { 0x3D4, new Tuple<string, string>("Aigis Armor v3", "DEF 155, Max HP +15%, Aigis") },
            { 0x3D5, new Tuple<string, string>("Element Body", "DEF 168, Increase all Binds resistance, Aigis") },
            { 0x3D6, new Tuple<string, string>("Aigis Armor v0", "DEF 176, All stats +2, Aigis") },
            { 0x3D8, new Tuple<string, string>("Frilled Armor", "DEF 125, Moderately raise Party Meter gains, Aigis") },
            { 0x3D9, new Tuple<string, string>("Aigis Armor v100", "DEF 180, Nullifies all Binds, Aigis") },
            { 0x3F5, new Tuple<string, string>("Aigis Armor", "DEF 85, Aigis") },
        };

        #endregion ArmourAigis

        #region ArmourFemales

        internal static Dictionary<int, Tuple<string, string>> ArmourFemales = new Dictionary<int, Tuple<string, string>>()
        {
            { 0, new Tuple<string, string>("EMPTY", "") },

            { 0x366, new Tuple<string, string>("Primary Cat Suit", "DEF 13, Ag +2, Female") },
            { 0x368, new Tuple<string, string>("Enchanted Cami", "DEF 22, Max SP +10%, Female") },
            { 0x369, new Tuple<string, string>("Steel Bunny Suit", "DEF 35, Lu +3, Female") },
            { 0x36B, new Tuple<string, string>("Utility Apron", "DEF 46, All stats +1, Female") },
            { 0x36C, new Tuple<string, string>("Primal Bikini", "DEF 57, Max HP +10%/St +2, Female") },
            { 0x36E, new Tuple<string, string>("Witchy Dress", "DEF 77, Max SP +5%/Ma +2, Female") },
            { 0x36F, new Tuple<string, string>("Futuristic Tights", "DEF 85, Ag +3, Female") },
            { 0x370, new Tuple<string, string>("Capital Robe", "DEF 88, Max HP +10%/Lu +3, Female") },
            { 0x372, new Tuple<string, string>("Breeze Tutu", "DEF 114, Ag +2, Female") },
            { 0x373, new Tuple<string, string>("Spiked Bra", "DEF 120, St +3, Female") },
            { 0x374, new Tuple<string, string>("Rune Dress", "DEF 128, Max SP +10%/Ma +3, Female") },
            { 0x375, new Tuple<string, string>("Invincible Mini", "DEF 149, Max HP +15%/En +2, Female") },
            { 0x376, new Tuple<string, string>("Haten Robe", "DEF 163, Ma +2/Ag +3, Female") },
            { 0x377, new Tuple<string, string>("Eternity Dress", "DEF 172, All stats +2, Female") },
            { 0x37A, new Tuple<string, string>("7-Fold Yukata", "DEF 92, St +2/Ma +2, Female") },
            { 0x37B, new Tuple<string, string>("New Year Kimono", "DEF 125, Nullifies Instant Kill, Female") },
            { 0x380, new Tuple<string, string>("Crimson Leotard", "DEF 180, Nullifies all Binds, Ann") },
            { 0x381, new Tuple<string, string>("Lord's Dress", "DEF 180, Nullifies all Binds, /P3F/") },
            { 0x382, new Tuple<string, string>("Beauty Thief", "DEF 180, Nullifies all Binds, Haru") },
            { 0x383, new Tuple<string, string>("Wasteland Robe", "DEF 180, Nullifies all Binds, Makoto") },
            { 0x384, new Tuple<string, string>("Solar Robe", "DEF 180, Nullifies all Binds, Chie") },
            { 0x385, new Tuple<string, string>("Ruby Kimono", "DEF 180, Nullifies all Binds, Yukiko") },
            { 0x386, new Tuple<string, string>("Black Dress", "DEF 180, Nullifies all Binds, Naoto") },
            { 0x387, new Tuple<string, string>("Sakura Robe", "DEF 180, Nullifies all Binds, Yukari") },
            { 0x388, new Tuple<string, string>("Empress Fur", "DEF 180, Nullifies all Binds, Mitsuru") },
            { 0x3DF, new Tuple<string, string>("4-Leaf Hoodie", "DEF 5, Female") },
            { 0x3E3, new Tuple<string, string>("Noble Blouse", "DEF 16, Female") },
            { 0x3E4, new Tuple<string, string>("Pink Top", "DEF 18, Female") },
            { 0x3E5, new Tuple<string, string>("Turtleneck", "DEF 20, Female") },
            { 0x3E7, new Tuple<string, string>("Green Sweats", "DEF 22, Female") },
            { 0x3E8, new Tuple<string, string>("Lace Blouse", "DEF 21, Female") },
            { 0x3EE, new Tuple<string, string>("Long Bolero", "DEF 54, Female") },
            { 0x3F0, new Tuple<string, string>("Ruffled Blouse", "DEF 54, Female") },
        };

        #endregion ArmourFemales

        #region ArmourKoromaru

        internal static Dictionary<int, Tuple<string, string>> ArmourKoromaru = new Dictionary<int, Tuple<string, string>>()
        {
            { 0, new Tuple<string, string>("EMPTY", "") },

            { 0x3B9, new Tuple<string, string>("Dotted Dog Suit", "DEF 72, Koromaru") },
            { 0x3BA, new Tuple<string, string>("Cross Dog Suit", "DEF 75, Ma +2, Koromaru") },
            { 0x3BB, new Tuple<string, string>("Guard Dog Suit", "DEF 85, Max HP +10%, Koromaru") },
            { 0x3BC, new Tuple<string, string>("Tech Dog Suit", "DEF 92, St +2/Ag +1, Koromaru") },
            { 0x3BD, new Tuple<string, string>("Metal Dog Suit", "DEF 112, Max HP +10%, Koromaru") },
            { 0x3BE, new Tuple<string, string>("Sky Dog Suit", "DEF 117, Ag +4, Koromaru") },
            { 0x3BF, new Tuple<string, string>("Marine Dog Suit", "DEF 117, Max SP +10%, Koromaru") },
            { 0x3C0, new Tuple<string, string>("Star Dog Suit", "DEF 134, Ma +4, Koromaru") },
            { 0x3C1, new Tuple<string, string>("Luna Dog Suit", "DEF 157, Max SP +10%/Ma +2, Koromaru") },
            { 0x3C2, new Tuple<string, string>("Solar Dog Suit", "DEF 165, Max HP +10%/St +2, Koromaru") },
            { 0x3C3, new Tuple<string, string>("Cosmos Dog Suit", "DEF 174, All stats +2, Koromaru") },
            { 0x3C4, new Tuple<string, string>("Light Dog Suit", "DEF 78, Ag +3, Koromaru") },
            { 0x3C5, new Tuple<string, string>("Soft Mane", "DEF 125, All stats +1, Koromaru") },
            { 0x3C6, new Tuple<string, string>("Ebony Dog Suit", "DEF 180, Nullifies all ailments, Koromaru") },
            { 0x3F2, new Tuple<string, string>("Silk Dog Suit", "DEF 55, Koromaru") },
        };

        #endregion ArmourKoromaru

        #region ArmourMales

        internal static Dictionary<int, Tuple<string, string>> ArmourMales = new Dictionary<int, Tuple<string, string>>()
        {
            { 0, new Tuple<string, string>("EMPTY", "") },

            { 0x334, new Tuple<string, string>("Shoulder Vest", "DEF 14, St +2, Male") },
            { 0x337, new Tuple<string, string>("Hero Tights", "DEF 32, Max HP +5%/Ag +2, Male") },
            { 0x338, new Tuple<string, string>("Manly Jinbei", "DEF 40, St +3, Male") },
            { 0x33A, new Tuple<string, string>("Tigerhide Belt", "DEF 62, St +2/Ag +2, Male") },
            { 0x33C, new Tuple<string, string>("Lion Happi", "DEF 78, Max HP +10%, Male") },
            { 0x33D, new Tuple<string, string>("Doumaru", "DEF 84, Ag +4, Male") },
            { 0x33E, new Tuple<string, string>("Gigas Vest", "DEF 95, St +3, Male") },
            { 0x340, new Tuple<string, string>("Passion Sweats", "DEF 111, Max HP +10%, Male") },
            { 0x341, new Tuple<string, string>("Crusader Plate", "DEF 125, Resist Instant Kill, Male") },
            { 0x342, new Tuple<string, string>("Hero's Knickers", "DEF 135, Max HP +10%/St +2, Male") },
            { 0x343, new Tuple<string, string>("Desperate Plate", "DEF 160, Increases all Bind resistance, Male") },
            { 0x344, new Tuple<string, string>("Full Jin-Baori", "DEF 168, Greatly resist Instant Kill, Male") },
            { 0x345, new Tuple<string, string>("Hallowed Plate", "DEF 175, All stats +2, Male") },
            { 0x348, new Tuple<string, string>("Purple Suit", "DEF 79, Max SP +10%/Ma +3, Male") },
            { 0x349, new Tuple<string, string>("Superstar Attire", "DEF 120, All stats +1, Male") },
            { 0x350, new Tuple<string, string>("Master Thief", "DEF 180, Nullifies all ailments, /P5/") },
            { 0x351, new Tuple<string, string>("Gold Sneak Suit", "DEF 180, Nullifies all ailments, Ryuji") },
            { 0x352, new Tuple<string, string>("Shajinbaori", "DEF 180, Nullifies all ailments, Yusuke") },
            { 0x353, new Tuple<string, string>("White Regalia", "DEF 180, Nullifies all ailments, Akechi") },
            { 0x354, new Tuple<string, string>("King Collar", "DEF 180, Nullifies all ailments, /P4/") },
            { 0x355, new Tuple<string, string>("Fuuma Outfit", "DEF 180, Nullifies all ailments, Yosuke") },
            { 0x356, new Tuple<string, string>("Gold Tokkoufuku", "DEF 180, Nullifies all ailments, Kanji") },
            { 0x357, new Tuple<string, string>("Magnet Bearsuit", "DEF 180, Nullifies all ailments, Teddie") },
            { 0x358, new Tuple<string, string>("Monarch Jacket", "DEF 180, Nullifies all ailments, /P3M/") },
            { 0x359, new Tuple<string, string>("Brave Scale", "DEF 180, Nullifies all ailments, Junpei") },
            { 0x35A, new Tuple<string, string>("Sirius Suit", "DEF 180, Nullifies all ailments, Akihiko") },
            { 0x35B, new Tuple<string, string>("Hero Hoodie", "DEF 180, Nullifies all ailments, Ken") },
            { 0x35C, new Tuple<string, string>("Moon Haori", "DEF 180, Nullifies all ailments, Shinjiro") },
            { 0x3DD, new Tuple<string, string>("Dark Undershirt", "DEF 6, Male") },
            { 0x3DE, new Tuple<string, string>("Printed T-Shirt", "DEF 7, Male") },
            { 0x3E0, new Tuple<string, string>("White Shirt", "DEF 6, Male") },
            { 0x3E1, new Tuple<string, string>("Formal Shirt", "DEF 5, Male") },
            { 0x3E6, new Tuple<string, string>("T-Shirt", "DEF 21, Male") },
            { 0x3E9, new Tuple<string, string>("Pretty Suit", "DEF 23, Male") },
            { 0x3EA, new Tuple<string, string>("Skull T-Shirt", "DEF 23, Male") },
            { 0x3EC, new Tuple<string, string>("Long T-Shirt", "DEF 49, Male") },
            { 0x3ED, new Tuple<string, string>("Plain Shirt", "DEF 82, Male") },
            { 0x3EF, new Tuple<string, string>("Dress Shirt", "DEF 55, Male") },
            { 0x3F1, new Tuple<string, string>("Fleece Vest", "DEF 56, Male") },
            { 0x3F3, new Tuple<string, string>("Zip Hoodie", "DEF 54, Male") },
            { 0x3F4, new Tuple<string, string>("Long Pea Coat", "DEF 58, Male") },
        };

        #endregion ArmourMales

        #region ArmourMorgana

        internal static Dictionary<int, Tuple<string, string>> ArmourMorgana = new Dictionary<int, Tuple<string, string>>()
        {
            { 0, new Tuple<string, string>("EMPTY", "") },

            { 0x397, new Tuple<string, string>("Cat Sweater", "DEF 13, Morgana") },
            { 0x398, new Tuple<string, string>("Breeze Scarf", "DEF 18, Ag +1, Morgana") },
            { 0x399, new Tuple<string, string>("Steel Scarf", "DEF 24, Morgana") },
            { 0x39A, new Tuple<string, string>("Red Scarf", "DEF 33, Max SP +5%, Morgana") },
            { 0x39B, new Tuple<string, string>("Grassland Scarf", "DEF 39, Increases resistance to all Binds, Morgana") },
            { 0x39C, new Tuple<string, string>("E-Collar", "DEF 49, Ma +2/Lu +1, Morgana") },
            { 0x39D, new Tuple<string, string>("Beast's Scarf", "DEF 59, Max HP +10%, Morgana") },
            { 0x39E, new Tuple<string, string>("Flowing Scarf", "DEF 70, Increase evasion, Morgana") },
            { 0x39F, new Tuple<string, string>("Artistic Scarf", "DEF 80, Lu +3, Morgana") },
            { 0x3A0, new Tuple<string, string>("Magical Scarf", "DEF 82, Max SP +5%/Ma +2, Morgana") },
            { 0x3A1, new Tuple<string, string>("Solar Wind Scarf", "DEF 90, Max HP +5%/St +2, Morgana") },
            { 0x3A2, new Tuple<string, string>("Id Collar", "DEF 106, Slightly raise Party Meter gains, Morgana") },
            { 0x3A3, new Tuple<string, string>("Memorial Collar", "DEF 117, Max HP & SP +5%, Morgana") },
            { 0x3A4, new Tuple<string, string>("Master's Scarf", "DEF 120, Increase EXP +15%, Morgana") },
            { 0x3A5, new Tuple<string, string>("Killer Collar", "DEF 130, St +3, Morgana") },
            { 0x3A6, new Tuple<string, string>("Sublime Collar", "DEF 154, Max SP +10%/Ma +2, Morgana") },
            { 0x3A7, new Tuple<string, string>("Gorgeous Collar", "DEF 162, Max HP +10%/St +3, Morgana") },
            { 0x3A8, new Tuple<string, string>("Nekomata Coat+", "DEF 173, All stats +2, Morgana") },
            { 0x3AA, new Tuple<string, string>("Guard Scarf", "DEF 20, Max HP +5%, Morgana") },
            { 0x3AB, new Tuple<string, string>("Trendy Scarf", "DEF 20, Increase EXP +10%, Morgana") },
            //{ 0x3AB, new Tuple<string, string>("Fluffy Scarf", "DEF 125, All stats +1, Morgana") }, // TODO
            { 0x3B0, new Tuple<string, string>("Archangel Scarf", "DEF 180, Nullifies all ailments, Morgana") },
            { 0x3E2, new Tuple<string, string>("Neckerchief", "DEF 10, Morgana") },
        };

        #endregion ArmourMorgana

        #region ArmourUnisex

        internal static Dictionary<int, Tuple<string, string>> ArmourUnisex = new Dictionary<int, Tuple<string, string>>()
        {
            { 0, new Tuple<string, string>("EMPTY", "") },

            { 0x301, new Tuple<string, string>("Novelty Hoodie", "DEF 9, Unisex") },
            { 0x302, new Tuple<string, string>("Staff Sweater", "DEF 12, Unisex") },
            { 0x304, new Tuple<string, string>("Puffy Down Coat", "DEF 21, Max HP +5%, Unisex") },
            { 0x305, new Tuple<string, string>("Fitted Chainmail", "DEF 25, Unisex") },
            { 0x306, new Tuple<string, string>("Aramid Vest", "DEF 34, Unisex") },
            { 0x307, new Tuple<string, string>("Survival Guard", "DEF 38, Max HP +10%, Unisex") },
            { 0x308, new Tuple<string, string>("Camo Poncho", "DEF 50, Ag +1, Unisex") },
            { 0x30A, new Tuple<string, string>("Enhanced Armor", "DEF 68, Low chance to resist all ailments, Unisex") },
            { 0x30B, new Tuple<string, string>("Hard Armor", "DEF 82, Unisex") },
            { 0x30D, new Tuple<string, string>("Hope Shirt", "DEF 60, Increase item drop rate, Unisex") },
            { 0x30E, new Tuple<string, string>("Soul Vest", "DEF 110, Max SP +10%, Unisex") },
            { 0x30F, new Tuple<string, string>("Opera Coat", "DEF 115, Ma +2, Unisex") },
            { 0x310, new Tuple<string, string>("Egoist Coat", "DEF 119, Max HP +15%, Unisex") },
            { 0x311, new Tuple<string, string>("Vogue Vest", "DEF 154, Ag +2/Lu +3, Unisex") },
            { 0x312, new Tuple<string, string>("Akashic Shirt", "DEF 154, Max HP & SP +10%, Unisex") },
            { 0x313, new Tuple<string, string>("Kaiser Armor", "DEF 170, St +3/Ag +3, Unisex") },
            { 0x314, new Tuple<string, string>("Papal Robe", "DEF 163, Max SP +15%/Ma +3, Unisex") },
            { 0x316, new Tuple<string, string>("Resistance Armor", "DEF 72, Low chance of resisting all Binds, Unisex") },
            { 0x317, new Tuple<string, string>("Unity Shirt", "DEF 70, Moderately raise Party Meter gains, Unisex") },
            { 0x318, new Tuple<string, string>("Growth Shirt", "DEF 50, Increase EXP +15%, Unisex") },
            { 0x319, new Tuple<string, string>("Trendy Shirt", "DEF 20, Increase EXP +10%, Unisex") },
            { 0x3EB, new Tuple<string, string>("Gothic Shirt", "DEF 22, Unisex") },
        };

        #endregion ArmourUnisex

        #endregion Armours

        #region Consumables

        internal static Dictionary<int, Tuple<string, string>> Consumables = new Dictionary<int, Tuple<string, string>>()
        {
            { 0, new Tuple<string, string>("EMPTY", "") },

            { 0x504, new Tuple<string, string>("Bead", "A mysterious bead that emits a healing light. Restore all HP. (1 Ally)") },
            { 0x505, new Tuple<string, string>("Soul Drop", "Mysterious candy that invigorates the soul. Restore 30 SP. (1 Ally)") },
            { 0x506, new Tuple<string, string>("Snuff Soul", "A treat made rom invigorating ingredients. Restore 60 SP. (1 Ally)") },
            { 0x507, new Tuple<string, string>("Chewing Soul", "A mysterious gum that enriches the soul. Restore 100 SP. (1 Ally)") },
            { 0x509, new Tuple<string, string>("Soma Droplet", "A droplet of the \"drink of the gods.\" Restore 100 HP and 25 SP. (1 Ally)") },
            { 0x50A, new Tuple<string, string>("Soma Tear", "Crystallized \"drink of the gods.\" Restore 200 HP and 50 SP. (1 Ally)") },
            { 0x50C, new Tuple<string, string>("Revival Bead", "A bead that can recall a fallen soul. Revive and restore half HP. (1 Ally)") },
            { 0x50D, new Tuple<string, string>("Balm of Life", "Incense that can recall a fallen soul. Revive and restore all HP. (1 Ally)") },
            { 0x510, new Tuple<string, string>("Bind-Aid", "Medicine to unseal one's strength. Cure Binds. (1 Ally)") },
            { 0x512, new Tuple<string, string>("Purifying Water", "Spring water consecrated in a holy ritual. Remove buffs. (All Enemies)") },
            { 0x513, new Tuple<string, string>("Purifying Salt", "Coarse salt consecrated in a holy ritual. Remove debuffs. (Party)") },
            { 0x514, new Tuple<string, string>("Rasetsu Ofuda", "A ward with Rasetsu's divine blessing. Raise attack for 3 turns. (1 Row)") },
            { 0x515, new Tuple<string, string>("Kongo Ofuda", "A ward with the Kongou's divine blessing. Raise defense for 3 turns. (1 Row)") },
            { 0x516, new Tuple<string, string>("Idaten Ofuda", "A ward with Idaten's divine blessing. Raise accuracy/evasion for 3 turns. (1 Row)") },
            { 0x517, new Tuple<string, string>("Amrita Soda", "Liquid styled after the drink of the gods. Cure ailments and Binds. (Party)") },
            { 0x519, new Tuple<string, string>("Fire Mist", "Conjures mist that drowns out flames. Grant Fire resistance for 3 turns. (Party)") },
            { 0x51A, new Tuple<string, string>("Ice Mist", "Conjures mist that reduces chill. Grant Ice resistance for 3 turns. (Party)") },
            { 0x51B, new Tuple<string, string>("Elec Mist", "Conjures mist that dampens electricity. Grant Elec resistance for 3 turns. (Party)") },
            { 0x51C, new Tuple<string, string>("Wind Mist", "Conjures mist that lessens wind. Grant Wind resistance for 3 turns. (Party)") },
            { 0x51D, new Tuple<string, string>("Nuclear Mist", "Conjures mist that negates nuclear energy. Grant Nuke resistance for 3 turns. (Party)") },
            { 0x51E, new Tuple<string, string>("Psy Mist", "Conjures mist that muddles ESP. Grant Psy resistance for 3 turns. (Party)") },
            { 0x521, new Tuple<string, string>("San-zun Tama", "Bursts with a fiery blast when thrown. Light Fire damage. (All Enemies)") },
            { 0x522, new Tuple<string, string>("Scorching Bullet", "A bullet that explodes into flame. Medium Fire damage. (All Enemies)") },
            { 0x523, new Tuple<string, string>("Dry Ice", "Bursts with crackling frost when thrown. Light Ice damage. (All Enemies)") },
            { 0x524, new Tuple<string, string>("Cryo Bullet", "A bullet that explodes into a blizzard. Medium Ice damage. (All Enemies)") },
            { 0x525, new Tuple<string, string>("Tesla Coil", "Bursts with high voltage when thrown. Light Elec damage. (All Enemies)") },
            { 0x526, new Tuple<string, string>("Bolt Bullet", "A bullet that explodes into electricity. Medium Elec damage. (All Enemies)") },
            { 0x527, new Tuple<string, string>("Yashichi", "Bursts with galeforce winds when thrown. Light Wind damage. (All Enemies)") },
            { 0x528, new Tuple<string, string>("Tornado Bullet", "A bullet that explodes into a whirlwind. Medium Wind damage. (All Enemies)") },
            { 0x529, new Tuple<string, string>("Segaki Rice", "Blessed rice that can purify a target. Instant kill Bless attack. (1 Enemy)") },
            { 0x52A, new Tuple<string, string>("Curse Paper", "A forbidden sutra used as a death curse. Instant kill Curse attack. (1 Enemy)") },
            { 0x52B, new Tuple<string, string>("Psycho Bomb", "Bursts with kinetic power when thrown. Light Psy damage. (All Enemies)") },
            { 0x52C, new Tuple<string, string>("Fission Bullet", "A bullet that explodes with telekinetic power. Medium Psy damage. (All Enemies)") },
            { 0x52D, new Tuple<string, string>("Atom Match", "Bursts with nuclear energy when thrown. Light Nuke damage. (All Enemies)") },
            { 0x52E, new Tuple<string, string>("Mental Bullet", "A bullet that explodes with nuclear energy. Medium Nuke damage. (All Enemies)") },
            { 0x532, new Tuple<string, string>("Homunculus", "This effigy oddly resembles its bearer. Protect user from 1 Bless/Curse instant kill.") },
            { 0x533, new Tuple<string, string>("Repulse Pipe", "Playing this pipe repels Shadows. Reduce encounter rate.") },
            { 0x534, new Tuple<string, string>("Attract Pipe", "Playing this pipe attracts Shadows. Raise encounter rate.") },
            { 0x535, new Tuple<string, string>("Goho-M", "Instantly teleports user to safety. Escape from current labyrinth.") },
            { 0x536, new Tuple<string, string>("Goba-K", "Instantly teleports user to the staircase. Return to floor entrance.") },
            { 0x538, new Tuple<string, string>("Sight Beacon", "A tool that can amplify FOE frequencies. Display FOE locations on current floor.") },
            { 0x539, new Tuple<string, string>("Soma", "Known as the drink of the gods. Restore all HP and SP. (Party)") },
            { 0x53A, new Tuple<string, string>("Bead Chain", "A chain of beads with a healing light. Restore all HP. (Party)") },
            { 0x53B, new Tuple<string, string>("Goho-M More", "A mysterious Goho-M that doesn't vanish after use. Escape from current labyrinth.") },
            { 0x53C, new Tuple<string, string>("Goba-K More", "A mysterious Goba-K that doesn't vanish after use. Return to floor entrance.") },
            { 0x53D, new Tuple<string, string>("Attract Horn", "Sounding this horn attracts Shadows. Greatly raise encounter rate.") },
            { 0x53E, new Tuple<string, string>("Repulse Horn", "Sounding this horn drives Shadows away. Eliminates random encounters.") },
            { 0x53F, new Tuple<string, string>("Treasure Renew", "Exploits the dungeon's volatility. Refresh Treasure Spots for current floor.") },
            { 0x541, new Tuple<string, string>("Heal Stone", "Only usable in battle. Doesn't vanish after use. Restore 30 HP. (1 Ally)") },
            { 0x542, new Tuple<string, string>("Vanish Ball", "A ball that deploys a smokescreen. Guaranteed escape (in escapable battles).") },
            { 0x543, new Tuple<string, string>("Undying Fowl", "A spicy, deep-fried thigh of a phoenix. Revive and restore 30% HP. (1 Ally)") },
            { 0x544, new Tuple<string, string>("Popcorn", "Popcorn that is seasoned with salt. Restore 30 HP. (1 Ally)") },
            { 0x545, new Tuple<string, string>("Gutsy Fries", "Fries made with healthy potatoes. Revive and restore 10% HP. (1 Ally)") },
            { 0x547, new Tuple<string, string>("Revival Stone", "Only usable in battle. Doesn't vanish after use. Revive and restore 10% HP. (1 Ally)") },
            { 0x549, new Tuple<string, string>("Honey Popcorn", "Sweet popcorn infused with honey. Restore 80 HP. (1 Ally)") },
            { 0x54A, new Tuple<string, string>("Large Hot Dog", "A hot dog long enough to feed three. Restore 60 HP. (1 Row)") },
            { 0x54B, new Tuple<string, string>("Clean Soda", "Refreshing drink that cleanses impurities. Cure status ailments. (1 Ally)") },
            { 0x54C, new Tuple<string, string>("Tropic Popcorn", "Popcorn with a tropical flavor. Restore 120 HP. (1 Ally)") },
            { 0x54D, new Tuple<string, string>("Meaty Popcorn", "Popcorn bursting with savory juices. Restore 180 HP. (1 Ally)") },
            { 0x54E, new Tuple<string, string>("Prism Popcorn", "Mysterious seven-flavor popcorn. Restore 250 HP. (1 Ally)") },
            { 0x550, new Tuple<string, string>("Blank Card", "A Persona's power can be copied onto this card. Special effect in Sacrifice Fusion.") },
            { 0x551, new Tuple<string, string>("Wild Card", "A Persona's power can be copied onto this card. Special effect in Sacrifice Fusion.") },
            { 0x552, new Tuple<string, string>("Hagakure Bowl", "Specialty of a ramen place in Iwatodai Station. Restore 50% HP. (Party)") },
            { 0x553, new Tuple<string, string>("Mega Beef Bowl", "Speciality of a Chinese diner in Inaba. Restore all HP. (1 Row)") },
            { 0x554, new Tuple<string, string>("Leblanc Curry", "Specialty of an alleyway café in Yongen-Jaya. Restore 50% SP. (Party)") },
            { 0x555, new Tuple<string, string>("Growth Incense", "Incense that unifies mind and memory. Raise level to Wild Level. (1 Ally)") },
            { 0x557, new Tuple<string, string>("Large Dual Dog", "Two long hot dogs combined into one. Restore 120 HP. (1 Row)") },
            { 0x558, new Tuple<string, string>("Giganto Dog", "A extra-long, extra-thick hot dog. Restore 180 HP. (1 Row)") },
            { 0x559, new Tuple<string, string>("Clarity Bottle", "Water that purges bodily impurities. Cure status ailments. (1 Row)") },
            { 0x55A, new Tuple<string, string>("Clarity Shower", "Vat of water that purges bodily impurities. Cure status ailments. (Party)") },
            { 0x55B, new Tuple<string, string>("Bind Cure", "A multi-pack of unbinding medicine. Cure Binds. (1 Row)") },
            { 0x55C, new Tuple<string, string>("Bind Free", "A jumbo pack of unbinding medicine. Cure Binds. (Party)") },
            { 0x55D, new Tuple<string, string>("Huge Churros", "A long arrangement of pastries. Can feed up to 3. Restore 30 HP. (1 Row)") },
            { 0x5FD, new Tuple<string, string>("Hagakure Bowl x3", "Specialty of a ramen place in Iwatodai Station. Restore 50% HP. (Party)") },
            { 0x5FE, new Tuple<string, string>("Mega Beef Bowl x3", "Speciality of a Chinese diner in Inaba. Restore all HP. (1 Row)") },
            { 0x5FF, new Tuple<string, string>("Leblanc Curry x3", "Specialty of an alleyway café in Yongen-Jaya. Restore 50% SP. (Party)") },
        };

        #endregion Consumables

        #region Skills

        #region SkillsAilment

        internal static Dictionary<int, Tuple<string, string>> SkillsAilment = new Dictionary<int, Tuple<string, string>>()
        {
            { 0, new Tuple<string, string>("EMPTY", "") },

            { 0x1053, new Tuple<string, string>("Pulinpa", "Medium chance of Confusion. (1 Row)") },
            { 0x1054, new Tuple<string, string>("Tentarafoo", "Medium chance of Confusion. (All Enemies)") },
            { 0x1055, new Tuple<string, string>("Poisma", "Medium chance of Poison. (1 Row)") },
            { 0x1056, new Tuple<string, string>("Poison Breath", "Medium chance of Poison. (All Enemies)") },
            { 0x1059, new Tuple<string, string>("Dormina", "Medium chance of Sleep. (1 Row)") },
            { 0x105A, new Tuple<string, string>("Lullaby Song", "Medium chance of Sleep. (All Enemies)") },
            { 0x105B, new Tuple<string, string>("Evil Touch", "Medium chance of Hex. (1 Row)") },
            { 0x105C, new Tuple<string, string>("Evil Smile", "Medium chance of Hex. (All Enemies)") },
            { 0x1097, new Tuple<string, string>("Scarecrow", "Medium chance of Agility Bind. (1 Row)") },
            { 0x1098, new Tuple<string, string>("Disarm", "Medium chance of Strength Bind. (1 Row)") },
            { 0x1099, new Tuple<string, string>("Makajam", "Medium chance of Magic Bind. (1 Row)") },
            { 0x110D, new Tuple<string, string>("Salome's Kiss", "High chance of Magic/Strength/Agility Bind. (1 Enemy)") },
            { 0x133F, new Tuple<string, string>("Poisma", "Medium chance of Poison. (1 Row)") },
            { 0x1340, new Tuple<string, string>("Poison Breath", "Medium chance of Poison. (All Enemies)") },
            { 0x1341, new Tuple<string, string>("Stona", "Medium chance of Petrification. (1 Enemy)") },
            { 0x1342, new Tuple<string, string>("Stone Mist", "Medium chance of Petrification. (1 Row)") },
            { 0x1343, new Tuple<string, string>("Dormina", "Medium chance of Sleep. (1 Row)") },
            { 0x1344, new Tuple<string, string>("Lullaby Song", "Medium chance of Sleep. (All Enemies)") },
            { 0x1345, new Tuple<string, string>("Pulinpa", "Medium chance of Confusion. (1 Row)") },
            { 0x1346, new Tuple<string, string>("Tentarafoo", "Medium chance of Confusion. (All Enemies)") },
            { 0x1347, new Tuple<string, string>("Perplex Flash", "Medium chance of Confusion. (1 Enemy)") },
            { 0x1349, new Tuple<string, string>("Evil Touch", "Medium chance of Hex. (1 Row)") },
            { 0x134A, new Tuple<string, string>("Evil Smile", "Medium chance of Hex. (All Enemies)") },
            { 0x134B, new Tuple<string, string>("Shibaboo", "Medium chance of Paralysis. (1 Enemy)") },
            { 0x134C, new Tuple<string, string>("Binding Cry", "Medium chance of Paralysis. (All Enemies)") },
            { 0x134D, new Tuple<string, string>("Makajam", "Medium chance of Magic Bind. (1 Row)") },
            { 0x134E, new Tuple<string, string>("Silent Song", "Medium chance of Magic Bind. (All Enemies)") },
            { 0x134F, new Tuple<string, string>("Disarm", "Medium chance of Strength Bind. (1 Row)") },
            { 0x1350, new Tuple<string, string>("Muscle Down", "Medium chance of Strength Bind. (All Enemies)") },
            { 0x1351, new Tuple<string, string>("Scarecrow", "Medium chance of Agility Bind. (1 Row)") },
            { 0x1352, new Tuple<string, string>("Spiderweb", "Medium chance of Agility Bind. (All Enemies)") },
            { 0x1445, new Tuple<string, string>("Ariadne Chain", "High chance of Magic/Strength/Agility Bind. (1 Row)") },
            { 0x15A0, new Tuple<string, string>("Sleuth Insight", "Medium chance of Magic/Strength/Agility Bind. (1 Enemy)") },
            { 0x15B4, new Tuple<string, string>("Detective's Ban", "Medium chance of Magic/Strength/Agility Bind. (1 Row)") },
        };

        #endregion SkillsAilment

        #region SkillsAlmighty

        internal static Dictionary<int, Tuple<string, string>> SkillsAlmighty = new Dictionary<int, Tuple<string, string>>()
        {
            { 0, new Tuple<string, string>("EMPTY", "") },

            { 0x1033, new Tuple<string, string>("Megido", "Medium Almighty attack. (All Enemies)") },
            { 0x1034, new Tuple<string, string>("Megidola", "Heavy Almighty attack. (All Enemies)") },
            { 0x1035, new Tuple<string, string>("Megidolaon", "Severe Almighty attack. (All Enemies)") },
            { 0x1037, new Tuple<string, string>("Black Viper", "Extreme Almighty attack. (1 Enemy)") },
            { 0x1038, new Tuple<string, string>("Morning Star", "Extreme Almighty attack. (All Enemies)") },
            { 0x10AF, new Tuple<string, string>("Photon Edge", "Heavy Almighty attack. (1 Enemy)") },
            { 0x10B0, new Tuple<string, string>("Prana", "Severe Almighty attack. (All Enemies)") },
            { 0x10C1, new Tuple<string, string>("Shinkuuha", "2 medium Almighty attack. (1 Enemy)") },
            { 0x10C2, new Tuple<string, string>("Resseiha", "2-5 medium Almighty attack to random targets. (All Enemies)") },
            { 0x10C3, new Tuple<string, string>("Pralaya", "Heavy Almighty attack, with medium chance of Instant Kill. (All Enemies)") },
            { 0x10C4, new Tuple<string, string>("Shadow Run", "Heavy Almighty attack. (All Enemies)") },
            { 0x10C5, new Tuple<string, string>("World's End", "Severe Almighty attack. (All Enemies)") },
            { 0x1125, new Tuple<string, string>("Door of Hades", "3 heavy Almighty attacks, 1 each turn. (All Enemies)") },
            { 0x118F, new Tuple<string, string>("Beast Summon", "Medium Almighty attack. (All Enemies) [Requires Circles]") },
            { 0x1190, new Tuple<string, string>("Spirit Summon", "Heavy Almighty attack. (All Enemies) [Requires Circles]") },
            { 0x132B, new Tuple<string, string>("Megido", "Medium Almighty attack. (All Enemies)") },
            { 0x132C, new Tuple<string, string>("Megidola", "Heavy Almighty attack. (All Enemies)") },
            { 0x132D, new Tuple<string, string>("Megidolaon", "Severe Almighty attack. (All Enemies)") },
        };

        #endregion SkillsAlmighty

        #region SkillsAuto

        internal static Dictionary<int, Tuple<string, string>> SkillsAuto = new Dictionary<int, Tuple<string, string>>()
        {
            { 0, new Tuple<string, string>("EMPTY", "") },
            
            { 0x1193, new Tuple<string, string>("Shura Instincts", "High chance of activating Shura Tensei when battle starts. (Auto)") },
            { 0x11B1, new Tuple<string, string>("Pain Watcher", "High chance of Pain-Eater when battle starts. (Auto)") },
            { 0x11D1, new Tuple<string, string>("Orgia Mastery", "Raise damage each time Aigis overheats. (Auto)") },
            { 0x11D2, new Tuple<string, string>("Spy Arts", "Restore SP when striking an enemy's weakness. (Auto)") },
            { 0x11D3, new Tuple<string, string>("Spy Expertise", "Restore SP when striking an enemy's weakness. (Auto)") },
            { 0x11D4, new Tuple<string, string>("Pose", "Restore SP after defeating an enemy. (Auto)") },
            { 0x11D5, new Tuple<string, string>("Heroic Pose", "Greatly restore SP after defeating an enemy. (Auto)") },
            { 0x11D6, new Tuple<string, string>("Hermit Arts", "Restore SP when protecting an ally. (Auto)") },
            { 0x11D7, new Tuple<string, string>("Hermit Mastery", "Greatly restore SP when protecting an ally. (Auto)") },
            { 0x11D8, new Tuple<string, string>("Double Attack", "Medium chance attack skills will activate twice. (Auto)") },
            { 0x11D9, new Tuple<string, string>("Deadly Vanguard", "When attacking an enemy, allies deal more damage to target. (Auto)") },
            { 0x11DA, new Tuple<string, string>("Empress Arts", "Restore SP when inflicting a Bind. (Auto)") },
            { 0x11DB, new Tuple<string, string>("Empress Mastery", "Greatly restore SP when inflicting a Bind. (Auto)") },
            { 0x11DC, new Tuple<string, string>("Squire Card", "Raise attack power for Fire/Ice/Elec/Wind/Psy/Nuke. (Auto)") },
            { 0x11DD, new Tuple<string, string>("Fool Card", "Raise attack power for Fire/Ice/Elec/Wind/Psy/Nuke. (Auto)") },
            { 0x11DE, new Tuple<string, string>("Soldier Arts", "Restore SP after normal attacks. (Auto)") },
            { 0x11DF, new Tuple<string, string>("Soldier Mastery", "Greatly restore SP after normal attacks. (Auto)") },
            { 0x11E0, new Tuple<string, string>("Kung Fu Arts", "Lower HP cost of Phys skills. (Auto)") },
            { 0x11E1, new Tuple<string, string>("Kung Fu Mastery", "Greatly lower HP cost of Phys skills. (Auto)") },
            { 0x11E2, new Tuple<string, string>("Wild Arts", "Restore SP when evading an attack. (Auto)") },
            { 0x11E3, new Tuple<string, string>("Wild Arts+", "Restore SP when evading an attack. (Auto)") },
            { 0x11E4, new Tuple<string, string>("Brave Soul", "Restore SP at the end of turn if at full HP & in front row. (Auto)") },
            { 0x11E5, new Tuple<string, string>("Brave Soul+", "Restore SP at the end of turn if at full HP & in front row. (Auto)") },
            { 0x11E6, new Tuple<string, string>("Royal Song", "Slightly restore SP while walking in labyrinth. (Auto)") },
            { 0x11E7, new Tuple<string, string>("Circle Mastery", "Restore SP when Circle ends. (Auto)") },
            { 0x11E8, new Tuple<string, string>("Circle Virtuoso", "Greatly restore SP when Circle ends. (Auto)") },
            { 0x11E9, new Tuple<string, string>("Brawler Arts", "Restore SP when attacked and in the front row. (Auto)") },
            { 0x11EA, new Tuple<string, string>("Brawler Arts+", "Greatly restore SP when attacked and in the front row. (Auto)") },
            { 0x11EB, new Tuple<string, string>("Life Support", "Restore HP after battle. (Party) (Auto)") },
            { 0x11EC, new Tuple<string, string>("Life Support+", "Greatly restore HP after battle. (Party) (Auto)") },
            { 0x11ED, new Tuple<string, string>("Stance Master", "Stance effects last 1 turn longer. (Auto)") },
            { 0x11EE, new Tuple<string, string>("Stance Virtuoso", "Stance effects last 1 turn longer. (Auto)") },
            { 0x11EF, new Tuple<string, string>("Raging Lion", "Restore HP after normal attacks. (Auto)") },
            { 0x11F0, new Tuple<string, string>("Raging Lion+", "Greatly restore HP after normal attacks. (Auto)") },
            { 0x11F1, new Tuple<string, string>("Heroic Gemini", "Low chance attack skills will activate twice. (Auto)") },
            { 0x11F2, new Tuple<string, string>("Golden Gemini", "High chance attack skills will activate twice. (Auto)") },
            { 0x11F3, new Tuple<string, string>("Runic Shield", "High chance to null Fire/Ice/Elec/Wind/Psy/Nuke damage to user's row. (Auto)") },
            { 0x11F4, new Tuple<string, string>("Aegis Shield", "Med. chance to null Fire/Ice/Elec/Wind/Psy/Nuke damage to user's row. (Auto)") },
            { 0x11F5, new Tuple<string, string>("Swordbreaker", "Medium chance of lowering Phys damage to the user's row. (Auto)") },
            { 0x11F6, new Tuple<string, string>("Bind Arts", "Raise chance of inflicting Binds. (Auto)") },
            { 0x11F7, new Tuple<string, string>("Bind Arts+", "Greatly raise chance of inflicting Binds. (Auto)") },
            { 0x11F8, new Tuple<string, string>("First Star", "Raise damage when attacking before all enemies act. (Auto)") },
            { 0x11F9, new Tuple<string, string>("Challenger Arts", "Slightly raise attack against enemies with higher HP. (Auto)") },
            { 0x11FA, new Tuple<string, string>("Challenger Arts+", "Moderately raise attack against enemies with higher HP. (Auto)") },
            { 0x11FB, new Tuple<string, string>("Uprising", "Greatly raise attack against enemies with higher HP. (Auto)") },
            { 0x11FC, new Tuple<string, string>("Restoring Touch", "Raise natural recovery speed from ailments and Binds. (Auto)") },
            { 0x11FD, new Tuple<string, string>("Lucky Star", "Raise critical rate. (Auto)") },
            { 0x11FE, new Tuple<string, string>("Conqueror Title", "Greatly raise critical rate. (Auto)") },
            { 0x11FF, new Tuple<string, string>("Healing Finger", "Slightly raise restorative power of HP healing skills. (Auto)") },
            { 0x1202, new Tuple<string, string>("Colossal Swing", "Low chance to add splash to single-target attack skills. (Auto)") },
            { 0x1203, new Tuple<string, string>("Whirlwind Swing", "Medium chance to add splash to single-target attack skills. (Auto)") },
            { 0x1204, new Tuple<string, string>("Golden Link", "Raise Link damage based on number of Links used. (Auto)") },
            { 0x1205, new Tuple<string, string>("Combo Arts", "Low chance of another attack after normal attack. (Auto)") },
            { 0x1206, new Tuple<string, string>("Combo Arts+", "Medium chance of another attack after normal attack. (Auto)") },
            { 0x1207, new Tuple<string, string>("Dodge Arts", "Slightly raise evasion against all attacks. (Auto)") },
            { 0x1208, new Tuple<string, string>("Dodge Arts+", "Moderately raise evasion against all attacks. (Auto)") },
            { 0x1209, new Tuple<string, string>("Trick Step", "Greatly raise evasion against all attacks. (Auto)") },
            { 0x120A, new Tuple<string, string>("Immunity Buffer", "High chance of nullifying Phys attacks. (Auto)") },
            { 0x120B, new Tuple<string, string>("Lemegeton", "Raise power of summons in Circles. (Auto)") },
            { 0x120C, new Tuple<string, string>("Repulse Mist", "Medium chance to nullify all ailments and Binds to user's row. (Auto)") },
            { 0x120D, new Tuple<string, string>("Absorb Fire", "Absorb damage from Fire attacks. (Auto)") },
            { 0x120E, new Tuple<string, string>("Absorb Wind", "Absorb damage from Wind attacks. (Auto)") },
            { 0x120F, new Tuple<string, string>("Absorb Ice", "Absorb damage from Ice attacks. (Auto)") },
            { 0x1210, new Tuple<string, string>("Absorb Elec", "Absorb damage from Elec attacks. (Auto)") },
            { 0x1211, new Tuple<string, string>("Healing Hand", "Moderately raise restorative power of HP healing skills. (Auto)") },
            { 0x1212, new Tuple<string, string>("Fire Boost", "Slightly raise Fire attack power. (Auto)") },
            { 0x1213, new Tuple<string, string>("Ice Boost", "Slightly raise Ice attack power. (Auto)") },
            { 0x1214, new Tuple<string, string>("Elec Boost", "Slightly raise Elec attack power. (Auto)") },
            { 0x1215, new Tuple<string, string>("Wind Boost", "Slightly raise Wind attack power. (Auto)") },
            { 0x1216, new Tuple<string, string>("Fire Amp", "Moderately raise Fire attack power. (Auto)") },
            { 0x1217, new Tuple<string, string>("Ice Amp", "Moderately raise Ice attack power. (Auto)") },
            { 0x1218, new Tuple<string, string>("Elec Amp", "Moderately raise Elec attack power. (Auto)") },
            { 0x1219, new Tuple<string, string>("Wind Amp", "Moderately raise Wind attack power. (Auto)") },
            { 0x121A, new Tuple<string, string>("Devotion Arts", "Lower SP cost of skills. (Auto)") },
            { 0x121B, new Tuple<string, string>("Devotion Mastery", "Greatly lower SP cost of skills. (Auto)") },
            { 0x121C, new Tuple<string, string>("Idol Hands", "Greatly raise restorative power of HP healing skills. (Auto)") },
            { 0x121D, new Tuple<string, string>("Ambush Arts", "Raise damage when attacking a target inflicted with an ailment. (Auto)") },
            { 0x121E, new Tuple<string, string>("Ambush Arts+", "Greatly raise damage when attacking a target inflicted with an ailment. (Auto)") },
            { 0x121F, new Tuple<string, string>("Ailment Arts", "Raise chance of inflicting ailments. (Auto)") },
            { 0x1220, new Tuple<string, string>("Ailment Arts+", "Greatly raise chance of inflicting ailments. (Auto)") },
            { 0x1221, new Tuple<string, string>("Circle Recovery", "Slightly restore HP each turn. (Party) (Auto) [Requires Circles]") },
            { 0x1222, new Tuple<string, string>("Judgment Card", "Greatly raise attack power for Fire/Ice/Elec/Wind/Psy/Nuke. (Auto)") },
            { 0x1223, new Tuple<string, string>("Inferno Boost", "Greatly raise Fire attack power. (Auto)") },
            { 0x1224, new Tuple<string, string>("Tornado Boost", "Greatly raise Wind attack power. (Auto)") },
            { 0x1225, new Tuple<string, string>("SP Focus", "Medium chance to lower SP cost of skills to 0. (Auto)") },
            { 0x1226, new Tuple<string, string>("Critical Arts", "Raise damage when striking an enemy's weakness. (Auto)") },
            { 0x1227, new Tuple<string, string>("Critical Arts+", "Greatly raise damage when striking an enemy's weakness. (Auto)") },
            { 0x1228, new Tuple<string, string>("Queen's Pierce", "Greatly raise damage when striking an enemy's weakness. (Auto)") },
            { 0x1229, new Tuple<string, string>("Attack Boost I", "Slightly raise Phys attack. (Auto)") },
            { 0x122A, new Tuple<string, string>("Attack Boost II", "Raise Phys attack. (Auto)") },
            { 0x122B, new Tuple<string, string>("Attack Boost III", "Greatly raise Phys attack. (Auto)") },
            { 0x122C, new Tuple<string, string>("Defense Boost I", "Slightly raise Phys defense. (Auto)") },
            { 0x122D, new Tuple<string, string>("Defense Boost II", "Raise Phys defense. (Auto)") },
            { 0x122E, new Tuple<string, string>("Defense Boost III", "Greatly raise Phys defense. (Auto)") },
            { 0x122F, new Tuple<string, string>("Affinity Boost I", "Slightly raise affinity resistance. (Auto)") },
            { 0x1230, new Tuple<string, string>("Affinity Boost II", "Raise affinity resistance. (Auto)") },
            { 0x1231, new Tuple<string, string>("Affinity Boost III", "Greatly raise affinity resistance. (Auto)") },
            { 0x1232, new Tuple<string, string>("Serene Stroll", "Slightly restore SP while walking in labyrinth. (Auto)") },
            { 0x1233, new Tuple<string, string>("Serene Quest", "Restore HP/SP while walking in a labyrinth. (Auto)") },
            { 0x1234, new Tuple<string, string>("Serene Pilgrimage", "Restore HP/SP while walking in a labyrinth. (Auto)") },
            { 0x1235, new Tuple<string, string>("Double Link", "Slightly raise Link skills and chance of Link damage afterwards. (Auto)") },
            { 0x1236, new Tuple<string, string>("Powerhouse", "Low chance of knockdown. (Auto)") },
            { 0x1237, new Tuple<string, string>("Powerhouse+", "Medium chance of knockdown. (Auto)") },
            { 0x1238, new Tuple<string, string>("Solitary Noblesse", "High chance of knockdown. (Auto)") },
            { 0x1239, new Tuple<string, string>("Magic Boost I", "Slightly raise affinity attack. (Auto)") },
            { 0x123A, new Tuple<string, string>("Magic Boost II", "Raise affinity attack. (Auto)") },
            { 0x123B, new Tuple<string, string>("Magic Boost III", "Greatly raise affinity attack. (Auto)") },
            { 0x123C, new Tuple<string, string>("HP Boost", "Slightly raise max HP. (Auto)") },
            { 0x123D, new Tuple<string, string>("HP Amp", "Raise max HP. (Auto)") },
            { 0x123E, new Tuple<string, string>("SP Boost", "Slightly raise max SP. (Auto)") },
            { 0x123F, new Tuple<string, string>("SP Amp", "Raise max SP. (Auto)") },
            { 0x1240, new Tuple<string, string>("Agility Boost", "Slightly raise act speed, accuracy, and evasion. (Auto)") },
            { 0x1241, new Tuple<string, string>("Agility Amp", "Raise act speed, accuracy, and evasion. (Auto)") },
            { 0x1242, new Tuple<string, string>("Resist Fire", "Raise Fire resistance. (Auto)") },
            { 0x1243, new Tuple<string, string>("Resist Ice", "Raise Ice resistance. (Auto)") },
            { 0x1244, new Tuple<string, string>("Resist Elec", "Raise Elec resistance. (Auto)") },
            { 0x1245, new Tuple<string, string>("Resist Wind", "Raise Wind resistance. (Auto)") },
            { 0x1246, new Tuple<string, string>("Resist Bless", "Raise Bless resistance. (Auto)") },
            { 0x1247, new Tuple<string, string>("Resist Curse", "Raise Curse resistance. (Auto)") },
            { 0x1248, new Tuple<string, string>("Resist Phys", "Raise Phys resistance. (Auto)") },
            { 0x1249, new Tuple<string, string>("Resist Psy", "Raise Psy resistance. (Auto)") },
            { 0x124A, new Tuple<string, string>("Resist Nuke", "Raise Nuke resistance. (Auto)") },
            { 0x124B, new Tuple<string, string>("Null Petrify", "Nullifies Petrification status ailment. (Auto)") },
            { 0x124C, new Tuple<string, string>("Null Sleep", "Nullifies Sleep status ailment. (Auto)") },
            { 0x124D, new Tuple<string, string>("Null Confusion", "Nullifies Confusion status ailment. (Auto)") },
            { 0x124E, new Tuple<string, string>("Null Poison", "Nullifies Poison status ailment. (Auto)") },
            { 0x124F, new Tuple<string, string>("Endure", "Survive one lethal attack with 1 HP instead of being KOed. (Auto)") },
            { 0x1250, new Tuple<string, string>("Null Hex", "Nullifies Hex status ailment. (Auto)") },
            { 0x1251, new Tuple<string, string>("Null Paralysis", "Nullifies Paralysis status ailment. (Auto)") },
            { 0x1252, new Tuple<string, string>("Keburi no Sue", "Negates Phys attack power penalty of attacking from back row. (Auto)") },
            { 0x1253, new Tuple<string, string>("Masochist Lash", "Additional attack when a target is bound. (Auto)") },
            { 0x1255, new Tuple<string, string>("Null M-Bind", "Nullifies Magic Bind. (Auto)") },
            { 0x1256, new Tuple<string, string>("Null S-Bind", "Nullifies Strength Bind. (Auto)") },
            { 0x1257, new Tuple<string, string>("Null A-Bind", "Nullifies Agility Bind. (Auto)") },
            { 0x1258, new Tuple<string, string>("Demon's Cut", "Slightly raise Cut attack power. (Auto)") },
            { 0x1259, new Tuple<string, string>("Demon's Bash", "Slightly raise Bash attack power. (Auto)") },
            { 0x125A, new Tuple<string, string>("Demon's Stab", "Slightly raise Stab attack power. (Auto)") },
            { 0x125B, new Tuple<string, string>("God's Cut", "Raise Cut attack power. (Auto)") },
            { 0x125C, new Tuple<string, string>("God's Bash", "Raise Bash attack power. (Auto)") },
            { 0x125D, new Tuple<string, string>("God's Stab", "Raise Stab attack power. (Auto)") },
            { 0x125E, new Tuple<string, string>("SP Recoil", "Slightly restore SP when enemy is inflicted with ailment/Bind. (Auto)") },
            { 0x125F, new Tuple<string, string>("SP Redeemer", "Restore SP when enemy is inflicted with ailment/Bind. (Auto)") },
            { 0x12D2, new Tuple<string, string>("Null Ailments", "High chance of halving Phys damage versus the user's row.") },
            { 0x12D3, new Tuple<string, string>("Null Binds", "Amplify HP-restoring effects. [Greater inrease with more buffs]") },
            { 0x12D5, new Tuple<string, string>("Return from Yomi", "Medium chance of HP restore when near KO. (Auto)") },
            { 0x12D6, new Tuple<string, string>("Triple Link", "Moderately raise Link skills and chance of Link damage afterwards. (Auto)") },
            { 0x12D7, new Tuple<string, string>("Quadruple Link", "Greatly raise Link skills and chance of Link damage afterwards. (Auto)") },
            { 0x12D8, new Tuple<string, string>("Infinite Link", "Intensely raise Link skills and chance of Link damage afterwards. (Auto)") },
            { 0x12D9, new Tuple<string, string>("Magic Aura", "Restore SP after defeating an enemy by striking its weakness. (Auto)") },
            { 0x12DA, new Tuple<string, string>("Skill Boost", "Raise damage of Phys skills. (Auto)") },
            { 0x12DC, new Tuple<string, string>("Circle Aura", "Restore SP each turn. (Auto) [Requires Circles]") },
            { 0x12DD, new Tuple<string, string>("Healing Surge", "Amplify HP-restoring effects, more effective when with buffs. (Auto)") },
            { 0x12E3, new Tuple<string, string>("Nuclear Boost", "Slightly raise Nuke attack power. (Auto)") },
            { 0x12E4, new Tuple<string, string>("Psy Boost", "Slightly raise Psy attack power. (Auto)") },
            { 0x12E5, new Tuple<string, string>("Bless Boost", "Slightly raise Bless attack power. (Auto)") },
            { 0x12E6, new Tuple<string, string>("Curse Boost", "Slightly raise Curse attack power. (Auto)") },
            { 0x12E7, new Tuple<string, string>("Nuclear Amp", "Moderately raise Nuke attack power. (Auto)") },
            { 0x12E8, new Tuple<string, string>("Psy Amp", "Moderately raise Psy attack power. (Auto)") },
            { 0x12E9, new Tuple<string, string>("Bless Amp", "Moderately raise Bless attack power. (Auto)") },
            { 0x12EA, new Tuple<string, string>("Curse Amp", "Moderately raise Curse attack power. (Auto)") },
            { 0x12F4, new Tuple<string, string>("Ailment Boost", "Slightly raise chance of inflicting ailments/Binds. (Auto)") },
            { 0x12F5, new Tuple<string, string>("Ailment Surge", "Raise chance of inflicting ailments/Binds. (Auto)") },
            { 0x12F6, new Tuple<string, string>("Raging Fists", "Slightly raise critical hit damage. (Auto)") },
            { 0x12F7, new Tuple<string, string>("Furious Fists", "Raise critical hit damage. (Auto)") },
            { 0x12F8, new Tuple<string, string>("Bound Arts", "Raise critical rate when target is bound. (Auto)") },
            { 0x12F9, new Tuple<string, string>("Bound Arts+", "Greatly raise critical rate when target is bound. (Auto)") },
            { 0x12FA, new Tuple<string, string>("Mistress Lash", "Additional heavy attack when a target is bound. (Auto)") },
            { 0x12FC, new Tuple<string, string>("Protecting Sword", "High chance of decreasing Phys damage to the user's row. (Auto)") },
            { 0x1431, new Tuple<string, string>("Fire Boost", "Slightly raise Fire attack power. (Auto)") },
            { 0x1432, new Tuple<string, string>("Ice Boost", "Slightly raise Ice attack power. (Auto)") },
            { 0x1433, new Tuple<string, string>("Elec Boost", "Slightly raise Elec attack power. (Auto)") },
            { 0x1434, new Tuple<string, string>("Wind Boost", "Slightly raise Wind attack power. (Auto)") },
            { 0x1435, new Tuple<string, string>("Trick Step", "Greatly raise evasion against all attacks. (Auto)") },
            { 0x143B, new Tuple<string, string>("Wide Attack", "Low chance to add splash to single-target attack skills. (Auto)") },
            { 0x143C, new Tuple<string, string>("Wide Attack II", "Medium chance to add splash to single-target attack skills. (Auto)") },
            { 0x143D, new Tuple<string, string>("Wide Attack III", "High chance to add splash to single-target attack skills. (Auto)") },
            { 0x143E, new Tuple<string, string>("Double Break", "Low chance attack skills will activate twice. (Auto)") },
            { 0x143F, new Tuple<string, string>("Double Break II", "Medium chance attack skills will activate twice. (Auto)") },
            { 0x1440, new Tuple<string, string>("Double Break III", "High chance attack skills will activate twice. (Auto)") },
            { 0x1444, new Tuple<string, string>("Brutal Arm", "High chance to add splash to single-target attack skills. (Auto)") },
            { 0x1446, new Tuple<string, string>("Infernal Wings", "High chance attack skills will activate twice. (Auto)") },
            { 0x1449, new Tuple<string, string>("Elec Turbo", "Greatly raise Elec attack power. (Auto)") },
            { 0x144A, new Tuple<string, string>("Ice Turbo", "Greatly raise Ice attack power. (Auto)") },
            { 0x144B, new Tuple<string, string>("Psy Turbo", "Greatly raise Psy attack power. (Auto)") },
            { 0x144C, new Tuple<string, string>("Nuke Turbo", "Greatly raise Nuke attack power. (Auto)") },
            { 0x144D, new Tuple<string, string>("Bless Turbo", "Greatly raise Bless attack power. (Auto)") },
            { 0x144E, new Tuple<string, string>("Curse Turbo", "Greatly raise Curse attack power. (Auto)") },
            { 0x1450, new Tuple<string, string>("Resonance", "Raise Link damage based on number of Links used. (Auto)") },
            { 0x1451, new Tuple<string, string>("Telepathic Bond", "Greatly raise Link damage based on number of Links used. (Auto)") },
            { 0x1452, new Tuple<string, string>("Aspiring Heart", "Raise Link damage based on number of Links used. (Auto)") },
            { 0x1453, new Tuple<string, string>("Great Unity Link", "Greatly raise Link damage based on number of Links used. (Auto)") },
            { 0x1459, new Tuple<string, string>("Psy Drain", "Absorb damage from Psy attacks. (Auto)") },
            { 0x145A, new Tuple<string, string>("Nuclear Drain", "Absorb damage from Nuke attacks. (Auto)") },
            { 0x15A9, new Tuple<string, string>("Empress Pierce", "Intensely raise damage when striking an enemy's weakness. (Auto)") },
            { 0x15B0, new Tuple<string, string>("Counter Style", "Low chance of Death Counter when battle starts. (Auto)") },
            { 0x15B1, new Tuple<string, string>("Revenge Style", "Medium chance of Death Counter when battle starts. (Auto)") },
            { 0x15B2, new Tuple<string, string>("Snacking Art", "Slightly restore HP at the end of the turn. (Auto)") },
            { 0x15B3, new Tuple<string, string>("Snack Expertise", "Greatly restore HP at the end of the turn. (Auto)") },
            { 0x15B5, new Tuple<string, string>("Kitty Step", "Moderately raise evasion against all attacks. (Auto)") },
            { 0x15B6, new Tuple<string, string>("Axe Guard", "Low chance of nullifying Phys attacks. (Auto)") },
            { 0x15B7, new Tuple<string, string>("Illusive Dance", "Moderately raise evasion against all attacks. (Auto)") },
            { 0x15B8, new Tuple<string, string>("Mirage Dance", "Greatly raise evasion against all attacks. (Auto)") },
            { 0x15B9, new Tuple<string, string>("Immunity Bod", "Low chance of nullifying phys attacks. (Auto)") },
            { 0x15BA, new Tuple<string, string>("Empress Pierce", "Intensely raise damage when striking an enemy's weakness. (Auto)") },
            { 0x15BB, new Tuple<string, string>("Paradise Stroll", "Moderately restore SP while walking in labyrinth. (Auto)") },
            { 0x15BC, new Tuple<string, string>("Veteran's Aura", "High chance of knockdown. (Auto)") },
            { 0x15BD, new Tuple<string, string>("Aegis Shielding", "High chance of null Fire/Ice/Elec/Wind/Psy/Nuke damage to user's row. (Auto)") },
            { 0x15BE, new Tuple<string, string>("Regenerate 1", "Slightly restore HP at the end of the turn. (Auto)") },
            { 0x15BF, new Tuple<string, string>("Regenerate 2", "Moderately restore HP at the end of the turn. (Auto)") },
            { 0x15C0, new Tuple<string, string>("Regenerate 3", "Greatly restore HP at the end of the turn. (Auto)") },
            { 0x15C1, new Tuple<string, string>("Invigorate 1", "Slightly restore SP at the end of the turn. (Auto)") },
            { 0x15C2, new Tuple<string, string>("Invigorate 2", "Moderately restore SP at the end of the turn. (Auto)") },
            { 0x15C3, new Tuple<string, string>("Invigorate 3", "Greatly restore SP at the end of the turn. (Auto)") },
            { 0x15CA, new Tuple<string, string>("Axe Bulwark", "Medium chance of nullifying phys attacks. (Auto)") },
            { 0x15CD, new Tuple<string, string>("Invincible Bod", "Medium chance of nullifying phys attacks. (Auto)") },
            { 0x15CE, new Tuple<string, string>("Power Knack", "Increase St by 2. (Auto)") },
            { 0x15D1, new Tuple<string, string>("Magic Knack", "Increase Ma by 2. (Auto)") },
            { 0x15D4, new Tuple<string, string>("Endure Knack", "Increase En by 2. (Auto)") },
            { 0x15D7, new Tuple<string, string>("Agility Knack", "Increase Ag by 2. (Auto)") },
            { 0x15DA, new Tuple<string, string>("Luck Knack", "Increase Lu by 2. (Auto)") },
        };

        #endregion SkillsAuto

        #region SkillsBless

        internal static Dictionary<int, Tuple<string, string>> SkillsBless = new Dictionary<int, Tuple<string, string>>()
        {
            { 0, new Tuple<string, string>("EMPTY", "") },

            { 0x103D, new Tuple<string, string>("Hama", "Low chance of Instant Kill. (1 Enemy) [Bless-based attack]") },
            { 0x103E, new Tuple<string, string>("Hamaon", "Medium chance of Instant Kill. (1 Enemy) [Bless-based attack]") },
            { 0x103F, new Tuple<string, string>("Mahama", "Low chance of Instant Kill. (All Enemies) [Bless-based attack]") },
            { 0x1040, new Tuple<string, string>("Mahamaon", "Medium chance of Instant Kill. (All Enemies) [Bless-based attack]") },
            { 0x1043, new Tuple<string, string>("Samsara", "High chance of Instant Kill. (All Enemies) [Bless-based attack]") },
            { 0x1331, new Tuple<string, string>("Hama", "Low chance of Instant Kill. (1 Enemy) [Bless-based attack]") },
            { 0x1332, new Tuple<string, string>("Hamaon", "Medium chance of Instant Kill. (1 Enemy) [Bless-based attack]") },
            { 0x1333, new Tuple<string, string>("Mahama", "Low chance of Instant Kill. (All Enemies) [Bless-based attack]") },
            { 0x1334, new Tuple<string, string>("Mahamaon", "Medium chance of Instant Kill. (All Enemies) [Bless-based attack]") },
            { 0x1356, new Tuple<string, string>("Kouga", "Medium Bless attack. (1 Enemy)") },
            { 0x1357, new Tuple<string, string>("Kougaon", "Heavy Bless attack. (1 Enemy)") },
            { 0x136F, new Tuple<string, string>("God's Judgment", "Bless attack dealing damage equal to half of enemy's current HP. (1 Enemy)") },
            { 0x1560, new Tuple<string, string>("Kouha", "Light Bless attack. (1 Enemy)") },
            { 0x1561, new Tuple<string, string>("Kouga", "Medium Bless attack. (1 Enemy)") },
            { 0x1562, new Tuple<string, string>("Kougaon", "Heavy Bless attack. (1 Enemy)") },
            { 0x1563, new Tuple<string, string>("Makouha", "Light Bless attack. (All Enemies)") },
            { 0x1564, new Tuple<string, string>("Makouga", "Medium Bless attack. (All Enemies)") },
            { 0x1565, new Tuple<string, string>("Makougaon", "Heavy Bless attack. (All Enemies)") },
        };

        #endregion SkillsBless

        #region SkillsBuff

        internal static Dictionary<int, Tuple<string, string>> SkillsBuff = new Dictionary<int, Tuple<string, string>>()
        {
            { 0, new Tuple<string, string>("EMPTY", "") },

            { 0x10E6, new Tuple<string, string>("Bestial Roar", "Act first and raise attack for 3 turns. (Self)") },
            { 0x10E7, new Tuple<string, string>("Dragon Cry", "Act first and greatly raise attack for 3 turns. (Self)") },
            { 0x10E8, new Tuple<string, string>("Improved Link", "Raise Link skills and chance of Link damage for 3 turns. (Self)") },
            { 0x10E9, new Tuple<string, string>("Master Link", "Raise Link skills and chance of Link damage for 5 turns. (Self)") },
            { 0x10F1, new Tuple<string, string>("Holy Blessing", "Nullifies an ailment and Bind once for 3 turns. (Party)") },
            { 0x10F2, new Tuple<string, string>("Warrior Song", "Raise attack for 3 turns. (1 Row)") },
            { 0x10F3, new Tuple<string, string>("Strike Guard", "Raise defense for 3 turns. (1 Row)") },
            { 0x10F4, new Tuple<string, string>("Rebel Vanguard", "Raise critical rate for 3 turns. (1 Row)") },
            { 0x10F5, new Tuple<string, string>("Critical Eye", "Raise critical rate for 3 turns. (Self)") },
            { 0x10F6, new Tuple<string, string>("Revolt Vanguard", "Raise critical rate for 2 turns. (1 Row)") },
            { 0x10F7, new Tuple<string, string>("Matarukaja", "Raise attack for 3 turns. (Party)") },
            { 0x10F8, new Tuple<string, string>("Marakukaja", "Raise defense for 3 turns. (Party)") },
            { 0x10F9, new Tuple<string, string>("Masukukaja", "Raise accuracy and evasion for 3 turns. (Party)") },
            { 0x10FA, new Tuple<string, string>("Power Charge", "Triples Phys attack of next hit for 3 turns. (Self)") },
            { 0x10FB, new Tuple<string, string>("Mind Charge", "Triples magic attack of next hit for 3 turns. (Self)") },
            { 0x1105, new Tuple<string, string>("Matarunda", "Lower attack for 3 turns. (All Enemies)") },
            { 0x1106, new Tuple<string, string>("Marakunda", "Lower defense for 3 turns. (All Enemies)") },
            { 0x1107, new Tuple<string, string>("Masukunda", "Lower accuracy and evasion for 3 turns. (All Enemies)") },
            { 0x1109, new Tuple<string, string>("Fury Order", "Triples Phys attack of next hit for 3 turns. (Party)") },
            { 0x110A, new Tuple<string, string>("Focus Order", "Triples magic attack of next hit for 3 turns. (Party)") },
            { 0x110C, new Tuple<string, string>("Binding Aura", "Extend timers on ailments and Binds. (All Enemies)") },
            { 0x1111, new Tuple<string, string>("Dekaja", "Remove buffs. (All Enemies)") },
            { 0x1112, new Tuple<string, string>("Dekunda", "Remove debuffs. (Party)") },
            { 0x1114, new Tuple<string, string>("Psy Screen", "Raise Psy resistance for 3 turns. (Party)") },
            { 0x1115, new Tuple<string, string>("Nuclear Screen", "Raise Nuke resistance for 3 turns. (Party)") },
            { 0x1116, new Tuple<string, string>("Fire Screen", "Raise Fire resistance for 3 turns. (Party)") },
            { 0x1117, new Tuple<string, string>("Wind Screen", "Raise Wind resistance for 3 turns. (Party)") },
            { 0x1118, new Tuple<string, string>("Ice Screen", "Raise Ice resistance for 3 turns. (Party)") },
            { 0x1119, new Tuple<string, string>("Elec Screen", "Raise Elec resistance for 3 turns. (Party)") },
            { 0x111A, new Tuple<string, string>("Bless Screen", "Raise Bless resistance for 3 turns. (Party)") },
            { 0x111B, new Tuple<string, string>("Curse Screen", "Raise Curse resistance for 3 turns. (Party)") },
            { 0x111C, new Tuple<string, string>("Tarukaja", "Raise attack for 3 turns. (1 Ally)") },
            { 0x111D, new Tuple<string, string>("Rakukaja", "Raise defense for 3 turns. (1 Ally)") },
            { 0x111E, new Tuple<string, string>("Sukukaja", "Raise accuracy and evasion for 3 turns. (1 Ally)") },
            { 0x111F, new Tuple<string, string>("Tarunda", "Lower attack for 3 turns. (1 Enemy)") },
            { 0x1120, new Tuple<string, string>("Rakunda", "Lower defense for 3 turns. (1 Enemy)") },
            { 0x1121, new Tuple<string, string>("Sukunda", "Lower accuracy and evasion for 3 turns. (1 Enemy)") },
            { 0x1122, new Tuple<string, string>("Debilitate", "Lower attack/defense and accuracy/evasion for 3 turns. (1 Enemy)") },
            { 0x1123, new Tuple<string, string>("Heat Riser", "Raise attack/defense and accuracy/evasion for 3 turns. (1 Ally)") },
            { 0x1124, new Tuple<string, string>("True Critical Eye", "Raise critical rate for 3 turns. (Party)") },
            { 0x1191, new Tuple<string, string>("Shura Tensei", "Greatly lower HP each turn, but greatly raise attack while active. (Self)") },
            { 0x1192, new Tuple<string, string>("Shura Revert", "Ends Shura Tensei and restore HP. (Self)") },
            { 0x1194, new Tuple<string, string>("Orgia Mode", "Greatly raise attack. [Will overheat after a few turns]") },
            { 0x1195, new Tuple<string, string>("Mode Disengage", "Deactivate Orgia Mode.") },
            { 0x119D, new Tuple<string, string>("Death Counter", "Counter whenever user's row is attacked for 3 turns. (Self)") },
            { 0x119F, new Tuple<string, string>("Death Chaser", "May follow-up user's row's attacks for 3 turns. (Self)") },
            { 0x11A1, new Tuple<string, string>("Fierce Stance", "Medium chance allies may follow-up attacks for 3 turns. (User's Row)") },
            { 0x12EB, new Tuple<string, string>("Kunai Dance", "May greatly follow-up user's row's attacks for 3 turns. (Self)") },
            { 0x12ED, new Tuple<string, string>("Intercept Fire", "High chance of nullifying Phys attacks for 3 turns. (1 Row)") },
            { 0x12EF, new Tuple<string, string>("Called Shot", "Powerful counter whenever user's row is attacked for 3 turns. (Self)") },
            { 0x12F1, new Tuple<string, string>("Sonic Rush", "Attack skills targeting enemies activate twice for 3 turns. (Self)") },
            { 0x13E5, new Tuple<string, string>("Bestial Roar", "Act first and raise attack for 3 turns. (Self)") },
            { 0x13F2, new Tuple<string, string>("Matarukaja", "Raise attack for 3 turns. (Party)") },
            { 0x13F3, new Tuple<string, string>("Marakukaja", "Raise defense for 3 turns. (Party)") },
            { 0x13F4, new Tuple<string, string>("Masukukaja", "Raise accuracy and evasion for 3 turns. (Party)") },
            { 0x13F5, new Tuple<string, string>("Power Charge", "Triples Phys attack of next hit for 3 turns. (Self)") },
            { 0x13F6, new Tuple<string, string>("Mind Charge", "Triples magic attack of next hit for 3 turns. (Self)") },
            { 0x13F7, new Tuple<string, string>("Fire Corrosion", "Lower Fire resistance for 3 turns. (All Enemies)") },
            { 0x13F8, new Tuple<string, string>("Ice Corrosion", "Lower Ice resistance for 3 turns. (All Enemies)") },
            { 0x13F9, new Tuple<string, string>("Elec Corrosion", "Lower Elec resistance for 3 turns. (All Enemies)") },
            { 0x13FA, new Tuple<string, string>("Wind Corrosion", "Lower Wind resistance for 3 turns. (All Enemies)") },
            { 0x13FD, new Tuple<string, string>("Matarunda", "Lower attack for 3 turns. (All Enemies)") },
            { 0x13FE, new Tuple<string, string>("Marakunda", "Lower defense for 3 turns. (All Enemies)") },
            { 0x13FF, new Tuple<string, string>("Masukunda", "Lower accuracy and evasion for 3 turns. (All Enemies)") },
            { 0x1401, new Tuple<string, string>("Dekaja", "Remove buffs. (All Enemies)") },
            { 0x1402, new Tuple<string, string>("Dekunda", "Remove debuffs. (Party)") },
            { 0x1407, new Tuple<string, string>("Tarukaja", "Raise attack for 3 turns. (1 Ally)") },
            { 0x1408, new Tuple<string, string>("Rakukaja", "Raise defense for 3 turns. (1 Ally)") },
            { 0x1409, new Tuple<string, string>("Sukukaja", "Raise accuracy and evasion for 3 turns. (1 Ally)") },
            { 0x140A, new Tuple<string, string>("Tarunda", "Lower attack for 3 turns. (1 Enemy)") },
            { 0x140B, new Tuple<string, string>("Rakunda", "Lower defense for 3 turns. (1 Enemy)") },
            { 0x140C, new Tuple<string, string>("Sukunda", "Lower accuracy and evasion for 3 turns. (1 Enemy)") },
            { 0x1415, new Tuple<string, string>("Sukukaja", "Raise accuracy and evasion for 3 turns. (1 Ally)") },
            { 0x1418, new Tuple<string, string>("Debilitate", "Lower attack/defense and accuracy/evasion for 3 turns. (1 Enemy)") },
            { 0x1424, new Tuple<string, string>("Fire Screen", "Raise Fire resistance for 3 turns. (Party)") },
            { 0x1425, new Tuple<string, string>("Wind Screen", "Raise Wind resistance for 3 turns. (Party)") },
            { 0x1426, new Tuple<string, string>("Ice Screen", "Raise Ice resistance for 3 turns. (Party)") },
            { 0x1427, new Tuple<string, string>("Elec Screen", "Raise Elec resistance for 3 turns. (Party)") },
            { 0x1428, new Tuple<string, string>("Bless Screen", "Raise Bless resistance for 3 turns. (Party)") },
            { 0x1429, new Tuple<string, string>("Curse Screen", "Raise Curse resistance for 3 turns. (Party)") },
            { 0x142A, new Tuple<string, string>("Psy Screen", "Raise Psy resistance for 3 turns. (Party)") },
            { 0x142B, new Tuple<string, string>("Nuclear Screen", "Raise Nuke resistance for 3 turns. (Party)") },
            { 0x1454, new Tuple<string, string>("Ironfist Style", "High chance to add splash to single-target attack skills for 3 turns. (Self)") },
            { 0x1456, new Tuple<string, string>("Effortless Style", "Add splash to single-target attack skills for 3 turns. (Self)") },
            { 0x1458, new Tuple<string, string>("War cry", "Raise attack and lower defense for 3 turns. (Self)") },
            { 0x1596, new Tuple<string, string>("Machismo Wall", "Slightly raise defense & protect allies. Extra defense while protecting. (Party)") },
            { 0x1597, new Tuple<string, string>("Cost Cutter", "2/3 kill costs for 3 turns. (Self)") },
            { 0x1599, new Tuple<string, string>("Called Homerun", "Counter whenever user's row is attacked for 3 turns. (Self)") },
            { 0x159B, new Tuple<string, string>("Kunai Backup", "May follow-up user's row's attacks for 3 turns. (Self)") },
            { 0x159D, new Tuple<string, string>("Voltaic Surge", "Doubles magic attack of next hit for 3 turns. (Self)") },
            { 0x159E, new Tuple<string, string>("Deflector Shot", "Medium chance of nullifying Phys attacks for 3 turns. (1 Row)") },
            { 0x15A1, new Tuple<string, string>("Counter Stance", "Counter whenever user's row is attacked for 3 turns. (Self)") },
            { 0x15A3, new Tuple<string, string>("Machismo Barrier", "Greatly raise defense & protect allies. Extra defense while protecting. (Party)") },
            { 0x15A4, new Tuple<string, string>("Rush Stance", "High chance attack skills will activate twice for 2 turns. (Self)") },
            { 0x15A6, new Tuple<string, string>("Counter Bash", "Powerful counter whenever user's row is attacked for 3 turns. (Self)") },
            { 0x15A8, new Tuple<string, string>("Taunt Counter", "Taunt, guard, and counter when attacked. Extra defense while protecting. (Party)") },
            { 0x15AA, new Tuple<string, string>("Unlimiter", "Act first and greatly raise attack for 3 turns. (Self)") },
            { 0x15AB, new Tuple<string, string>("Full Throttle", "Act first and raise attack for 3 turns. (Party)") },
            { 0x15CB, new Tuple<string, string>("Cost Reduction", "Skill costs halved for 3 turns. (Self)") },
        };

        #endregion SkillsBuff

        #region SkillsCircle

        internal static Dictionary<int, Tuple<string, string>> SkillsCircle = new Dictionary<int, Tuple<string, string>>()
        {
            { 0, new Tuple<string, string>("EMPTY", "") },

            { 0x1181, new Tuple<string, string>("Stun Circle", "Medium chance of Paralysis each turn for 3 turns. (All Enemies)") },
            { 0x1183, new Tuple<string, string>("Sleep Circle", "Medium chance of Sleep each turn for 3 turns. (All Enemies)") },
            { 0x1185, new Tuple<string, string>("Poison Circle", "Medium chance of Poison each turn for 3 turns. (All Enemies)") },
            { 0x1187, new Tuple<string, string>("Confusion Circle", "Medium chance of Confusion each turn for 3 turns. (All Enemies)") },
            { 0x1189, new Tuple<string, string>("Silence Circle", "Medium chance of Magic Bind each turn for 3 turns. (All Enemies)") },
            { 0x118B, new Tuple<string, string>("Decay Circle", "Medium chance of Strength Bind each turn for 3 turns. (All Enemies)") },
            { 0x118D, new Tuple<string, string>("Lethargy Circle", "Medium chance of Agility Bind each turn for 3 turns. (All Enemies)") },
        };

        #endregion SkillsCircle

        #region SkillsCurse

        internal static Dictionary<int, Tuple<string, string>> SkillsCurse = new Dictionary<int, Tuple<string, string>>()
        {
            { 0, new Tuple<string, string>("EMPTY", "") },

            { 0x1048, new Tuple<string, string>("Mudo", "Low chance of Instant Kill. (1 Enemy) [Curse-based attack]") },
            { 0x1049, new Tuple<string, string>("Mudoon", "Medium chance of Instant Kill. (1 Enemy) [Curse-based attack]") },
            { 0x104A, new Tuple<string, string>("Mamudo", "Low chance of Instant Kill. (All Enemies) [Curse-based attack]") },
            { 0x104B, new Tuple<string, string>("Mamudoon", "Medium chance of Instant Kill. (All Enemies) [Curse-based attack]") },
            { 0x104E, new Tuple<string, string>("Die For Me!", "High chance of Instant Kill. (All Enemies) [Curse-based attack]") },
            { 0x1338, new Tuple<string, string>("Mudo", "Low chance of Instant Kill. (1 Enemy) [Curse-based attack]") },
            { 0x1339, new Tuple<string, string>("Mudoon", "Medium chance of Instant Kill. (1 Enemy) [Curse-based attack]") },
            { 0x133A, new Tuple<string, string>("Mamudo", "Low chance of Instant Kill. (All Enemies) [Curse-based attack]") },
            { 0x133B, new Tuple<string, string>("Mamudoon", "Medium chance of Instant Kill. (All Enemies) [Curse-based attack]") },
            { 0x135C, new Tuple<string, string>("Eiga", "Medium Curse attack. (1 Enemy)") },
            { 0x135D, new Tuple<string, string>("Eigaon", "Heavy Curse attack. (1 Enemy)") },
            { 0x1370, new Tuple<string, string>("Hell's Judgment", "Curse attack dealing damage equal to half of enemy's current HP. (1 Enemy)") },
            { 0x156A, new Tuple<string, string>("Eiha", "Light Curse attack. (1 Enemy)") },
            { 0x156C, new Tuple<string, string>("Eiga", "Medium Curse attack. (1 Enemy)") },
            { 0x156D, new Tuple<string, string>("Eigaon", "Heavy Curse attack. (1 Enemy)") },
            { 0x156E, new Tuple<string, string>("Maeiga", "Medium Curse attack. (All Enemies)") },
            { 0x156F, new Tuple<string, string>("Maeigaon", "Heavy Curse attack. (All Enemies)") },
        };

        #endregion SkillsCurse

        #region SkillsElectric

        internal static Dictionary<int, Tuple<string, string>> SkillsElectric = new Dictionary<int, Tuple<string, string>>()
        {
            { 0, new Tuple<string, string>("EMPTY", "") },

            { 0x1027, new Tuple<string, string>("Zio", "Light Elec attack. (1 Enemy)") },
            { 0x1028, new Tuple<string, string>("Zionga", "Medium Elec attack. (1 Enemy)") },
            { 0x1029, new Tuple<string, string>("Ziodyne", "Heavy Elec attack. (1 Enemy)") },
            { 0x102A, new Tuple<string, string>("Mazio", "Light Elec attack. (All Enemies)") },
            { 0x102B, new Tuple<string, string>("Mazionga", "Medium Elec attack. (All Enemies)") },
            { 0x102C, new Tuple<string, string>("Maziodyne", "Heavy Elec attack. (All Enemies)") },
            { 0x102E, new Tuple<string, string>("Thunder Reign", "Severe Elec attack. (1 Enemy)") },
            { 0x102F, new Tuple<string, string>("Keraunos", "Proportional Elec attack. (1 Enemy)") },
            { 0x1030, new Tuple<string, string>("Thunder Clap", "Light Elec attack. (1 Row)") },
            { 0x1031, new Tuple<string, string>("Empyrean Storm", "Proportional Elec attack. (All Enemies)") },
            { 0x10BF, new Tuple<string, string>("Raikouzan", "Light Elec attack that splashes to either side. (1 Enemy)") },
            { 0x10C6, new Tuple<string, string>("Raimeizan", "Medium Elec attack that splashes to either side. (1 Enemy)") },
            { 0x10C7, new Tuple<string, string>("Raijinzan", "Heavy Elec attack that splashes to either side. (1 Enemy)") },
            { 0x1320, new Tuple<string, string>("Zio", "Light Elec attack. (1 Enemy)") },
            { 0x1321, new Tuple<string, string>("Zionga", "Medium Elec attack. (1 Enemy)") },
            { 0x1322, new Tuple<string, string>("Ziodyne", "Heavy Elec attack. (1 Enemy)") },
            { 0x1323, new Tuple<string, string>("Mazio", "Light Elec attack. (All Enemies)") },
            { 0x1324, new Tuple<string, string>("Mazionga", "Medium Elec attack. (All Enemies)") },
            { 0x1325, new Tuple<string, string>("Maziodyne", "Heavy Elec attack. (All Enemies)") },
            { 0x1326, new Tuple<string, string>("Keraunos", "6 heavy Elec attacks to random targets. (All Enemies)") },
            { 0x1327, new Tuple<string, string>("Elec Dance", "Light Elec attack. (1 Row)") },
            { 0x1328, new Tuple<string, string>("Bolt Dance", "Medium Elec attack. (1 Row)") },
            { 0x1329, new Tuple<string, string>("Storm Dance", "Heavy Elec attack. (1 Row)") },
        };

        #endregion SkillsElectric

        #region SkillsEnemy

        internal static Dictionary<int, Tuple<string, string>> SkillsEnemy = new Dictionary<int, Tuple<string, string>>()
        {
            { 0, new Tuple<string, string>("EMPTY", "") },

            { 0x10EC, new Tuple<string, string>("Pain-Eater", "Raise defense and chance to be targeted for 3 turns. (Self)") },
            { 0x10ED, new Tuple<string, string>("Safeguard", "Raise defense & protect an ally. Extra defense while protecting. (1 Ally)") },
            { 0x10EE, new Tuple<string, string>("Line Guard", "Raise defense & protects allies. Extra defense while protecting. (User's Row)") },
            { 0x10EF, new Tuple<string, string>("All Guard", "Raise defense & protects allies. Extra defense while protecting. (Party)") },
            { 0x1129, new Tuple<string, string>("Lunar Blessing", "Nullifies 1 attack each turn for 3 turns. (Party)") },
            { 0x1307, new Tuple<string, string>("Fire Dance", "Light Fire attack that splashes to either side. (1 Enemy)") },
            { 0x1308, new Tuple<string, string>("Flame Dance", "Medium Fire attack that splashes to either side. (1 Enemy)") },
            { 0x1309, new Tuple<string, string>("Inferno Dance", "Heavy Fire attack that splashes to either side. (1 Enemy)") },
            { 0x1311, new Tuple<string, string>("Wind Dance", "2-3 light Wind attacks to random targets. (All Enemies)") },
            { 0x1312, new Tuple<string, string>("Gust Dance", "2-3 medium Wind attacks to random targets. (All Enemies)") },
            { 0x1313, new Tuple<string, string>("Hurricane Dance", "2-3 heavy Wind attacks to random targets. (All Enemies)") },
            { 0x131B, new Tuple<string, string>("Absolute Zero", "2-4 medium Ice attacks to random targets. (All Enemies)") },
            { 0x131C, new Tuple<string, string>("Ice Dance", "Light Ice attack that pierces to the back row. (1 Enemy)") },
            { 0x131D, new Tuple<string, string>("Frost Dance", "Medium Ice attack that pierces to the back row. (1 Enemy)") },
            { 0x131E, new Tuple<string, string>("Frigid Dance", "Heavy Ice attack that pierces to the back row. (1 Enemy)") },
            { 0x1326, new Tuple<string, string>("Keraunos", "6 heavy Elec attacks to random targets. (All Enemies)") },
            { 0x1327, new Tuple<string, string>("Elec Dance", "Medium Elec attack. (1 Row)") },
            { 0x1328, new Tuple<string, string>("Bolt Dance", "Medium Elec attack. (1 Row)") },
            { 0x1329, new Tuple<string, string>("Storm Dance", "Heavy Elec attack. (1 Row)") },
            { 0x1341, new Tuple<string, string>("Stona", "Medium chance of Petrification. (1 Row)") },
            { 0x1342, new Tuple<string, string>("Stone Mist", "Medium chance of Petrification. (All Enemies)") },
            { 0x1347, new Tuple<string, string>("Perplex Flash", "Medium chance of Confusion. (1 Enemy)") },
            { 0x1348, new Tuple<string, string>("Blank", "Medium chance of Blind. (All Enemies)") },
            { 0x134B, new Tuple<string, string>("Shibaboo", "Medium chance of Paralysis. (1 Row)") },
            { 0x134C, new Tuple<string, string>("Binding Cry", "Medium chance of Paralysis. (All Enemies)") },
            { 0x134E, new Tuple<string, string>("Silent Song", "Medium chance of Magic Bind. (All Enemies)") },
            { 0x1350, new Tuple<string, string>("Muscle Down", "Medium chance of Strength Bind. (All Enemies)") },
            { 0x1352, new Tuple<string, string>("Spiderweb", "Medium chance of Agility Bind. (All Enemies)") },
            { 0x1353, new Tuple<string, string>("Self-Destruct", "Heavy Almighty attack to everyone, including allies and enemies.") },
            { 0x1354, new Tuple<string, string>("Bunny Slumber", "Medium chance of Sleep. (1 Enemy)") },
            { 0x1388, new Tuple<string, string>("Hard Slash", "Heavy Phys attack. (1 Enemy)") },
            { 0x1389, new Tuple<string, string>("Charge Shot", "Medium Phys attack that pierces to the back row. (1 Enemy)") },
            { 0x138A, new Tuple<string, string>("Stab Shower", "3 light Phys attacks to random targets. (All Enemies)") },
            { 0x138F, new Tuple<string, string>("Drain Touch", "Medium Phys attack that restores HP upon dealing damage. (1 Enemy)") },
            { 0x1392, new Tuple<string, string>("Thorn Shackles", "Light Phys attack, with medium chance of Agility Bind. (1 Enemy)") },
            { 0x1393, new Tuple<string, string>("Thorn Cuffs", "Medium Phys attack, with medium chance of Strength Bind. (1 Enemy)") },
            { 0x1394, new Tuple<string, string>("Thorn Chains", "Heavy Phys attack, with medium chance of Magic Bind. (1 Enemy)") },
            { 0x139A, new Tuple<string, string>("Megaton Press", "Medium Phys attack, with medium chance of knockdown. (1 Enemy)") },
            { 0x139C, new Tuple<string, string>("Stardrop", "Medium Phys attack. Lower defense for 1 turn. (1 Enemy)") },
            { 0x139D, new Tuple<string, string>("Buchikamashi", "Light Phys attack. (1 Enemy)") },
            { 0x139E, new Tuple<string, string>("Spin Slash", "Heavy Phys attack. (1 Enemy)") },
            { 0x13A2, new Tuple<string, string>("Crazed Slash", "4-7 medium Phys attacks to random targets. (All Enemies)") },
            { 0x13A6, new Tuple<string, string>("Megaton Slam", "Medium Phys attack. (1 Enemy)") },
            { 0x13A9, new Tuple<string, string>("Toxic Slice", "Medium Phys attack, with medium chance of Poison. (1 Enemy)") },
            { 0x13AA, new Tuple<string, string>("Dream Slice", "Heavy Phys attack, with medium chance of Sleep. (1 Enemy)") },
            { 0x13AB, new Tuple<string, string>("Stunning Slice", "Light Phys attack, with medium chance of Paralysis. (1 Enemy)") },
            { 0x13AC, new Tuple<string, string>("Bane Slice", "Medium Phys attack, with medium chance of Hex. (1 Enemy)") },
            { 0x13AD, new Tuple<string, string>("Mow Down", "Light Phys attack. (1 Enemy)") },
            { 0x13AF, new Tuple<string, string>("Torrent Shot", "Medium Phys attack. (All Enemies)") },
            { 0x13B2, new Tuple<string, string>("Egg Bomb", "Light Phys attack that splashes to either side. (1 Enemy)") },
            { 0x13B3, new Tuple<string, string>("Burst Arm", "4-8 light Phys attacks to random targets. (All Enemies)") },
            { 0x13B4, new Tuple<string, string>("Supersonic Hero", "Phys attack. (All Enemies)") },
            { 0x13B5, new Tuple<string, string>("Kamoshida Kick", "Medium Phys attack. Lower attack for 3 turns. (1 Enemy)") },
            { 0x13B6, new Tuple<string, string>("Carrot Gun", "Light Phys attack that splashes to either side. (1 Enemy)") },
            { 0x13B7, new Tuple<string, string>("Carrot Bomb", "Light Fire attack. (1 Enemy)") },
            { 0x13BA, new Tuple<string, string>("Scepter Strike", "Phys attack dealing damage equal to half of enemy's current HP. (1 Enemy)") },
            { 0x13BB, new Tuple<string, string>("End Shot", "Medium Phys attack that activates last in the turn it's used. (1 Enemy)") },
            { 0x13BC, new Tuple<string, string>("Scarlet Laser", "4-8 medium Nuke attacks to random targets. (All Enemies)") },
            { 0x13BD, new Tuple<string, string>("Barrage", "10 medium Phys attacks to random targets. (All Enemies)") },
            { 0x13BE, new Tuple<string, string>("Berserk Frenzy", "3-6 medium Phys attacks to random targets. (All Enemies)") },
            { 0x13BF, new Tuple<string, string>("Sabaton Slice", "Heavy Phys attack, with medium chance of Agility Bind. (1 Enemy)") },
            { 0x13C0, new Tuple<string, string>("Crushing Strike", "Heavy Phys attack. Medium chance of Magic/Strength/Agility Bind. (1 Enemy)") },
            { 0x13C3, new Tuple<string, string>("Eliminate", "Proportional Phys attack. (1 Enemy)") },
            { 0x13C4, new Tuple<string, string>("Leg Eraser", "Medium Phys attack, with high chance of Agility Bind. (1 Enemy)") },
            { 0x13C5, new Tuple<string, string>("Power Eraser", "Medium Phys attack, with high chance of Strength Bind. (1 Enemy)") },
            { 0x13C6, new Tuple<string, string>("Head Eraser", "Medium Phys attack, with high chance of Magic Bind. (1 Enemy)") },
            { 0x13C7, new Tuple<string, string>("Weapon Breaker", "Medium Phys attack. Lower attack for 3 turns. (1 Enemy)") },
            { 0x13C8, new Tuple<string, string>("Tail Whip", "Medium Phys attack. (1 Row)") },
            { 0x13D4, new Tuple<string, string>("Medical Kit", "Moderate HP restore. (1 Row)") },
            { 0x13D5, new Tuple<string, string>("Auto Recovery", "Proportional HP restore. (Self)") },
            { 0x13E6, new Tuple<string, string>("Nasty Taunt", "Raise attack for 1 turn. (1 Ally)") },
            { 0x13B1, new Tuple<string, string>("Buchikamashi", "Light Phys attack. (1 Enemy)") },
            { 0x13B8, new Tuple<string, string>("Mow Down", "Light Phys attack. (1 Enemy)") },
            { 0x13B9, new Tuple<string, string>("Supersonic Hero", "Phys attack. (All Enemies)") },
            { 0x13E8, new Tuple<string, string>("Rampart", "Raise defense for 3 turns. (1 Ally)") },
            { 0x13E9, new Tuple<string, string>("Safeguard", "Raise defense & protect an ally. Extra defense while protecting. (1 Ally)") },
            { 0x13EB, new Tuple<string, string>("All Guard", "Raise defense & protects allies. Extra defense while protecting. (Party)") },
            { 0x140E, new Tuple<string, string>("Hypercharge", "Triples Phys attack of next hit for 3 turns. (Self)") },
            { 0x140F, new Tuple<string, string>("Absolute Power", "Magic/Strength/Agility Bind all targets.") },
            { 0x1412, new Tuple<string, string>("Mow Down", "2-3 heavy Phys attacks to random targets. (All Enemies)") },
            { 0x1413, new Tuple<string, string>("Wild Rush", "4 heavy Phys attacks to random targets. (All Enemies)") },
            { 0x1414, new Tuple<string, string>("Riot Run", "Proportional Almighty attack. (All Enemies)") },
            { 0x1416, new Tuple<string, string>("Double Sukukaja", "Raise accuracy and evasion for 3 turns. (1 Ally)") },
            { 0x1417, new Tuple<string, string>("Triple Sukukaja", "Raise accuracy and evasion for 3 turns. (1 Ally)") },
            { 0x14A6, new Tuple<string, string>("Piercing Rapier", "Heavy Phys attack that pierces to the back row. (1 Enemy)") },
            { 0x14A7, new Tuple<string, string>("Tempest Dance", "8 medium Phys attacks to random targets. (All Enemies)") },
            { 0x14A8, new Tuple<string, string>("Tempest Dance", "Heavy Phys attack. (All Enemies)") },
            { 0x14A9, new Tuple<string, string>("Wind Fang", "Light Wind attack. (1 Enemy)") },
            { 0x14AA, new Tuple<string, string>("Swift Fang", "Light Wind attack that pierces to the back row. (1 Enemy)") },
            { 0x14AB, new Tuple<string, string>("Gale Fang", "Light Wind attack. (1 Row)") },
            { 0x14AC, new Tuple<string, string>("Tempest Fang", "4 light Wind attacks to random targets. (All Enemies)") },
            { 0x14BE, new Tuple<string, string>("Absolute Power", "Magic/Strength/Agility Bind all targets.") },
            { 0x14BF, new Tuple<string, string>("Denial", "Ban usage of one mass-targeting skill per person.") },
            { 0x14C0, new Tuple<string, string>("Synchronicity", "Ban usage of one skill for all.") },
            { 0x14C4, new Tuple<string, string>("Pounce Punch", "2 medium Phys attacks to random targets. (All Enemies)") },
            { 0x14C6, new Tuple<string, string>("Infinite Despair", "Proportional HP attack that causes Magic/Strength/Agility Bind to all.") },
            { 0x14C7, new Tuple<string, string>("Auto Recovery", "Proportional HP restore. (Self)") },
            { 0x14C8, new Tuple<string, string>("Urgent Recovery", "Full HP restore. (Self)") },
            { 0x14CA, new Tuple<string, string>("All-out Attack", "Severe Almighty attack. (All Enemies)") },
            { 0x14CB, new Tuple<string, string>("Megidolaon", "Severe Almighty attack. (All Enemies)") },
            { 0x14CF, new Tuple<string, string>("Flame Tempest", "Heavy Fire attack that splashes to either side. (1 Enemy)") },
            { 0x14D0, new Tuple<string, string>("Frost Tempest", "Heavy Ice attack that pierces to the back row. (1 Enemy)") },
            { 0x14D1, new Tuple<string, string>("Saint Tempest", "Heavy Bless attack. (1 Row)") },
            { 0x14D2, new Tuple<string, string>("Umbral Tempest", "2-3 heavy Curse attacks to random targets. (1 Row)") },
            { 0x14D4, new Tuple<string, string>("Saint Spear", "Heavy Phys attack that pierces to the back row. (1 Enemy)") },
            { 0x14D5, new Tuple<string, string>("Air Slice", "Heavy Phys attack. (1 Enemy)") },
            { 0x14D7, new Tuple<string, string>("Flame Aura", "Take on a Fire affinity aura.") },
            { 0x14D8, new Tuple<string, string>("Frost Aura", "Take on a Ice affinity aura.") },
            { 0x14D9, new Tuple<string, string>("Saint Aura", "Take on a Bless affinity aura.") },
            { 0x14DA, new Tuple<string, string>("Umbral Aura", "Take on a Curse affinity aura.") },
            { 0x14DB, new Tuple<string, string>("Flame Aura", "Take on a Fire affinity aura.") },
            { 0x14DC, new Tuple<string, string>("Frost Aura", "Take on a Ice affinity aura.") },
            { 0x14DD, new Tuple<string, string>("Saint Aura", "Take on a Bless affinity aura.") },
            { 0x14DE, new Tuple<string, string>("Umbral Aura", "Take on a Curse affinity aura.") },
            { 0x14E0, new Tuple<string, string>("Rage", "Release rage and charge up power.") },
            { 0x14E1, new Tuple<string, string>("Origin Strike", "Severe Phys attack that splashes to either side. (1 Enemy)") },
            { 0x14E3, new Tuple<string, string>("Retake", "Rewind time.") },
            { 0x14E4, new Tuple<string, string>("Time Accelerate", "Fast forward time.") },
        };

        #endregion SkillsEnemy

        #region SkillsFire

        internal static Dictionary<int, Tuple<string, string>> SkillsFire = new Dictionary<int, Tuple<string, string>>()
        {
            { 0, new Tuple<string, string>("EMPTY", "") },

            { 0x1002, new Tuple<string, string>("Agi", "Light Fire attack. (1 Enemy)") },
            { 0x1003, new Tuple<string, string>("Agilao", "Medium Fire attack. (1 Enemy)") },
            { 0x1004, new Tuple<string, string>("Agidyne", "Heavy Fire attack. (1 Enemy)") },
            { 0x1005, new Tuple<string, string>("Maragi", "Light Fire attack. (All Enemies)") },
            { 0x1006, new Tuple<string, string>("Maragion", "Medium Fire attack. (All Enemies)") },
            { 0x1007, new Tuple<string, string>("Maragidyne", "Heavy Fire attack. (All Enemies)") },
            { 0x1008, new Tuple<string, string>("Fire Spray", "Light Fire attack that splashes to either side. (1 Enemy)") },
            { 0x1009, new Tuple<string, string>("Ragnarok", "Severe Fire attack. (1 Enemy)") },
            { 0x10BD, new Tuple<string, string>("Gurentou", "Light Fire attack that splashes to either side. (1 Enemy)") },
            { 0x10C8, new Tuple<string, string>("Gokuentou", "Medium Fire attack that splashes to either side. (1 Enemy)") },
            { 0x10C9, new Tuple<string, string>("Hientou", "Heavy Fire attack that splashes to either side. (1 Enemy)") },
            { 0x1301, new Tuple<string, string>("Agi", "Light Fire attack. (1 Enemy)") },
            { 0x1302, new Tuple<string, string>("Agilao", "Medium Fire attack. (1 Enemy)") },
            { 0x1303, new Tuple<string, string>("Agidyne", "Heavy Fire attack. (1 Enemy)") },
            { 0x1304, new Tuple<string, string>("Maragi", "Light Fire attack. (All Enemies)") },
            { 0x1305, new Tuple<string, string>("Maragion", "Medium Fire attack. (All Enemies)") },
            { 0x1306, new Tuple<string, string>("Maragidyne", "Heavy Fire attack. (All Enemies)") },
            { 0x1307, new Tuple<string, string>("Fire Dance", "Light Fire attack that splashes to either side. (1 Enemy)") },
            { 0x1308, new Tuple<string, string>("Flame Dance", "Medium Fire attack that splashes to either side. (1 Enemy)") },
            { 0x1309, new Tuple<string, string>("Inferno Dance", "Heavy Fire attack that splashes to either side. (1 Enemy)") },
        };

        #endregion SkillsFire

        #region SkillsHealing

        internal static Dictionary<int, Tuple<string, string>> SkillsHealing = new Dictionary<int, Tuple<string, string>>()
        {
            { 0, new Tuple<string, string>("EMPTY", "") },
            
            { 0x10D1, new Tuple<string, string>("Dia", "Slight HP restore. (1 Ally)") },
            { 0x10D2, new Tuple<string, string>("Diarama", "Moderate HP restore. (1 Ally)") },
            { 0x10D3, new Tuple<string, string>("Diarahan", "Full HP restore. (1 Ally)") },
            { 0x10D4, new Tuple<string, string>("Line Heal", "Slight HP restore. (1 Row) [Only usable in battle]") },
            { 0x10D5, new Tuple<string, string>("Life Goblet", "Moderate HP restore. (1 Row) [Only usable in battle]") },
            { 0x10D6, new Tuple<string, string>("Media", "Slight HP restore. (Party)") },
            { 0x10D7, new Tuple<string, string>("Mediarama", "Moderate HP restore. (Party)") },
            { 0x10D8, new Tuple<string, string>("Mediarahan", "Full HP restore. (Party)") },
            { 0x10D9, new Tuple<string, string>("Recarm", "Revive and restore half HP. (1 Ally)") },
            { 0x10DA, new Tuple<string, string>("Samarecarm", "Revive and restore all HP. (1 Ally)") },
            { 0x10DB, new Tuple<string, string>("Treatment", "Remove debuffs. (1 Ally)") },
            { 0x10DC, new Tuple<string, string>("Renewal", "Remove debuffs. (1 Row)") },
            { 0x10DD, new Tuple<string, string>("Patra", "Cure ailments. (1 Row)") },
            { 0x10DE, new Tuple<string, string>("Refresh", "Slight HP restore and removes ailments. (1 Row) [Only usable in battle]") },
            { 0x10DF, new Tuple<string, string>("Me Patra", "Cure ailments. (Party)") },
            { 0x10E0, new Tuple<string, string>("Mutudi", "Remove Binds. (1 Row)") },
            { 0x10E1, new Tuple<string, string>("Becalm", "Remove Binds. (1 Row)") },
            { 0x10E2, new Tuple<string, string>("Mamutudi", "Remove Binds. (Party)") },
            { 0x10E3, new Tuple<string, string>("Amrita", "Full HP restore. Remove all ailments and Binds. (1 Ally)") },
            { 0x112B, new Tuple<string, string>("Healing Harp", "Restore HP and remove ailments/Binds each turn for 3 turns. (Party)") },
            { 0x1196, new Tuple<string, string>("Heal Memento", "Slightly restore HP at the end of the turn for 3 turns. (User's Row)") },
            { 0x1198, new Tuple<string, string>("Free Memento", "Remove Binds at the end of the turn for 3 turns. (User's Row)") },
            { 0x119A, new Tuple<string, string>("Pure Memento", "Remove ailments at the end of the turn for 3 turns. (User's Row)") },
            { 0x13D2, new Tuple<string, string>("Diarama", "Moderate HP restore. (1 Ally)") },
            { 0x13D3, new Tuple<string, string>("Diarahan", "Full HP restore. (1 Ally)") },
            { 0x13D6, new Tuple<string, string>("Media", "Slight HP restore. (Party)") },
            { 0x13D7, new Tuple<string, string>("Mediarama", "Moderate HP restore. (Party)") },
            { 0x13D8, new Tuple<string, string>("Mediarahan", "Full HP restore. (Party)") },
            { 0x13DA, new Tuple<string, string>("Samarecarm", "Revive and restore all HP. (1 Ally)") },
            { 0x13DB, new Tuple<string, string>("Recarm", "Revive and restore half HP. (1 Ally)") },
            { 0x13DD, new Tuple<string, string>("Patra", "Cure ailments. (1 Row)") },
            { 0x13E0, new Tuple<string, string>("Mutudi", "Remove Binds. (1 Row)") },
            { 0x1443, new Tuple<string, string>("Oratorio", "Full HP restore. Remove all ailments and Binds. (1 Row)") },
        };

        #endregion SkillsHealing

        #region SkillsIce

        internal static Dictionary<int, Tuple<string, string>> SkillsIce = new Dictionary<int, Tuple<string, string>>()
        {
            { 0, new Tuple<string, string>("EMPTY", "") },

            { 0x101A, new Tuple<string, string>("Bufu", "Light Ice attack. (1 Enemy)") },
            { 0x101B, new Tuple<string, string>("Bufula", "Medium Ice attack. (1 Enemy)") },
            { 0x101C, new Tuple<string, string>("Bufudyne", "Heavy Ice attack. (1 Enemy)") },
            { 0x101D, new Tuple<string, string>("Mabufu", "Light Ice attack. (All Enemies)") },
            { 0x101E, new Tuple<string, string>("Mabufula", "Medium Ice attack. (All Enemies)") },
            { 0x101F, new Tuple<string, string>("Mabufudyne", "Heavy Ice attack. (All Enemies)") },
            { 0x1020, new Tuple<string, string>("Icy Paradise", "2-4 medium Ice attacks to random targets. (All Enemies)") },
            { 0x1021, new Tuple<string, string>("Niflheim", "Severe Ice attack. (1 Enemy)") },
            { 0x1022, new Tuple<string, string>("Frozen Spear", "Light Ice attack that pierces to the back row. (1 Enemy)") },
            { 0x108B, new Tuple<string, string>("Frost Edge", "Light Ice attack that splashes to either side. (1 Enemy)") },
            { 0x10A6, new Tuple<string, string>("Glacial Edge", "Medium Ice attack that splashes to either side. (1 Enemy)") },
            { 0x10BE, new Tuple<string, string>("Tousatsujin", "Heavy Ice attack that splashes to either side. (1 Enemy)") },
            { 0x1315, new Tuple<string, string>("Bufu", "Light Ice attack. (1 Enemy)") },
            { 0x1316, new Tuple<string, string>("Bufula", "Medium Ice attack. (1 Enemy)") },
            { 0x1317, new Tuple<string, string>("Bufudyne", "Heavy Ice attack. (1 Enemy)") },
            { 0x1318, new Tuple<string, string>("Mabufu", "Light Ice attack. (All Enemies)") },
            { 0x1319, new Tuple<string, string>("Mabufula", "Medium Ice attack. (All Enemies)") },
            { 0x131A, new Tuple<string, string>("Mabufudyne", "Heavy Ice attack. (All Enemies)") },
            { 0x131B, new Tuple<string, string>("Absolute Zero", "2-4 medium Ice attacks to random targets. (All Enemies)") },
            { 0x131C, new Tuple<string, string>("Ice Dance", "Light Ice attack that pierces to the back row. (1 Enemy)") },
            { 0x131D, new Tuple<string, string>("Frost Dance", "Medium Ice attack that pierces to the back row. (1 Enemy)") },
            { 0x131E, new Tuple<string, string>("Frigid Dance", "Heavy Ice attack that pierces to the back row. (1 Enemy)") },
            { 0x138B, new Tuple<string, string>("Frost Edge", "Light Ice attack that splashes to either side. (1 Enemy)") },
            { 0x13A0, new Tuple<string, string>("Tousatsujin", "Heavy Ice attack that splashes to either side. (1 Enemy)") },
        };

        #endregion SkillsIce

        #region SkillsLinks

        internal static Dictionary<int, Tuple<string, string>> SkillsLinks = new Dictionary<int, Tuple<string, string>>()
        {
            { 0, new Tuple<string, string>("EMPTY", "") },

            { 0x11A9, new Tuple<string, string>("Firestrike Link", "Light Fire attack. Further allied attacks gain Link damage. (1 Enemy)") },
            { 0x11AB, new Tuple<string, string>("Icestrike Link", "Light Ice attack. Further allied attacks gain Link damage. (1 Enemy)") },
            { 0x11AD, new Tuple<string, string>("Elecstrike Link", "Light Elec attack. Further allied attacks gain Link damage. (1 Enemy)") },
            { 0x11AF, new Tuple<string, string>("Windstrike Link", "Light Wind attack. Further allied attacks gain Link damage. (1 Enemy)") },
            { 0x11B2, new Tuple<string, string>("Strike Link", "Light Phys attack. Further allied attacks gain Link damage. (1 Enemy)") },
            { 0x11B4, new Tuple<string, string>("Psystrike Link", "Light Psy attack. Further allied attacks gain Link damage. (1 Enemy)") },
            { 0x11B6, new Tuple<string, string>("Nucleostrike Link", "Light Nuke attack. Further allied attacks gain Link damage. (1 Enemy)") },
            { 0x14F2, new Tuple<string, string>("Windstrike Link", "Light Wind attack. Further allied attacks gain Link damage. (1 Enemy)") },
        };

        #endregion SkillsLinks

        #region SkillsNaviBattle

        internal static Dictionary<int, Tuple<string, string>> SkillsNaviBattle = new Dictionary<int, Tuple<string, string>>()
        {
            { 0, new Tuple<string, string>("EMPTY", "") },

            { 0x1139, new Tuple<string, string>("True Analysis", "Unlocks all affinity details. (1 Enemy)") },
            { 0x113A, new Tuple<string, string>("True Analysis", "Unlocks all affinity details. (1 Enemy)") },
            { 0x113B, new Tuple<string, string>("Zero Shift", "Lower skill costs to 0. (1 Ally)") },
            { 0x113C, new Tuple<string, string>("Zero Shift", "Lower skill costs to 0. (1 Ally)") },
            { 0x113D, new Tuple<string, string>("Encore", "Extend buffs by 3 additional turns. (Party)") },
            { 0x113E, new Tuple<string, string>("Encore", "Extend buffs by 3 additional turns. (Party)") },
            { 0x1140, new Tuple<string, string>("Oracle", "Give a random beneficial effect. (Party)") },
            { 0x1142, new Tuple<string, string>("Sutakora Foot", "Flee from battle and teleport to floor entrance. (Party)") },
            { 0x1146, new Tuple<string, string>("Heroic Wind", "Restore 5% of HP each turn. (Auto)") },
            { 0x114A, new Tuple<string, string>("Healing Tide", "Moderately restores HP at the end of turn for 3 turns. (Party)") },
            { 0x114D, new Tuple<string, string>("Healing Tide", "Moderately restores HP at the end of turn for 3 turns. (Party)") },
            { 0x114F, new Tuple<string, string>("Renewal Ray", "Slightly restore HP. (Party)") },
            { 0x1150, new Tuple<string, string>("Renewal Aura", "Moderately restore HP. (Party)") },
            { 0x1151, new Tuple<string, string>("Mini Recovery", "Slightly restore HP at the end of turn for 3 turns. (Party)") },
            { 0x1152, new Tuple<string, string>("Spotlight", "Allow someone to act first for the turn. (1 Ally)") },
            { 0x1153, new Tuple<string, string>("Yomi Return", "Revive and restore half HP. (1 Ally)") },
            { 0x1155, new Tuple<string, string>("Orb of Resolve", "Raise defense for 3 turns. (Party)") },
            { 0x1156, new Tuple<string, string>("Orb of Power", "Raise attack for 3 turns. (Party)") },
            { 0x1157, new Tuple<string, string>("Orb of Haste", "Raise accuracy and evasion for 3 turns. (Party)") },
            { 0x1158, new Tuple<string, string>("Life Aid", "Restore 20% of HP after battle. (Party) (Auto)") },
            { 0x1159, new Tuple<string, string>("Victory Cry", "Fully restore HP after battle. (Party) (Auto)") },
            { 0x115E, new Tuple<string, string>("Final Guard", "Nullifies all damage for 1 turn. (Party)") },
            { 0x115F, new Tuple<string, string>("Final Guard", "Nullifies all damage for 1 turn. (Party)") },
            { 0x1160, new Tuple<string, string>("Tidal Wave", "Allow everyone to act first for the turn. (Party)") },
            { 0x1161, new Tuple<string, string>("Oracle", "Give a random beneficial effect. (Party)") },
            { 0x1171, new Tuple<string, string>("Purifying Rain", "Removes all ailments and Binds. (Party)") },
            { 0x1173, new Tuple<string, string>("Zero Set", "Lower skill costs to 0. (Party)") },
            { 0x1174, new Tuple<string, string>("Eternal Melody", "Extend buffs by 3 additional turns. (Party)") },
            { 0x1176, new Tuple<string, string>("Escape Route", "Flee from battle and teleport to floor entrance. (Party)") },
            { 0x1177, new Tuple<string, string>("Hikari's Cheer", "Raise attack, removes Binds, and adds Boost. (1 Ally)") },
            { 0x1179, new Tuple<string, string>("Hunting Prayer", "Slightly raise drop rate of materials from enemies. (Auto)") },
            { 0x117A, new Tuple<string, string>("Endless Bounty", "Greatly raise drop rate of materials from enemies. (Auto)") },
            { 0x149D, new Tuple<string, string>("Baton Pass", "Give Boost to another ally. Usage cannot be canceled.") },
        };

        #endregion SkillsNaviBattle

        #region SkillsNaviDungeon

        internal static Dictionary<int, Tuple<string, string>> SkillsNaviDungeon = new Dictionary<int, Tuple<string, string>>()
        {
            { 0, new Tuple<string, string>("EMPTY", "") },

            { 0x1145, new Tuple<string, string>("Snake Glare", "High chance of preventing enemy ambushes. (Auto)") },
            { 0x114B, new Tuple<string, string>("Harvest Prayer", "Slightly increase items gathered at Treasure Spots. (Auto)") },
            { 0x114C, new Tuple<string, string>("Cornucopia", "Greatly increase items gathered at Treasure Spots. (Auto)") },
            { 0x115A, new Tuple<string, string>("Safety Prayer", "Slightly lower chance of ambushes at Treasure Spots. (Auto)") },
            { 0x115B, new Tuple<string, string>("Calming Lull", "Greatly lower chance of ambushes at Treasure Spots. (Auto)") },
            { 0x115C, new Tuple<string, string>("Spotter", "Slightly raise chance of rare items at Treasure Spots. (Auto)") },
            { 0x115D, new Tuple<string, string>("Magic Mallet", "Greatly raise chance of rare items at Treasure Spots. (Auto)") },
            { 0x116A, new Tuple<string, string>("Sneak Guide", "Higher chance of gaining a preemptive attack. (Auto)") },
            { 0x116B, new Tuple<string, string>("Treasure Hunter", "Display nearby treasure chests on map. (Auto)") },
            { 0x116C, new Tuple<string, string>("Fortune Hunter", "Display wide range of treasure chests on map. (Auto)") },
            { 0x116D, new Tuple<string, string>("Faerie's Virgil", "Display nearby hidden passages on map. (Auto)") },
            { 0x116E, new Tuple<string, string>("Tengu's Virgil", "Display wide range of hidden passages on map. (Auto)") },
            { 0x116F, new Tuple<string, string>("Clairvoyance", "Display nearby FOEs/chests/hidden passages on map. (Auto)") },
            { 0x1170, new Tuple<string, string>("Farsight", "Display wide range of FOEs/chests/hidden passages on map. (Auto)") },
            { 0x1172, new Tuple<string, string>("Treasure Skimmer", "Raise amount of found items at Treasure Spots. (Auto)") },
            { 0x1175, new Tuple<string, string>("On Stage", "Greatly lower chance of ambushes at Treasure Spots. (Auto)") },
            { 0x1178, new Tuple<string, string>("Risk Management", "High chance of preventing enemy ambushes. (Auto)") },
            { 0x117B, new Tuple<string, string>("Third Eye", "Display nearby FOEs/chests/hidden passages on map. (Auto)") },
            { 0x117C, new Tuple<string, string>("Cheer March", "Slightly restore HP while walking in labyrinth. (Auto)") },
            { 0x117D, new Tuple<string, string>("Paths of Light", "Moderately restore HP while walking in labyrinth. (Auto)") },
            { 0x117E, new Tuple<string, string>("Roads of Light", "Slightly restore SP while walking in labyrinth. (Auto)") },
        };

        #endregion SkillsNaviDungeon

        #region SkillsNuclear

        internal static Dictionary<int, Tuple<string, string>> SkillsNuclear = new Dictionary<int, Tuple<string, string>>()
        {
            { 0, new Tuple<string, string>("EMPTY", "") },

            { 0x10AA, new Tuple<string, string>("Fusion Blast", "Light Nuke attack that splashes to either side. (1 Enemy)") },
            { 0x112F, new Tuple<string, string>("Scorching Blast", "Medium Nuke attack that splashes to either side. (1 Enemy)") },
            { 0x1130, new Tuple<string, string>("Nuclear Blast", "Heavy Nuke attack that splashes to either side. (1 Enemy)") },
            { 0x13A4, new Tuple<string, string>("Scorching Blast", "Medium Nuke attack that splashes to either side. (1 Enemy)") },
            { 0x1574, new Tuple<string, string>("Frei", "Light Nuke attack. (1 Enemy)") },
            { 0x1575, new Tuple<string, string>("Freila", "Medium Nuke attack. (1 Enemy)") },
            { 0x1576, new Tuple<string, string>("Freidyne", "Heavy Nuke attack. (1 Enemy)") },
            { 0x1577, new Tuple<string, string>("Mafrei", "Light Nuke attack. (All Enemies)") },
            { 0x1578, new Tuple<string, string>("Mafreila", "Medium Nuke attack. (All Enemies)") },
            { 0x1579, new Tuple<string, string>("Mafreidyne", "Heavy Nuke attack. (All Enemies)") },
            { 0x157B, new Tuple<string, string>("Atomic Flare", "Severe Nuke attack. (1 Enemy)") },
            { 0x157C, new Tuple<string, string>("Cosmic Flare", "Severe Nuke attack. (All Enemies)") },
        };

        #endregion SkillsNuclear

        #region SkillsPhys

        internal static Dictionary<int, Tuple<string, string>> SkillsPhys = new Dictionary<int, Tuple<string, string>>()
        {
            { 0, new Tuple<string, string>("EMPTY", "") },

            { 0x1081, new Tuple<string, string>("Fang Smash", "Light Phys attack. Lower attack for 3 turns. (1 Enemy)") },
            { 0x1082, new Tuple<string, string>("Kidney Smash", "Medium Phys attack. Lower attack for 3 turns. (1 Enemy)") },
            { 0x1083, new Tuple<string, string>("Giant Slice", "Light Phys attack that splashes to either side. (1 Enemy) Stronger in Boost") },
            { 0x1084, new Tuple<string, string>("Twin Slash", "2 Light Phys attacks. (1 Enemy) Stronger in Boost") },
            { 0x1085, new Tuple<string, string>("Leg Reaper", "Medium Phys attack, with medium chance of Agility Bind. (1 Enemy)") },
            { 0x1086, new Tuple<string, string>("Triple Down", "3 medium Phys attacks. (1 Enemy) Stronger in Boost") },
            { 0x1087, new Tuple<string, string>("Mighty Swing", "Medium Phys attack that splashes to either side. (1 Enemy) Stronger in Boost") },
            { 0x1088, new Tuple<string, string>("Achilles Reaper", "Heavy Phys attack, with medium chance of Agility Bind. (1 Enemy)") },
            { 0x1089, new Tuple<string, string>("Silent Thrust", "Light Phys attack, with medium chance of Hex. (1 Enemy)") },
            { 0x108A, new Tuple<string, string>("Rampage", "2-6 Light Phys attacks to random targets. (All Enemies) Stronger in Boost") },
            { 0x108C, new Tuple<string, string>("Zanshinken", "Light Phys attack. If unhurt during turn, extra attack activates. (1 Enemy)") },
            { 0x108E, new Tuple<string, string>("Death Needle", "Medium Phys attack, with high chance of Poison. (1 Enemy)") },
            { 0x108F, new Tuple<string, string>("Guillotine", "Light Phys attack, higher damage if target has ailment. (1 Enemy)") },
            { 0x1090, new Tuple<string, string>("Tempest Slash", "2-6 Medium Phys attacks. (1 Enemy) Stronger in Boost") },
            { 0x1092, new Tuple<string, string>("Holy Touch", "Medium Phys attack, with medium chance of Magic Bind. (1 Enemy)") },
            { 0x1093, new Tuple<string, string>("Revenge Blow", "Light Phys attack to random targets, stronger for each hit taken. (All Enemies)") },
            { 0x1095, new Tuple<string, string>("Arm Chopper", "Medium Phys attack, with medium chance of Strength Bind. (1 Enemy)") },
            { 0x1096, new Tuple<string, string>("Single Shot", "Light Phys attack that pierces to the back row. (1 Enemy) Stronger in Boost") },
            { 0x109A, new Tuple<string, string>("Assault Shot", "Medium Phys attack that pierces to the back row. (1 Enemy) Stronger in Boost") },
            { 0x109B, new Tuple<string, string>("Primal Force", "2 heavy Phys attacks. (1 Enemy) Stronger in Boost") },
            { 0x109C, new Tuple<string, string>("Weapon Crusher", "Heavy Phys attack. Lower attack for 3 turns. (1 Enemy)") },
            { 0x109D, new Tuple<string, string>("Broadshot", "Heavy Phys attack that pierces to the back row. (1 Enemy) Stronger in Boost") },
            { 0x109E, new Tuple<string, string>("Myriad Arrows", "6-8 heavy Phys attacks. (1 Enemy) Stronger in Boost") },
            { 0x109F, new Tuple<string, string>("Punishment", "Light Phys attack, higher damage if target is bound, but dispels Bind. (1 Enemy)") },
            { 0x10A0, new Tuple<string, string>("Assault Dive", "Medium Phys attack. (1 Enemy) Stronger in Boost") },
            { 0x10A1, new Tuple<string, string>("Sleeper Punch", "Light Phys attack, with medium chance of Sleep. (1 Enemy)") },
            { 0x10A2, new Tuple<string, string>("Armor Splitter", "Immediate light Phys attack. Lower defense for 3 turns. (1 Enemy)") },
            { 0x10A3, new Tuple<string, string>("Lunge", "Light Phys attack. (1 Enemy) Stronger in Boost") },
            { 0x10A4, new Tuple<string, string>("Deathbound", "Heavy Phys attack. (All Enemies) Stronger in Boost") },
            { 0x10A5, new Tuple<string, string>("Blade of Fury", "Heavy Phys attack that splashes to either side. (1 Enemy) Stronger in Boost") },
            { 0x10A7, new Tuple<string, string>("Vorpal Blade", "4 heavy Phys attacks. (1 Enemy) Stronger in Boost") },
            { 0x10A8, new Tuple<string, string>("Danse Macabre", "5-7 medium Phys attacks to random targets. (All Enemies) Stronger in Boost") },
            { 0x10A9, new Tuple<string, string>("Headbutt", "Light Phys attack, with medium chance of Confusion. (1 Enemy)") },
            { 0x10AB, new Tuple<string, string>("Heat Wave", "Medium Phys attack. (All Enemies) Stronger in Boost") },
            { 0x10AC, new Tuple<string, string>("Light Wave", "Severe Phys attack. (1 Enemy) Stronger in Boost") },
            { 0x10AE, new Tuple<string, string>("Brutal Slash", "Medium Phys attack. If unhurt during turn, extra attack activates. (1 Enemy)") },
            { 0x10B1, new Tuple<string, string>("Heaven's Blade", "Heavy Phys attack. If unhurt during turn, extra attack activates. (1 Enemy)") },
            { 0x10B2, new Tuple<string, string>("God's Hand", "Severe Phys attack. (1 Enemy) Stronger in Boost") },
            { 0x10B3, new Tuple<string, string>("Angelic Trumpet", "3 heavy Phys attacks to random targets. (All Enemies) Stronger in Boost") },
            { 0x10B4, new Tuple<string, string>("Hassou Tobi", "8 medium Phys attacks to random targets. (All Enemies) Stronger in Boost") },
            { 0x10B5, new Tuple<string, string>("Hailstorm", "3-8 heavy Phys attacks to random targets. (All Enemies) Stronger in Boost") },
            { 0x10B6, new Tuple<string, string>("Arrow Rain", "3-6 medium Phys attacks to random targets. (All Enemies) Stronger in Boost") },
            { 0x10B7, new Tuple<string, string>("Aeon Rain", "3-6 severe Phys attacks to random targets. (All Enemies) Stronger in Boost") },
            { 0x10B8, new Tuple<string, string>("Beheading Cut", "Medium Phys attack, with slight chance of Instant Kill. (1 Enemy)") },
            { 0x10BC, new Tuple<string, string>("Poison Skewer", "Light Phys attack, with medium chance of Poison. (1 Enemy)") },
            { 0x1127, new Tuple<string, string>("Calamity Seed", "3-6 heavy Phys attacks each turn for 3 turns. (All Enemies)") },
            { 0x1131, new Tuple<string, string>("Sleep Strike", "Medium Phys attack, with high chance of Sleep. (1 Enemy)") },
            { 0x1132, new Tuple<string, string>("Brain Shake", "Medium Phys attack, with high chance of Confusion. (1 Enemy)") },
            { 0x1133, new Tuple<string, string>("Armor Breaker", "Immediate medium Phys attack. Lower defense for 1 turn. (1 Enemy)") },
            { 0x1134, new Tuple<string, string>("Armor Destroyer", "Immediate heavy Phys attack. Lower defense for 1 turn. (1 Enemy)") },
            { 0x1381, new Tuple<string, string>("Fang Smash", "Light Phys attack. Lower attack for 3 turns. (1 Enemy)") },
            { 0x1382, new Tuple<string, string>("Kidney Smash", "Medium Phys attack. Lower attack for 3 turns. (1 Enemy)") },
            { 0x1383, new Tuple<string, string>("Giant Slice", "Light Phys attack that splashes to either side. (1 Enemy) Stronger in Boost") },
            { 0x1384, new Tuple<string, string>("Twin Slash", "2 Light Phys attacks. (1 Enemy) Stronger in Boost") },
            { 0x1385, new Tuple<string, string>("Lunge", "Light Phys attack. (1 Enemy) Stronger in Boost") },
            { 0x1386, new Tuple<string, string>("Triple Down", "3 medium Phys attacks. (1 Enemy) Stronger in Boost") },
            { 0x1387, new Tuple<string, string>("Mighty Swing", "Medium Phys attack that splashes to either side. (1 Enemy) Stronger in Boost") },
            { 0x138C, new Tuple<string, string>("Guillotine", "Light Phys attack, higher damage if target has ailment. (1 Enemy)") },
            { 0x138D, new Tuple<string, string>("Tempest Slash", "2-6 Medium Phys attacks. (1 Enemy) Stronger in Boost") },
            { 0x1390, new Tuple<string, string>("Arm Chopper", "Medium Phys attack, with medium chance of Strength Bind. (1 Enemy)") },
            { 0x1391, new Tuple<string, string>("Single Shot", "Light Phys attack that pierces to the back row. (1 Enemy) Stronger in Boost") },
            { 0x1395, new Tuple<string, string>("Assault Shot", "Medium Phys attack that pierces to the back row. (1 Enemy) Stronger in Boost") },
            { 0x1396, new Tuple<string, string>("Primal Force", "2 heavy Phys attacks. (1 Enemy) Stronger in Boost") },
            { 0x1397, new Tuple<string, string>("Weapon Crusher", "Heavy Phys attack. Lower attack for 3 turns. (1 Enemy)") },
            { 0x1398, new Tuple<string, string>("Broadshot", "Heavy Phys attack that pierces to the back row. (1 Enemy) Stronger in Boost") },
            { 0x1399, new Tuple<string, string>("Myriad Arrows", "6-8 heavy Phys attacks. (1 Enemy) Stronger in Boost") },
            { 0x139B, new Tuple<string, string>("Brain Shake", "Medium Phys attack, with high chance of Confusion. (1 Enemy)") },
            { 0x139F, new Tuple<string, string>("Blade of Fury", "Heavy Phys attack that splashes to either side. (1 Enemy) Stronger in Boost") },
            { 0x13A1, new Tuple<string, string>("Vorpal Blade", "4 heavy Phys attacks. (1 Enemy) Stronger in Boost") },
            { 0x13A3, new Tuple<string, string>("Headbutt", "Light Phys attack, with medium chance of Confusion. (1 Enemy)") },
            { 0x13A5, new Tuple<string, string>("Heat Wave", "Medium Phys attack. (All Enemies) Stronger in Boost") },
            { 0x13AE, new Tuple<string, string>("Deathbound", "Heavy Phys attack. (All Enemies) Stronger in Boost") },
            { 0x13B0, new Tuple<string, string>("Rampage", "2-6 Light Phys attacks to random targets. (All Enemies) Stronger in Boost") },
            { 0x13C1, new Tuple<string, string>("Hassou Tobi", "8 medium Phys attacks to random targets. (All Enemies) Stronger in Boost") },
            { 0x13C2, new Tuple<string, string>("God's Hand", "Severe Phys attack. (1 Enemy) Stronger in Boost") },
            { 0x1447, new Tuple<string, string>("Cross Slash", "Medium Phys attack that splashes to either side. (1 Enemy) Stronger in Boost") },
            { 0x1587, new Tuple<string, string>("Black Spot", "Heavy Phys attack. (1 Enemy) Stronger in Boost") },
            { 0x1588, new Tuple<string, string>("Herculean Strike", "3-6 medium Phys attacks to random targets. (All Enemies) Stronger in Boost") },
            { 0x1589, new Tuple<string, string>("Agneyastra", "Heavy Phys attacks. (All Enemies) Stronger in Boost") },
            { 0x158B, new Tuple<string, string>("Brave Blade", "Severe Phys attack that splashes to either side. (1 Enemy) Stronger in Boost") },
            { 0x158C, new Tuple<string, string>("Oneshot Kill", "Severe Phys attack that pierces to the back row. (1 Enemy) Stronger in Boost") },
            { 0x158D, new Tuple<string, string>("Calm Blade", "Medium Phys attack, with high chance of Hex. (1 Enemy) Stronger in Boost") },
            { 0x158E, new Tuple<string, string>("Phys-Ag Bind", "Medium Phys attack, with medium chance of Agility Bind. (1 Enemy)") },
            { 0x158F, new Tuple<string, string>("Vengeful Smash", "Medium Phys attacks to random targets, stronger for each hit taken. (All Enemies)") },
            { 0x1591, new Tuple<string, string>("Vengeful Melee", "Heavy Phys attacks to random targets, stronger for each hit taken. (All Enemies)") },
            { 0x1595, new Tuple<string, string>("Dream Needle", "Light Phys attack, with medium chance of Sleep. (1 Enemy)") },
            { 0x15AC, new Tuple<string, string>("Lucky Punch", "Tiny Phys damage. Low accuracy, but high critical rate. (1 Enemy)") },
            { 0x15AD, new Tuple<string, string>("Miracle Punch", "Medium Phys damage. Low accuracy, but high critical rate. (1 Enemy)") },
            { 0x15C4, new Tuple<string, string>("Guillotine Slice", "Medium Phys attack, higher damage if target has ailment. (1 Enemy)") },
            { 0x15C5, new Tuple<string, string>("Guillotine Blade", "Heavy Phys attack, higher damage if target has ailment. (1 Enemy)") },
            { 0x15C6, new Tuple<string, string>("Punishing Bite", "Med. Phys attack, higher damage if target is bound, but dispels Bind. (1 Enemy)") },
            { 0x15C7, new Tuple<string, string>("Punishing End", "Heavy Phys attack, higher damage if target is bound, dispels Bind. (1 Enemy)") },
            { 0x15C8, new Tuple<string, string>("Saint's Touch", "Heavy Phys attack, with medium chance of Magic Bind. (1 Enemy)") },
            { 0x15C9, new Tuple<string, string>("Arm Crusher", "Heavy Phys attack, with medium chance of Strength Bind. (1 Enemy)") },
        };

        #endregion SkillsPhys

        #region SkillsPsychokinesis

        internal static Dictionary<int, Tuple<string, string>> SkillsPsychochinesis = new Dictionary<int, Tuple<string, string>>()
        {
            { 0, new Tuple<string, string>("EMPTY", "") },

            { 0x1091, new Tuple<string, string>("Mind Strike", "Light Psy attack that splashes to either side. (1 Enemy)") },
            { 0x112D, new Tuple<string, string>("Mental Smash", "Medium Psy attack that splashes to either side. (1 Enemy)") },
            { 0x112E, new Tuple<string, string>("Psycho Smash", "Heavy Psy attack that splashes to either side. (1 Enemy)") },
            { 0x1367, new Tuple<string, string>("Psi", "Light Psy attack. (1 Enemy)") },
            { 0x1368, new Tuple<string, string>("Psio", "Medium Psy attack. (1 Enemy)") },
            { 0x1369, new Tuple<string, string>("Psiodyne", "Heavy Psy attack. (1 Enemy)") },
            { 0x136A, new Tuple<string, string>("Psycho Force", "Severe Psy attack. (1 Enemy)") },
            { 0x136B, new Tuple<string, string>("Mapsi", "Light Psy attack. (All Enemies)") },
            { 0x138E, new Tuple<string, string>("Mental Smash", "Medium Psy attack that splashes to either side. (1 Enemy)") },
            { 0x157E, new Tuple<string, string>("Psi", "Light Psy attack. (1 Enemy)") },
            { 0x157F, new Tuple<string, string>("Psio", "Medium Psy attack. (1 Enemy)") },
            { 0x1580, new Tuple<string, string>("Psiodyne", "Heavy Psy attack. (1 Enemy)") },
            { 0x1581, new Tuple<string, string>("Mapsi", "Light Psy attack. (All Enemies)") },
            { 0x1582, new Tuple<string, string>("Mapsio", "Medium Psy attack. (All Enemies)") },
            { 0x1583, new Tuple<string, string>("Mapsiodyne", "Heavy Psy attack. (All Enemies)") },
            { 0x1585, new Tuple<string, string>("Psycho Force", "Severe Psy attack. (1 Enemy)") },
            { 0x1586, new Tuple<string, string>("Psycho Blast", "Severe Psy attack. (All Enemies)") },
        };

        #endregion SkillsPsychokinesis

        #region SkillsReserved

        internal static Dictionary<int, Tuple<string, string>> SkillsReserved = new Dictionary<int, Tuple<string, string>>()
        {
            { 0, new Tuple<string, string>("EMPTY", "") },
            
            { 0x108D, new Tuple<string, string>("Zanshin Strike", "RESERVED") },
            { 0x1094, new Tuple<string, string>("Hammer Strike", "RESERVED") },
            { 0x10FC, new Tuple<string, string>("Fire Blessing", "RESERVED") },
            { 0x10FD, new Tuple<string, string>("Ice Blessing", "RESERVED") },
            { 0x10FE, new Tuple<string, string>("Elec Blessing", "RESERVED") },
            { 0x10FF, new Tuple<string, string>("Wind Blessing", "RESERVED") },
            { 0x1126, new Tuple<string, string>("Call to Hades", "RESERVED") },
            { 0x1128, new Tuple<string, string>("Calamity's Advent", "RESERVED") },
            { 0x1167, new Tuple<string, string>("Mini Recovery", "RESERVED") },
            { 0x1182, new Tuple<string, string>("Magic Circle", "RESERVED") },
            { 0x1184, new Tuple<string, string>("Magic Circle", "RESERVED") },
            { 0x1186, new Tuple<string, string>("Magic Circle", "RESERVED") },
            { 0x1188, new Tuple<string, string>("Magic Circle", "RESERVED") },
            { 0x118A, new Tuple<string, string>("Magic Circle", "RESERVED") },
            { 0x118C, new Tuple<string, string>("Magic Circle", "RESERVED") },
            { 0x118E, new Tuple<string, string>("Magic Circle", "RESERVED") },
            { 0x1197, new Tuple<string, string>("Heal Memento", "RESERVED") },
            { 0x1199, new Tuple<string, string>("Free Memento", "RESERVED") },
            { 0x119B, new Tuple<string, string>("Pure Memento", "RESERVED") },
            { 0x119C, new Tuple<string, string>("Blessing of Relief", "RESERVED") },
            { 0x119E, new Tuple<string, string>("Death Counter", "RESERVED") },
            { 0x11A0, new Tuple<string, string>("Death Chaser", "RESERVED") },
            { 0x11A2, new Tuple<string, string>("Fierce Stance", "RESERVED") },
            { 0x11A3, new Tuple<string, string>("Ferocious Stance", "RESERVED") },
            { 0x11A4, new Tuple<string, string>("Guard Stance", "RESERVED") },
            { 0x11A5, new Tuple<string, string>("Blank", "RESERVED") },
            { 0x11A7, new Tuple<string, string>("BLANK", "RESERVED") },
            { 0x11AA, new Tuple<string, string>("Fire Link", "RESERVED") },
            { 0x11AC, new Tuple<string, string>("Ice Link", "RESERVED") },
            { 0x11AE, new Tuple<string, string>("Elec Link", "RESERVED") },
            { 0x11B0, new Tuple<string, string>("Wind Link", "RESERVED") },
            { 0x11B3, new Tuple<string, string>("Smash Link", "RESERVED") },
            { 0x11B5, new Tuple<string, string>("Psy Link", "RESERVED") },
            { 0x11B7, new Tuple<string, string>("Nuclear Link", "RESERVED") },
            { 0x1200, new Tuple<string, string>("Dia Guard", "RESERVED") },
            { 0x1201, new Tuple<string, string>("Diarama Guard", "RESERVED") },
            { 0x1254, new Tuple<string, string>("Masochist Lash", "RESERVED") },
            { 0x14AF, new Tuple<string, string>("Drastic Measure", "RESERVED") },
            { 0x14B0, new Tuple<string, string>("Endure", "RESERVED") },
            { 0x14B1, new Tuple<string, string>("Rabbit's Foot", "RESERVED") },
            { 0x14B2, new Tuple<string, string>("Deafening Roar", "RESERVED") },
            { 0x14B4, new Tuple<string, string>("Gears of Time", "RESERVED") },
            { 0x14B5, new Tuple<string, string>("Time Warp", "RESERVED") },
            { 0x14B6, new Tuple<string, string>("Death Sentence", "RESERVED") },
            { 0x14B7, new Tuple<string, string>("Time Reversal", "RESERVED") },
            { 0x14B8, new Tuple<string, string>("Time Stop", "RESERVED") },
            { 0x14C5, new Tuple<string, string>("Cloak of Despair", "RESERVED") },
            { 0x14E6, new Tuple<string, string>("Kamoshidaman!", "RESERVED") },
            { 0x14F3, new Tuple<string, string>("Gale Link", "RESERVED") },
        };

        #endregion SkillsReserved

        #region SkillsUnison

        internal static Dictionary<int, Tuple<string, string>> SkillsUnison = new Dictionary<int, Tuple<string, string>>()
        {
            { 0, new Tuple<string, string>("EMPTY", "") },

            { 0x1501, new Tuple<string, string>("Skull Bro-Bash", "Skill by two tough, manly boys: Ryuji/Kanji") },
            { 0x1502, new Tuple<string, string>("Brand☆New☆Bam", "Skill by two endlessly cheerful girls: Ann/Chie") },
            { 0x1503, new Tuple<string, string>("Kacho Fugetsu", "Skill by two paragons of grace: Yukiko/Yusuke") },
            { 0x1504, new Tuple<string, string>("Lightning Duo", "Skill by two wild card team leaders: /P5///P4/") },
            { 0x1505, new Tuple<string, string>("Prince Pincer", "Skill by Detective Princes: Akechi/Naoto") },
            { 0x1506, new Tuple<string, string>("Severed Fate", "Skill by heiresses unbound: Haru/Yukiko") },
            { 0x1507, new Tuple<string, string>("Presidential Combo", "Skill by student council presidents: Makoto/Mitsuru") },
            { 0x1508, new Tuple<string, string>("Strike of Bonds", "Skill by a heroine and her classmates: /P3F//Yukari/Junpei/Fuuka") },
            { 0x1509, new Tuple<string, string>("Roar of Hunger", "Skill by three hungry lads: Yusue/Shinjiro/Akihiko") },
            { 0x150A, new Tuple<string, string>("Trainee Air Raid", "Skill by thieves and their disciple: Ken/Morgana/Ryuji/Ann") },
            { 0x150B, new Tuple<string, string>("Charge of Trust", "Skill by a heroine and her senpai: /P3F//Mitsuru/Akihiko/Shinjiro") },
            { 0x150C, new Tuple<string, string>("Animal Capriccio", "Skill by (mostly) lovable enigmas: Morgana/Teddie/Koromaru") },
            { 0x150D, new Tuple<string, string>("Pair of Aces", "Skill by wild cards with guts of steel: /P3M///P4/") },
            { 0x150E, new Tuple<string, string>("Destined Strike", "Skill by a heroine and her new bonds: /P3F//Aigis/Koromaru/Ken") },
            { 0x150F, new Tuple<string, string>("Double Trigger", "Skill by two mature wild cards: /P5///P3M/") },
            { 0x1510, new Tuple<string, string>("Beauty Quartet", "Skill by four beauties(?): Haru/Aigis/Rise/Teddie") },
            { 0x1511, new Tuple<string, string>("Triple Support", "Skill by three navigators: Futaba/Rise/Fuuka") },
            { 0x1512, new Tuple<string, string>("Martial Artistry", "Skill by three martial artists: Makoto/Akihiko/Chie") },
            { 0x1513, new Tuple<string, string>("Partner Power", "Skill by three partners(?) of the wild cards: Junpei/Yosuke/Ryuji") },
            { 0x1514, new Tuple<string, string>("Wildcard Order", "Skill by four wild cards: /P5///P3F///P4///P3M/") },
        };

        #endregion SkillsUnison

        #region SkillsWind

        internal static Dictionary<int, Tuple<string, string>> SkillsWind = new Dictionary<int, Tuple<string, string>>()
        {
            { 0, new Tuple<string, string>("EMPTY", "") },

            { 0x100E, new Tuple<string, string>("Garu", "Light Wind attack. (1 Enemy)") },
            { 0x100F, new Tuple<string, string>("Garula", "Medium Wind attack. (1 Enemy)") },
            { 0x1010, new Tuple<string, string>("Garudyne", "Heavy Wind attack. (1 Enemy)") },
            { 0x1011, new Tuple<string, string>("Magaru", "Light Wind attack. (All Enemies)") },
            { 0x1012, new Tuple<string, string>("Magarula", "Medium Wind attack. (All Enemies)") },
            { 0x1013, new Tuple<string, string>("Magarudyne", "Heavy Wind attack. (All Enemies)") },
            { 0x1014, new Tuple<string, string>("Cyclone", "2-3 light Wind attacks to random targets. (1 Row)") },
            { 0x1015, new Tuple<string, string>("Panta Rhei", "Severe Wind attack that splashes to either side. (1 Enemy)") },
            { 0x10C0, new Tuple<string, string>("Jinpugeki", "Light Wind attack that splashes to either side. (1 Enemy)") },
            { 0x10CA, new Tuple<string, string>("Reppu Strike", "Medium Wind attack that splashes to either side. (1 Enemy)") },
            { 0x10CB, new Tuple<string, string>("Kamikaze Strike", "Heavy Wind attack that splashes to either side. (1 Enemy)") },
            { 0x130B, new Tuple<string, string>("Garu", "Light Wind attack. (1 Enemy)") },
            { 0x130C, new Tuple<string, string>("Garula", "Medium Wind attack. (1 Enemy)") },
            { 0x130D, new Tuple<string, string>("Garudyne", "Heavy Wind attack. (1 Enemy)") },
            { 0x130E, new Tuple<string, string>("Magaru", "Light Wind attack. (All Enemies)") },
            { 0x130F, new Tuple<string, string>("Magarula", "Medium Wind attack. (All Enemies)") },
            { 0x1310, new Tuple<string, string>("Magarudyne", "Heavy Wind attack. (All Enemies)") },
            { 0x1311, new Tuple<string, string>("Wind Dance", "2-3 light Wind attacks to random targets. (All Enemies)") },
            { 0x1312, new Tuple<string, string>("Gust Dance", "2-3 medium Wind attacks to random targets. (All Enemies)") },
            { 0x1313, new Tuple<string, string>("Hurricane Dance", "2-3 heavy Wind attacks to random targets. (All Enemies)") },
            { 0x1372, new Tuple<string, string>("Panta Rhei", "Severe Wind attack that splashes to either side. (1 Enemy)") },
            { 0x1373, new Tuple<string, string>("Divine Vacuum", "Heavy Wind attack. (All Enemies)") },
        };

        #endregion SkillsWind

        #endregion Skills

        #region Weapons

        #region WeaponsAigis

        internal static Dictionary<int, Tuple<string, string>> WeaponsAigis = new Dictionary<int, Tuple<string, string>>()
        {
            { 0, new Tuple<string, string>("EMPTY", "") },

            { 0x2A6, new Tuple<string, string>("Albireo", "ATK 119, Ranged, Aigis") },
            { 0x2A7, new Tuple<string, string>("Carbon Railgun", "ATK 140, Ranged/Medium piercing damage, Aigis") },
            { 0x2A8, new Tuple<string, string>("Medusa", "ATK 146, Ranged/Medium splash damage, Aigis") },
            { 0x2A9, new Tuple<string, string>("Maxima Sniper", "ATK 159, Ranged/Critical rate +10%, Aigis") },
            { 0x2AA, new Tuple<string, string>("Axion IG", "ATK 165, Ranged/Low chance of Hex, Aigis") },
            { 0x2AB, new Tuple<string, string>("Lime Howitzer", "ATK 174, Ranged/Medium chance of Agility Bind, Aigis") },
            { 0x2AC, new Tuple<string, string>("Kiss of Athena", "ATK 183, Ranged/Medium splash damage, Aigis") },
            { 0x2AD, new Tuple<string, string>("Pandemonium", "ATK 204, Ranged/Medium chance of Magic Bind, Aigis") },
            { 0x2AE, new Tuple<string, string>("Antimatter Gun", "ATK 220, Ranged/High chance of Strength Bind, Aigis") },
            { 0x2AF, new Tuple<string, string>("Metatronius", "ATK 243, Ranged/Halves HP cost/En +7, Aigis") },
            { 0x2B1, new Tuple<string, string>("Grenade 80", "ATK 124, Ranged/Light splash damage, Aigis") },
            { 0x2B2, new Tuple<string, string>("Six-Shot", "ATK 132, Ranged/Light piercing damage, Aigis") },
        };

        #endregion WeaponsAigis

        #region WeaponsAkechi

        internal static Dictionary<int, Tuple<string, string>> WeaponsAkechi = new Dictionary<int, Tuple<string, string>>()
        {
            { 0, new Tuple<string, string>("EMPTY", "") },

            { 0x81, new Tuple<string, string>("Beam Saber", "ATK 26, Akechi") },
            { 0x82, new Tuple<string, string>("Lumina Saber", "ATK 39, Accuracy +5%, Akechi") },
            { 0x83, new Tuple<string, string>("Toxoid Saber", "ATK 46, Low chance of Poison, Akechi") },
            { 0x84, new Tuple<string, string>("Violet Saber", "ATK 56, Ma +2, Akechi") },
            { 0x85, new Tuple<string, string>("Eclat Saber", "ATK 69, Akechi") },
            { 0x87, new Tuple<string, string>("Particle Saber", "ATK 92, Max SP +10%, Akechi") },
            { 0x88, new Tuple<string, string>("Stellar Saber", "ATK 105, Max HP +10%, Akechi") },
            { 0x89, new Tuple<string, string>("Alpha Saber", "ATK 116, Max SP +5%, Akechi") },
            { 0x8A, new Tuple<string, string>("Erg Saber", "ATK 121, Slightly raise Party Meter gains, Akechi") },
            { 0x8B, new Tuple<string, string>("Laplace Saber", "ATK 132, Critical rate +5%/Ag +2, Akechi") },
            { 0x8C, new Tuple<string, string>("Ion Saber", "ATK 141, Max HP & SP +10%, Akechi") },
            { 0x8D, new Tuple<string, string>("Beta Saber", "ATK 148, Ag +2/Low chance of Sleep, Akechi") },
            { 0x8E, new Tuple<string, string>("Anode Saber", "ATK 161, Max HP +15%, Akechi") },
            { 0x8F, new Tuple<string, string>("Cathode Saber", "ATK 168, Max SP +15%, Akechi") },
            { 0x90, new Tuple<string, string>("Neutron Saber", "ATK 177, Medium chance of Agility Bind, Akechi") },
            { 0x91, new Tuple<string, string>("Gamma Saber", "ATK 184, Medium damage with piercing, Akechi") },
            { 0x92, new Tuple<string, string>("Force Saber", "ATK 206, Max HP +10%/Ag +3, Akechi") },
            { 0x93, new Tuple<string, string>("Omega Saber", "ATK 222, Heavy splash damage, Akechi") },
            { 0x94, new Tuple<string, string>("Tsar Saber", "ATK 246, Halves HP cost/St +3/Ma +6, Akechi") },
            { 0x96, new Tuple<string, string>("Riryoku Saber", "ATK 32, Max SP +5%, Akechi") },
            { 0x97, new Tuple<string, string>("Arabian Saber", "ATK 29, Critical rate +5%, Akechi") },
        };

        #endregion

        #region WeaponsAkihikoMakoto

        internal static Dictionary<int, Tuple<string, string>> WeaponsAkihikoMakoto = new Dictionary<int, Tuple<string, string>>()
        {
            { 0, new Tuple<string, string>("EMPTY", "") },

            { 0x103, new Tuple<string, string>("Brass Knuckles", "ATK 68, Akihiko/Makoto") },
            { 0x104, new Tuple<string, string>("Heavy Grip", "ATK 83, Akihiko/Makoto") },
            { 0x105, new Tuple<string, string>("Coin Knuckles", "ATK 92, Lu +3, Akihiko/Makoto") },
            { 0x106, new Tuple<string, string>("Delta Knuckles", "ATK 109, Akihiko/Makoto") },
            { 0x107, new Tuple<string, string>("Hard Rocker", "ATK 124, Critical rate +5%, Akihiko/Makoto") },
            { 0x108, new Tuple<string, string>("Hell Knuckles", "ATK 137, Low chance of Instant Kill, Akihiko/Makoto") },
            { 0x109, new Tuple<string, string>("Sonic Fist", "ATK 142, Critical rate +10%/Ag +2, Akihiko/Makoto") },
            { 0x10A, new Tuple<string, string>("Titanic Knuckles", "ATK 153, Max HP +15%, Akihiko/Makoto") },
            { 0x10B, new Tuple<string, string>("Metal Duster", "ATK 163, Max SP +10%/Ma +3, Akihiko/Makoto") },
            { 0x10C, new Tuple<string, string>("Pugilist's Fists", "ATK 171, Critical rate +5%/Ag +2, Akihiko/Makoto") },
            { 0x10D, new Tuple<string, string>("Golden Gloves", "ATK 183, Max HP +10%/Lu +3, Akihiko/Makoto") },
            { 0x10E, new Tuple<string, string>("Omega Knuckle", "ATK 190, Critical rate +10%/En +3, Akihiko/Makoto") },
            { 0x10F, new Tuple<string, string>("Dragon Fangs", "ATK 199, Max HP +10%/St +3, Akihiko/Makoto") },
            { 0x110, new Tuple<string, string>("Ominous Fists", "ATK 207, Max HP & SP +10%, Akihiko/Makoto") },
            { 0x111, new Tuple<string, string>("Sabazios", "ATK 229, Medium chance of Magic Bind, Akihiko/Makoto") },
            { 0x112, new Tuple<string, string>("Double Ziggurat", "ATK 245, Critical rate +20%/St +2, Akihiko/Makoto") },
            { 0x113, new Tuple<string, string>("Pegasus Fist", "ATK 264, Halves HP cost/St +4/Ag +5, Makoto") },
            { 0x114, new Tuple<string, string>("Ultimate Fist", "ATK 272, Halves HP cost/En +4/Ag +4, Akihiko") },
            { 0x115, new Tuple<string, string>("Champion Gloves", "ATK 128, Akihiko") },
            { 0x117, new Tuple<string, string>("Iron Fist", "ATK 100, Light splash damage, Akihiko/Makoto") },
        };

        #endregion WeaponsAkihikoMakoto

        #region WeaponsAnn

        internal static Dictionary<int, Tuple<string, string>> WeaponsAnn = new Dictionary<int, Tuple<string, string>>()
        {
            { 0, new Tuple<string, string>("EMPTY", "") },

            { 0x41, new Tuple<string, string>("Leather Whip", "ATK 22, Ranged, Ann") },
            { 0x43, new Tuple<string, string>("Training Whip", "ATK 40, Ranged, Ann") },
            { 0x44, new Tuple<string, string>("Fundo-Kusari", "ATK 48, Ranged/Low chance of Confusion, Ann") },
            { 0x45, new Tuple<string, string>("Fierce Whip", "ATK 60, Ranged/Critical rate +5%, Ann") },
            { 0x47, new Tuple<string, string>("Manacle Whip", "ATK 80, Ranged/Low chance of Agility Bind, Ann") },
            { 0x48, new Tuple<string, string>("Fern Whip", "ATK 93, Ranged/Light splash damage, Ann") },
            { 0x49, new Tuple<string, string>("Skin Ripper", "ATK 103, Ranged/Critical rate +5%, Ann") },
            { 0x4A, new Tuple<string, string>("Electromag Whip", "ATK 108, Ranged/Low chance of Paralysis, Ann") },
            { 0x4B, new Tuple<string, string>("Chain Whip", "ATK 118, Ranged/Low chance of pierce, Ann") },
            { 0x4C, new Tuple<string, string>("Burn Whip", "ATK 128, Ranged/Medium splash damage, Ann") },
            { 0x4D, new Tuple<string, string>("Hero Whip", "ATK 135, Ranged/Max HP & SP +5%, Ann") },
            { 0x4E, new Tuple<string, string>("Agony Whip", "ATK 146, Ranged/Critical rate +10%, Ann") },
            { 0x4F, new Tuple<string, string>("White Rose Whip", "ATK 153, Ranged/Max SP +5%/Lu +3, Ann") },
            { 0x50, new Tuple<string, string>("Fengshen Whip", "ATK 162, Ranged/Medium chance of Magic Bind, Ann") },
            { 0x51, new Tuple<string, string>("Nine Tails", "ATK 171, Ranged/Max SP +10%, Ann") },
            { 0x52, new Tuple<string, string>("Blue Rose Whip", "ATK 191, Ranged/Low chance of any Bind, Ann") },
            { 0x53, new Tuple<string, string>("Queen's Whip", "ATK 207, Ranged/High chance of Sleep, Ann") },
            { 0x54, new Tuple<string, string>("Golden Whip", "ATK 231, Ranged/Halves HP cost/Ma +7, Ann") },
            { 0x56, new Tuple<string, string>("Rosebell Whip", "ATK 30, Ranged, Ann") },
            { 0x57, new Tuple<string, string>("Explorer's Whip", "ATK 26, Ranged/Ag +1, Ann") },
        };

        #endregion WeaponsAnn

        #region WeaponsChie

        internal static Dictionary<int, Tuple<string, string>> WeaponsChie = new Dictionary<int, Tuple<string, string>>()
        {
            { 0, new Tuple<string, string>("EMPTY", "") },

            { 0x143, new Tuple<string, string>("Leather Shoes", "ATK 84, Chie") },
            { 0x144, new Tuple<string, string>("Safety Shoes", "ATK 92, En +1, Chie") },
            { 0x145, new Tuple<string, string>("Platform Sneaks", "ATK 110, Accuracy +10%, Chie") },
            { 0x146, new Tuple<string, string>("Dinosaur Legs", "ATK 125, Critical rate +10%, Chie") },
            { 0x147, new Tuple<string, string>("Nanman Gusoku", "ATK 138, Max HP +10%/Lu +1, Chie") },
            { 0x148, new Tuple<string, string>("Cobra Boots", "ATK 143, Medium chance of Poison, Chie") },
            { 0x149, new Tuple<string, string>("Bucking Broncos", "ATK 154, Ag +4, Chie") },
            { 0x14A, new Tuple<string, string>("Bishamonten", "ATK 159, Max HP & SP +10%, Chie") },
            { 0x14B, new Tuple<string, string>("Confusion Boots", "ATK 171, Low chance of Confusion, Chie") },
            { 0x14C, new Tuple<string, string>("Steel Slippers", "ATK 184, En +4, Chie") },
            { 0x14D, new Tuple<string, string>("Nice Shoes", "ATK 190, Lu +5, Chie") },
            { 0x14E, new Tuple<string, string>("Vidar's Boots", "ATK 200, Max HP +15%, Chie") },
            { 0x14F, new Tuple<string, string>("Sleipnir", "ATK 208, Critical rate +10%/Ag +2, Chie") },
            { 0x150, new Tuple<string, string>("Moses Sandals", "ATK 230, Max SP +15%/Ma +2, Chie") },
            { 0x151, new Tuple<string, string>("Stellar Greaves", "ATK 247, Critical rate +20%/Ag +2, Chie") },
            { 0x152, new Tuple<string, string>("Reigan Greaves", "ATK 271, Halves HP cost/ST +6/Ag +2, Chie") },
            { 0x154, new Tuple<string, string>("Verdure Greaves", "ATK 102, Ag +2, Chie") },
            { 0x155, new Tuple<string, string>("Furinkazan", "ATK 167, All stats +1, Chie") },
        };

        #endregion WeaponsChie

        #region WeaponsHaru

        internal static Dictionary<int, Tuple<string, string>> WeaponsHaru = new Dictionary<int, Tuple<string, string>>()
        {
            { 0, new Tuple<string, string>("EMPTY", "") },

            { 0xE3, new Tuple<string, string>("Axe", "ATK 68, Haru") },
            { 0xE4, new Tuple<string, string>("Shield Axe", "ATK 84, En +2, Haru") },
            { 0xE5, new Tuple<string, string>("Headbanger", "ATK 92, Slight chance of Confusion, Haru") },
            { 0xE6, new Tuple<string, string>("Bardiche", "ATK 110, Accuracy +10%, Haru") },
            { 0xE7, new Tuple<string, string>("Basalt Axe", "ATK 125, Critical rate +5%/En +2, Haru") },
            { 0xE8, new Tuple<string, string>("Mega Axe", "ATK 138, Max HP +10%, Haru") },
            { 0xE9, new Tuple<string, string>("Warborn", "ATK 143, Critical rate +10%/En +2, Haru") },
            { 0xEA, new Tuple<string, string>("Bastion Axe", "ATK 154, En +4, Haru") },
            { 0xEB, new Tuple<string, string>("Morph Axe", "ATK 165, En +3/Ag +2, Haru") },
            { 0xEC, new Tuple<string, string>("Death Contract", "ATK 171, En +1/Critical rate +10%, Haru") },
            { 0xED, new Tuple<string, string>("Erik's Bloodaxe", "ATK 184, Low chance of Instant Kill, Haru") },
            { 0xEE, new Tuple<string, string>("Crescent Axe", "ATK 190, En +3, Haru") },
            { 0xEF, new Tuple<string, string>("Lethe Axe", "ATK 200, Medium chance of Confusion, Haru") },
            { 0xF0, new Tuple<string, string>("Celtis", "ATK 208, Max HP +15%/Lu +2, Haru") },
            { 0xF1, new Tuple<string, string>("Gilgamesh Axe", "ATK 230, Low chance of Strength Bind/En +3, Haru") },
            { 0xF2, new Tuple<string, string>("Amazon Axe", "ATK 247, Heavy splash damage, Haru") },
            { 0xF3, new Tuple<string, string>("Fleurs du Mal", "ATK 271, Halves HP cost/ST +3/En +5, Haru") },
            { 0xF5, new Tuple<string, string>("Marble Axe", "ATK 118, Lu +3, Haru") },
        };

        #endregion WeaponsHaru

        #region WeaponsKanji

        internal static Dictionary<int, Tuple<string, string>> WeaponsKanji = new Dictionary<int, Tuple<string, string>>()
        {
            { 0, new Tuple<string, string>("EMPTY", "") },

            { 0x1A3, new Tuple<string, string>("Folding Chair", "ATK 81, Kanji") },
            { 0x1A4, new Tuple<string, string>("Iron Plate", "ATK 89, En +2, Kanji") },
            { 0x1A5, new Tuple<string, string>("Steel Plate", "ATK 106, En +3, Kanji") },
            { 0x1A6, new Tuple<string, string>("Power Plate", "ATK 121, Critical rate +5%/En +2, Kanji") },
            { 0x1A7, new Tuple<string, string>("Hard Board", "ATK 133, Max HP +10%, Kanji") },
            { 0x1A8, new Tuple<string, string>("Golden Plate", "ATK 138, Money obtained +10%, Kanji") },
            { 0x1A9, new Tuple<string, string>("Round Shield", "ATK 149, En +4, Kanji") },
            { 0x1AA, new Tuple<string, string>("Drowsy Shield", "ATK 160, Medium chance of Sleep, Kanji") },
            { 0x1AB, new Tuple<string, string>("Alloy Plate", "ATK 166, Max HP +10%/En +2, Kanji") },
            { 0x1AC, new Tuple<string, string>("Jet Shield", "ATK 179, En +2/Ag +3, Kanji") },
            { 0x1AD, new Tuple<string, string>("Oni-Gawara", "ATK 186, En +5, Kanji") },
            { 0x1AE, new Tuple<string, string>("Galahad's Shield", "ATK 195, Max HP +10%/En +3, Kanji") },
            { 0x1AF, new Tuple<string, string>("Asturias", "ATK 203, Max HP +15%/Lu +3, Kanji") },
            { 0x1B0, new Tuple<string, string>("Wisdom Shield", "ATK 225, Medium chance of Confusion, Kanji") },
            { 0x1B1, new Tuple<string, string>("Christ Mirror", "ATK 241, Max HP +20%, Kanji") },
            { 0x1B2, new Tuple<string, string>("Destiny Tablet", "ATK 266, Halves HP cost/St +4/En +5, Kanji") },
            { 0x1B4, new Tuple<string, string>("Origin Stone", "ATK 112, Lu +3, Kanji") },
        };

        #endregion WeaponsKanji

        #region WeaponsKen

        internal static Dictionary<int, Tuple<string, string>> WeaponsKen = new Dictionary<int, Tuple<string, string>>()
        {
            { 0, new Tuple<string, string>("EMPTY", "") },

            { 0x265, new Tuple<string, string>("Cross Spear", "ATK 103, Ranged, Ken") },
            { 0x266, new Tuple<string, string>("Sexy Lance", "ATK 119, Ranged/Max SP +5%/Ma +2, Ken") },
            { 0x267, new Tuple<string, string>("Framea", "ATK 130, Ranged/Light piercing damage, Ken") },
            { 0x268, new Tuple<string, string>("Scorpion", "ATK 140, Ranged/Medium chance of Poison, Ken") },
            { 0x269, new Tuple<string, string>("Ouka Jumonji", "ATK 146, Ranged/Max HP & SP +5%, Ken") },
            { 0x26A, new Tuple<string, string>("Oomiyari", "ATK 159, Ranged/Medium splash damage, Ken") },
            { 0x26B, new Tuple<string, string>("Ote-gine", "ATK 165, Ranged/St +3/En +2, Ken") },
            { 0x26C, new Tuple<string, string>("Trident", "ATK 174, Ranged/Max SP +15%, Ken") },
            { 0x26D, new Tuple<string, string>("Romulus's Spear", "ATK 183, Ranged/Max HP +15%, Ken") },
            { 0x26E, new Tuple<string, string>("Tonbokiri", "ATK 204, Ranged/Critical rate +15%, Ken") },
            { 0x26F, new Tuple<string, string>("Pinaka", "ATK 220, Ranged/High chance of Agility Bind, Ken") },
            { 0x270, new Tuple<string, string>("Gae Bolg", "ATK 243, Ranged/Halves HP cost/St +7, Ken") },
            { 0x272, new Tuple<string, string>("Javelin", "ATK 115, Ranged/Critical rate +5%, Ken") },
        };

        #endregion WeaponsKen

        #region WeaponsMitsuru

        internal static Dictionary<int, Tuple<string, string>> WeaponsMitsuru = new Dictionary<int, Tuple<string, string>>()
        {
            { 0, new Tuple<string, string>("EMPTY", "") },

            { 0x245, new Tuple<string, string>("Rapier", "ATK 101, Mitsuru") },
            { 0x246, new Tuple<string, string>("Silver Rapier", "ATK 117, Max SP +5%/Ma +2, Mitsuru") },
            { 0x247, new Tuple<string, string>("Fleuret", "ATK 127, Ag +3, Mitsuru") },
            { 0x248, new Tuple<string, string>("Espada Ropera", "ATK 138, Medium piercing damage, Mitsuru") },
            { 0x249, new Tuple<string, string>("Shadow Dancer", "ATK 144, Critical rate +10%, Mitsuru") },
            { 0x24A, new Tuple<string, string>("Eternal Blade", "ATK 156, Max SP +10%/Ma +2, Mitsuru") },
            { 0x24B, new Tuple<string, string>("Stiletto", "ATK 163, Critical rate +15%, Mitsuru") },
            { 0x24C, new Tuple<string, string>("Bloody Estoc", "ATK 172, Low chance of Instant Kill, Mitsuru") },
            { 0x24D, new Tuple<string, string>("Final Estoc", "ATK 181, Medium piercing damage, Mitsuru") },
            { 0x24E, new Tuple<string, string>("Illuminati", "ATK 202, Max SP +10%/Ma +3, Mitsuru") },
            { 0x24F, new Tuple<string, string>("Damascus Rapier", "ATK 217, Max SP +20%, Mitsuru") },
            { 0x250, new Tuple<string, string>("Kokuseki Senjin", "ATK 242, Halves HP cost/Ma +5/Ag +3, Mitsuru") },
            { 0x252, new Tuple<string, string>("Flamberge", "ATK 113, Light piercing damage, Mitsuru") },
        };

        #endregion WeaponsMitsuru

        #region WeaponsMorgana

        internal static Dictionary<int, Tuple<string, string>> WeaponsMorgana = new Dictionary<int, Tuple<string, string>>()
        {
            { 0, new Tuple<string, string>("EMPTY", "") },

            { 0xA1, new Tuple<string, string>("Slingbow", "ATK 32, Ranged, Morgana") },
            { 0xA2, new Tuple<string, string>("Mount Liang", "ATK 50, Ranged, Morgana") },
            { 0xA3, new Tuple<string, string>("Large Slingshot", "ATK 62, Ranged/Critical rate +5%, Morgana") },
            { 0xA4, new Tuple<string, string>("Paralytic Sling", "ATK 69, Low chance of Paralysis, Morgana") },
            { 0xA5, new Tuple<string, string>("Shooting Star", "ATK 84, Ranged/Ag +2, Morgana") },
            { 0xA7, new Tuple<string, string>("Rubble Launcher", "ATK 96, Ranged/Low chance of Confusion, Morgana") },
            { 0xA8, new Tuple<string, string>("Catnap", "ATK 107, Ranged/Low chance of Sleep, Morgana") },
            { 0xA9, new Tuple<string, string>("Northern Light", "ATK 112, Ranged/Max SP +5%, Morgana") },
            { 0xAA, new Tuple<string, string>("Strongarm", "ATK 121, Ranged/Critical rate +10%, Morgana") },
            { 0xAB, new Tuple<string, string>("Interceptor", "ATK 131, Ranged/Medium chance of Magic Bind, Morgana") },
            { 0xAC, new Tuple<string, string>("Comet 3", "ATK 138, Ranged/Light splash damage, Morgana") },
            { 0xAD, new Tuple<string, string>("Overthrower", "ATK 150, Ranged/Light piercing damage, Morgana") },
            { 0xAE, new Tuple<string, string>("David", "ATK 157, Ranged/Low chance of Instant Kill, Morgana") },
            { 0xAF, new Tuple<string, string>("Grand Fireworks", "ATK 166, Ranged/Medium splash damage, Morgana") },
            { 0xB0, new Tuple<string, string>("Star Slayer", "ATK 174, Ranged/Medium chance of confusion, Morgana") },
            { 0xB1, new Tuple<string, string>("Tom Sawyer", "ATK 195, Ranged/Critical rate +15%, Morgana") },
            { 0xB2, new Tuple<string, string>("Chromatic Orb", "ATK 211, Ranged/High chance of Paralysis, Morgana") },
            { 0xB3, new Tuple<string, string>("Utopia", "ATK 235, Ranged/Halves HP cost/Ag +8, Morgana") },
            { 0xB5, new Tuple<string, string>("Catapult", "ATK 36, Ranged/Critical rate +5%, Morgana") },
            { 0xB6, new Tuple<string, string>("Sleep Sling", "ATK 42, Ranged/Low chance of Sleep, Morgana") },
            { 0xB7, new Tuple<string, string>("Bomber Slingshot", "ATK 76, Ranged/Light splash damage, Morgana") },
        };

        #endregion WeaponsMorgana

        #region WeaponsNaoto

        internal static Dictionary<int, Tuple<string, string>> WeaponsNaoto = new Dictionary<int, Tuple<string, string>>()
        {
            { 0, new Tuple<string, string>("EMPTY", "") },

            { 0x1C3, new Tuple<string, string>("Nambu 2", "ATK 61, Ranged, Naoto") },
            { 0x1C4, new Tuple<string, string>("Smith Gun", "ATK 68, Ranged/Accuracy +5%, Naoto") },
            { 0x1C5, new Tuple<string, string>("Peacemaker", "ATK 82, Ranged/Lu +2, Naoto") },
            { 0x1C6, new Tuple<string, string>("Raging Bull", "ATK 94, Ranged/Light piercing damage, Naoto") },
            { 0x1C7, new Tuple<string, string>("Crimson Dirge", "ATK 105, Ranged/En +1/Ag +2, Naoto") },
            { 0x1C8, new Tuple<string, string>("Algernon", "ATK 110, Ranged/Max HP +10%, Naoto") },
            { 0x1C9, new Tuple<string, string>("44 Anaconda", "ATK 119, Ranged/Low chance of Poison, Naoto") },
            { 0x1CA, new Tuple<string, string>("Shield Gun", "ATK 130, Ranged/En +5, Naoto") },
            { 0x1CB, new Tuple<string, string>("Sci-fi Raygun", "ATK 136, Ranged/Max SP +10%, Naoto") },
            { 0x1CC, new Tuple<string, string>("Camel Red", "ATK 148, Ranged/Medium chance of Hex, Naoto") },
            { 0x1CD, new Tuple<string, string>("Chrome Heart", "ATK 155, Ranged/Ma +4, Naoto") },
            { 0x1CE, new Tuple<string, string>("Magical Gun", "ATK 164, Ranged/Max SP +15%, Naoto") },
            { 0x1CF, new Tuple<string, string>("Unlimited", "ATK 173, Ranged/En +3, Naoto") },
            { 0x1D0, new Tuple<string, string>("Judge of Hell", "ATK 193, Ranged/Low chance of Instant Kill, Naoto") },
            { 0x1D1, new Tuple<string, string>("Black Hole", "ATK 209, Ranged/High chance of Hex, Naoto") },
            { 0x1D2, new Tuple<string, string>("Megido Gun", "ATK 232, Ranged/Halves HP cost/Ma +7, Naoto") },
            { 0x1D4, new Tuple<string, string>("Baby Arquebus", "ATK 75, Ranged/Critical rate +10%, Naoto") },
        };

        #endregion WeaponsNaoto

        #region WeaponsP3F

        internal static Dictionary<int, Tuple<string, string>> WeaponsP3F = new Dictionary<int, Tuple<string, string>>()
        {
            { 0, new Tuple<string, string>("EMPTY", "") },

            { 0xC2, new Tuple<string, string>("Fake Naginata", "ATK 32, Ranged, /P3F/") },
            { 0xC3, new Tuple<string, string>("Ardor Naginata", "ATK 51, Ranged/Max HP +10%, /P3F/") },
            { 0xC4, new Tuple<string, string>("Tsukushi", "ATK 64, Ranged, /P3F/") },
            { 0xC5, new Tuple<string, string>("Emerald Stick", "ATK 71, Ranged/Ag +3, /P3F/") },
            { 0xC6, new Tuple<string, string>("Dai Naginata", "ATK 85, Ranged/Light splash damage, /P3F/") },
            { 0xC7, new Tuple<string, string>("Primal Naginata", "ATK 98, Ranged/En +2/Ag +2, /P3F/") },
            { 0xC8, new Tuple<string, string>("Youth Stick", "ATK 109, Ranged/Critical rate +10%, /P3F/") },
            { 0xC9, new Tuple<string, string>("Muso Naginata", "ATK 114, Ranged/Ag +3, /P3F/") },
            { 0xCA, new Tuple<string, string>("Kobayashi", "ATK 123, Ranged/Max HP +10%, /P3F/") },
            { 0xCB, new Tuple<string, string>("Beam Naginata", "ATK 133, Ranged/Medium chance of Strength Bind, /P3F/") },
            { 0xCC, new Tuple<string, string>("Victory Stick", "ATK 140, Ranged/Max HP +15%, /P3F/") },
            { 0xCD, new Tuple<string, string>("Future Naginata", "ATK 153, Ranged/Ma +2/Ag +2, /P3F/") },
            { 0xCE, new Tuple<string, string>("Nikko Naginata", "ATK 159, Ranged/All stats +1, /P3F/") },
            { 0xCF, new Tuple<string, string>("Origin Naginata", "ATK 168, Ranged/Low chance of any Bind, /P3F/") },
            { 0xD0, new Tuple<string, string>("Sonobe Naginata", "ATK 177, Ranged/Max HP +15%, /P3F/") },
            { 0xD1, new Tuple<string, string>("Khakkhara", "ATK 197, Ranged/Ma +4, /P3F/") },
            { 0xD2, new Tuple<string, string>("Shizuka", "ATK 213, Ranged/High chance of Strength Bind, /P3F/") },
            { 0xD3, new Tuple<string, string>("Kanemitsu", "ATK 237, Ranged/Halves HP cost/Ma +7, /P3F/") },
            { 0xD5, new Tuple<string, string>("Lacrosse Stick", "ATK 42, Ranged/Ag +1, /P3F/") },
            { 0xD6, new Tuple<string, string>("Hardwood Stick", "ATK 38, Ranged/En +2, /P3F/") },
            { 0xD7, new Tuple<string, string>("Long Stick", "ATK 78, Ranged/Light piercing damage, /P3F/") },
        };

        #endregion WeaponsP3F

        #region WeaponsP3M

        internal static Dictionary<int, Tuple<string, string>> WeaponsP3M = new Dictionary<int, Tuple<string, string>>()
        {
            { 0, new Tuple<string, string>("EMPTY", "") },

            { 0x206, new Tuple<string, string>("Shortsword", "ATK 133, /P3M/") },
            { 0x207, new Tuple<string, string>("Tizona", "ATK 155, Max HP +10%/En +3, /P3M/") },
            { 0x208, new Tuple<string, string>("Nagawakizashi", "ATK 162, Light splash damage, /P3M/") },
            { 0x209, new Tuple<string, string>("Shishioh", "ATK 174, All stats +1, /P3M/") },
            { 0x20A, new Tuple<string, string>("Arondight", "ATK 181, Ma +2/En +3, /P3M/") },
            { 0x20B, new Tuple<string, string>("Kogarasumaru", "ATK 190, Max HP & SP +10%, /P3M/") },
            { 0x20C, new Tuple<string, string>("Luna Edge", "ATK 198, Max SP +15%/Ma +2, /P3M/") },
            { 0x20D, new Tuple<string, string>("Amakuni", "ATK 219, Medium splash damage, /P3M/") },
            { 0x20E, new Tuple<string, string>("Excalibur", "ATK 236, Critical rate +20%/Ma +2, /P3M/") },
            { 0x20F, new Tuple<string, string>("Deus Xiphos", "ATK 260, Halves HP cost/St +4/Ma +5, /P3M/") },
            { 0x211, new Tuple<string, string>("Falcata", "ATK 145, Critical rate +5%, /P3M/") },
            { 0x212, new Tuple<string, string>("Unmarked Sword", "ATK 137, Max SP +5%, /P3M/") },
        };

        #endregion WeaponsP3M

        #region WeaponsP4Junpei

        internal static Dictionary<int, Tuple<string, string>> WeaponsP4Junpei = new Dictionary<int, Tuple<string, string>>()
        {
            { 0, new Tuple<string, string>("EMPTY", "") },

            { 0x123, new Tuple<string, string>("Imitation Katana", "ATK 82, /P4/") },
            { 0x124, new Tuple<string, string>("Long Sword", "ATK 91, Accuracy +10%, /P4//Junpei") },
            { 0x125, new Tuple<string, string>("Titanium Club", "ATK 108, Ag +2, /P4//Junpei") },
            { 0x126, new Tuple<string, string>("Mythic Sword", "ATK 124, Ma +1/En +1/Lu +1, /P4//Junpei") },
            { 0x127, new Tuple<string, string>("Great Sword", "ATK 136, Critical rate +5%/En +2, /P4//Junpei") },
            { 0x128, new Tuple<string, string>("Paralysis Blade", "ATK 141, Low chance of Paralysis, /P4//Junpei") },
            { 0x129, new Tuple<string, string>("Horsechopper", "ATK 152, Light splash damage, /P4//Junpei") },
            { 0x12A, new Tuple<string, string>("Bastard Sword", "ATK 162, Medium piercing damage, /P4//Junpei") },
            { 0x12B, new Tuple<string, string>("Tsubaki-maru", "ATK 169, Max HP & SP +5%, /P4//Junpei") },
            { 0x12C, new Tuple<string, string>("Venom Sword", "ATK 182, Medium chance of Poison, /P4//Junpei") },
            { 0x12D, new Tuple<string, string>("Caladbolg", "ATK 189, Max HP & SP +10%, /P4//Junpei") },
            { 0x12E, new Tuple<string, string>("Tobi-botaru", "ATK 197, Critical rate +15%, /P4//Junpei") },
            { 0x12F, new Tuple<string, string>("Galatine", "ATK 206, HP +10%/St +3, /P4//Junpei") },
            { 0x130, new Tuple<string, string>("Labyrinth Sword", "ATK 228, Max SP +10%/Lu +4, /P4//Junpei") },
            { 0x131, new Tuple<string, string>("Mithril Club", "ATK 244, Critical rate +20%/En +2, /P4//Junpei") },
            { 0x132, new Tuple<string, string>("Usumidori", "ATK 275, Halves HP cost/St +4/En +5, /P4/") },
            { 0x133, new Tuple<string, string>("Masakado Sword", "ATK 267, Halves HP cost/St +3/En +5, Junpei") },
            { 0x134, new Tuple<string, string>("Kishido Blade", "ATK 116, Junpei") },
            { 0x136, new Tuple<string, string>("Bone Sword", "ATK 100, En +3, /P4//Junpei") },
        };

        #endregion WeaponsP4Junpei

        #region WeaponsP5

        internal static Dictionary<int, Tuple<string, string>> WeaponsP5 = new Dictionary<int, Tuple<string, string>>()
        {
            { 0, new Tuple<string, string>("EMPTY", "") },

            { 0x1, new Tuple<string, string>("Rebel Knife", "ATK 28, /P5/") },
            { 0x2, new Tuple<string, string>("Light Knife", "ATK 42, Ag +2, /P5/") },
            { 0x3, new Tuple<string, string>("Sleet Dagger", "ATK 50, Accuracy +10%, /P5/") },
            { 0x4, new Tuple<string, string>("Crimson Knife", "ATK 60, Low chance of Paralysis, /P5/") },
            { 0x5, new Tuple<string, string>("Kukri", "ATK 74, /P5/") },
            { 0x6, new Tuple<string, string>("Igniter", "ATK 82, Low chance of Poison, /P5/") },
            { 0x7, new Tuple<string, string>("Silver Dagger", "ATK 98, Ma +1, /P5/") },
            { 0x8, new Tuple<string, string>("Obsidian Knife", "ATK 112, Light piercing damage, /P5/") },
            { 0x9, new Tuple<string, string>("Skinning Knife", "ATK 124, Lu +2, /P5/") },
            { 0xA, new Tuple<string, string>("Quality Kopis", "ATK 129, Max SP +10%, /P5/") },
            { 0xB, new Tuple<string, string>("Killing Scalpel", "ATK 139, Ag +3, /P5/") },
            { 0xC, new Tuple<string, string>("Superconductor", "ATK 150, Medium chance of Paralysis, /P5/") },
            { 0xD, new Tuple<string, string>("Dirk", "ATK 156, Critical rate +5%, /P5/") },
            { 0xE, new Tuple<string, string>("Headhunter", "ATK 169, Low chance of Instant Kill, /P5/") },
            { 0xF, new Tuple<string, string>("Parrying Dagger", "ATK 176, En +4, /P5/") },
            { 0x10, new Tuple<string, string>("Azoth Dagger", "ATK 184, Max SP +10%/Ma +3, /P5/") },
            { 0x11, new Tuple<string, string>("Arsene's Cane", "ATK 193, All stats +2, /P5/") },
            { 0x12, new Tuple<string, string>("Nihil", "ATK 214, Critical rate +20%/Ma +2, /P5/") },
            { 0x13, new Tuple<string, string>("Misericorde", "ATK 230, Low chance of Instant Kill/Ag +5, /P5/") },
            { 0x14, new Tuple<string, string>("Paradise Lost", "ATK 255, Halves HP cost/Ma +4/Ag +5, /P5/") },
            { 0x16, new Tuple<string, string>("Jailbreaker", "ATK 31, En +1, /P5/") },
            { 0x17, new Tuple<string, string>("Survivor's Knife", "ATK 35, Lu +2, /P5/") },
        };

        #endregion WeaponsP5

        #region WeaponsRyuji

        internal static Dictionary<int, Tuple<string, string>> WeaponsRyuji = new Dictionary<int, Tuple<string, string>>()
        {
            { 0, new Tuple<string, string>("EMPTY", "") },

            { 0x21, new Tuple<string, string>("Metal Pipe", "ATK 34, Ryuji") },
            { 0x22, new Tuple<string, string>("Spiked Bat", "ATK 50, Accuracy +5%, Ryuji") },
            { 0x23, new Tuple<string, string>("Lightweight Rod", "ATK 59, Ag +3, Ryuji") },
            { 0x24, new Tuple<string, string>("Hell Slugger", "ATK 71, Critical rate +5%, Ryuji") },
            { 0x25, new Tuple<string, string>("Heavy Steel Pipe", "ATK 87, Ryuji") },
            { 0x27, new Tuple<string, string>("Heavy Mace", "ATK 114, Ryuji") },
            { 0x28, new Tuple<string, string>("Femur Club", "ATK 130, Max HP +5%/En +2, Ryuji") },
            { 0x29, new Tuple<string, string>("Demon Pipe", "ATK 143, En +3, Ryuji") },
            { 0x2A, new Tuple<string, string>("Sleep Stick", "ATK 148, Medium chance of Sleep, Ryuji") },
            { 0x2B, new Tuple<string, string>("Spike Rod", "ATK 159, Critical rate +10%, Ryuji") },
            { 0x2C, new Tuple<string, string>("Iron Pipe", "ATK 170, Max SP +10%/Ma +3, Ryuji") },
            { 0x2D, new Tuple<string, string>("Wolf Fang Pole", "ATK 176, Ag +4, Ryuji") },
            { 0x2E, new Tuple<string, string>("Dagda's Club", "ATK 189, Critical rate +15%, Ryuji") },
            { 0x2F, new Tuple<string, string>("Gaea Rod", "ATK 196, Max SP +10%/Ma +1, Ryuji") },
            { 0x30, new Tuple<string, string>("Hercules' Club", "ATK 205, Critical rate +5%/Ag +3, Ryuji") },
            { 0x31, new Tuple<string, string>("Dragon God Pole", "ATK 213, Max HP +15%/St +3, Ryuji") },
            { 0x32, new Tuple<string, string>("Demon Staff", "ATK 236, High chance of Hex, Ryuji") },
            { 0x33, new Tuple<string, string>("Megaton Rod", "ATK 251, Critical rate +20%/En +2, Ryuji") },
            { 0x34, new Tuple<string, string>("Ruyi Jingu Bang", "ATK 277, Halves HP cost/St +5/En +4, Ryuji") },
            { 0x36, new Tuple<string, string>("Giant Penlight", "ATK 42, Critical rate +5%, Ryuji") },
            { 0x37, new Tuple<string, string>("Sequoia Mace", "ATK 95, Max SP +5%/Ma +2, Ryuji") },
        };

        #endregion WeaponsRyuji

        #region WeaponsShinjiro

        internal static Dictionary<int, Tuple<string, string>> WeaponsShinjiro = new Dictionary<int, Tuple<string, string>>()
        {
            { 0, new Tuple<string, string>("EMPTY", "") },

            { 0x285, new Tuple<string, string>("Sledgehammer", "ATK 131, Shinjiro") },
            { 0x286, new Tuple<string, string>("Nirili", "ATK 150, Critical rate +10%, Shinjiro") },
            { 0x287, new Tuple<string, string>("Megaton Hammer", "ATK 160, Max HP +10%, Shinjiro") },
            { 0x288, new Tuple<string, string>("Night Stalker", "ATK 171, Critical rate +10%, Shinjiro") },
            { 0x289, new Tuple<string, string>("Rocket Hammer", "ATK 178, Ag +4, Shinjiro") },
            { 0x28A, new Tuple<string, string>("The Pulverizer", "ATK 191, Medium chance of Confusion, Shinjiro") },
            { 0x28B, new Tuple<string, string>("Bus Stop Sign", "ATK 198, Medium chance of any Bind, Shinjiro") },
            { 0x28C, new Tuple<string, string>("Ukonvasara", "ATK 207, Max HP +10%/St +3, Shinjiro") },
            { 0x28D, new Tuple<string, string>("Eliminator", "ATK 215, Medium chance of Hex, Shinjiro") },
            { 0x28E, new Tuple<string, string>("Mjolnir", "ATK 237, Critical rate +15%, Shinjiro") },
            { 0x28F, new Tuple<string, string>("Warlord Hammer", "ATK 254, Critical rate +20%/St +2, Shinjiro") },
            { 0x290, new Tuple<string, string>("Caduceus", "ATK 278, Halves HP cost/St +5/En +4, Shinjiro") },
            { 0x292, new Tuple<string, string>("Ogre Hammer", "ATK 145, Critical rate +5%, Shinjiro") },
        };

        #endregion WeaponsShinjiro

        #region WeaponsTeddie

        internal static Dictionary<int, Tuple<string, string>> WeaponsTeddie = new Dictionary<int, Tuple<string, string>>()
        {
            { 0, new Tuple<string, string>("EMPTY", "") },

            { 0x183, new Tuple<string, string>("Spiky Claw", "ATK 79, Teddie") },
            { 0x184, new Tuple<string, string>("Kittycat Claw", "ATK 88, Lu +2, Teddie") },
            { 0x185, new Tuple<string, string>("Bear Claw", "ATK 104, Critical rate +5%, Teddie") },
            { 0x186, new Tuple<string, string>("Tyranno Claw", "ATK 119, Max HP +10%, Teddie") },
            { 0x187, new Tuple<string, string>("Kazegiri", "ATK 132, Light splash damage, Teddie") },
            { 0x188, new Tuple<string, string>("Breaker", "ATK 136, Critical rate +10%, Teddie") },
            { 0x189, new Tuple<string, string>("Thunder Claw", "ATK 146, Low chance of Paralysis, Teddie") },
            { 0x18A, new Tuple<string, string>("Hyper Drill", "ATK 158, Medium piercing damage, Teddie") },
            { 0x18B, new Tuple<string, string>("Sonic Claw", "ATK 164, Ag +4, Teddie") },
            { 0x18C, new Tuple<string, string>("Poison Claw", "ATK 177, Medium chance of Poison, Teddie") },
            { 0x18D, new Tuple<string, string>("Fuuma Bundou", "ATK 183, Ranged/Low chance of Instant Kill, Teddie") },
            { 0x18E, new Tuple<string, string>("Dangerous Claw", "ATK 192, Medium chance of Hex, Teddie") },
            { 0x18F, new Tuple<string, string>("Platinum Claw", "ATK 201, Max HP +10%/En +3, Teddie") },
            { 0x190, new Tuple<string, string>("The Ripper", "ATK 223, Medium splash damage, Teddie") },
            { 0x191, new Tuple<string, string>("7-Star Claw", "ATK 238, High chance of Confusion, Teddie") },
            { 0x192, new Tuple<string, string>("Claw of Revenge", "ATK 263, Halves HP cost/En +6/Lu +3, Teddie") },
            { 0x194, new Tuple<string, string>("Drunkard Claw", "ATK 94, Low chance of Confusion, Teddie") },
        };

        #endregion WeaponsTeddie

        #region WeaponsYosukeKoromaru

        internal static Dictionary<int, Tuple<string, string>> WeaponsYosukeKoromaru = new Dictionary<int, Tuple<string, string>>()
        {
            { 0, new Tuple<string, string>("EMPTY", "") },

            { 0x1E4, new Tuple<string, string>("Monkey Wrench", "ATK 86, Yosuke") },
            { 0x1E5, new Tuple<string, string>("Poison Kunai", "ATK 95, Medium chance of Poison, Yosuke/Koromaru") },
            { 0x1E6, new Tuple<string, string>("Machete", "ATK 116, Ag +2, Yosuke/Koromaru") },
            { 0x1E7, new Tuple<string, string>("Blitz Kunai", "ATK 121, Medium piercing damage, Yosuke/Koromaru") },
            { 0x1E8, new Tuple<string, string>("Karasu-maru", "ATK 132, Low chance of Instant Kill, Yosuke/Koromaru") },
            { 0x1E9, new Tuple<string, string>("En-Giri", "ATK 141, Critical rate +15%, Yosuke/Koromaru") },
            { 0x1EA, new Tuple<string, string>("Raven Claw", "ATK 148, Ma +2/Ag +1, Yosuke/Koromaru") },
            { 0x1EB, new Tuple<string, string>("Fearful Kunai", "ATK 161, Medium chance of Hex, Yosuke/Koromaru") },
            { 0x1EC, new Tuple<string, string>("Basho", "ATK 168, Low chance of Strength Bind, Yosuke/Koromaru") },
            { 0x1ED, new Tuple<string, string>("Kitchen Knife", "ATK 177, Critical rate +10%/Lu +3, Yosuke/Koromaru") },
            { 0x1EE, new Tuple<string, string>("Shadowrend", "ATK 184, Ag +5, Yosuke/Koromaru") },
            { 0x1EF, new Tuple<string, string>("Rappa", "ATK 206, Medium chance of Agility Bind, Yosuke/Koromaru") },
            { 0x1F0, new Tuple<string, string>("Kashin Koji", "ATK 222, Critical rate +20%/Ag +2, Yosuke/Koromaru") },
            { 0x1F1, new Tuple<string, string>("Nandaka", "ATK 250, Halves HP cost/Ag +7/Lu +2, Yosuke") },
            { 0x1F2, new Tuple<string, string>("Vajra", "ATK 241, Halves HP cost/Ma +4/Ag +4, Koromaru") },
            { 0x1F3, new Tuple<string, string>("Sylph Blade", "ATK 98, Ag +3, Koromaru") },
            { 0x1F5, new Tuple<string, string>("Kunai", "ATK 92, Ag +2, Yosuke/Koromaru") },
            { 0x1F6, new Tuple<string, string>("Skill Spanner", "ATK 105, St +2/Ma +1, Yosuke/Koromaru") },
        };

        #endregion WeaponsYosukeKoromaru

        #region WeaponsYukari

        internal static Dictionary<int, Tuple<string, string>> WeaponsYukari = new Dictionary<int, Tuple<string, string>>()
        {
            { 0, new Tuple<string, string>("EMPTY", "") },

            { 0x225, new Tuple<string, string>("Short Bow", "ATK 95, Ranged, Yukari") },
            { 0x226, new Tuple<string, string>("Spiral Bow", "ATK 111, Ranged/Light piercing damage, Yukari") },
            { 0x227, new Tuple<string, string>("Shigetou-yumi", "ATK 120, Ranged/Max HP +10%, Yukari") },
            { 0x228, new Tuple<string, string>("Silence Bow", "ATK 131, Ranged/Medium chance of Magic Bind, Yukari") },
            { 0x229, new Tuple<string, string>("Higo-yumi", "ATK 137, Ranged/Max HP & SP +5%, Yukari") },
            { 0x22A, new Tuple<string, string>("Fuuma Longbow", "ATK 149, Ranged/Low chance of Paralysis, Yukari") },
            { 0x22B, new Tuple<string, string>("Artemis Bow", "ATK 156, Ranged/Max SP +10%, Yukari") },
            { 0x22C, new Tuple<string, string>("Hydravenom Bow", "ATK 165, Ranged/Medium chance of Poison, Yukari") },
            { 0x22D, new Tuple<string, string>("Pleiades", "ATK 173, Ranged/Ma +3, Yukari") },
            { 0x22E, new Tuple<string, string>("Hades Bow", "ATK 195, Ranged/Low chance of Instant Kill, Yukari") },
            { 0x22F, new Tuple<string, string>("Tomoe's Bow", "ATK 210, Ranged/High chance of Strength Bind, Yukari") },
            { 0x230, new Tuple<string, string>("Origin Bow", "ATK 233, Ranged/Halves HP cost/Ma +7, Yukari") },
            { 0x232, new Tuple<string, string>("Quintet Bow", "ATK 106, Ranged/Ag +2, Yukari") },
        };

        #endregion WeaponsYukari

        #region WeaponsYukiko

        internal static Dictionary<int, Tuple<string, string>> WeaponsYukiko = new Dictionary<int, Tuple<string, string>>()
        {
            { 0, new Tuple<string, string>("EMPTY", "") },

            { 0x163, new Tuple<string, string>("Noh Fan", "ATK 57, Ranged, Yukiko") },
            { 0x164, new Tuple<string, string>("Kyo Sensu", "ATK 64, Ranged/Accuracy +5%, Yukiko") },
            { 0x165, new Tuple<string, string>("Hanachirusato", "ATK 77, Ranged/En +2, Yukiko") },
            { 0x166, new Tuple<string, string>("Downpour Fan", "ATK 89, Ranged/Ma +2, Yukiko") },
            { 0x167, new Tuple<string, string>("Fortified Tessen", "ATK 99, Ranged/Max HP +10%, Yukiko") },
            { 0x168, new Tuple<string, string>("Inversion Fan", "ATK 104, Ranged/Ma +2/En +1, Yukiko") },
            { 0x169, new Tuple<string, string>("Suzumushi", "ATK 114, Ranged/Medium chance of Sleep, Yukiko") },
            { 0x16A, new Tuple<string, string>("Masquerade", "ATK 123, Ranged/Ma +4, Yukiko") },
            { 0x16B, new Tuple<string, string>("Suzaku Fan", "ATK 130, Ranged/Light piercing damage, Yukiko") },
            { 0x16C, new Tuple<string, string>("Juli-sen", "ATK 142, Ranged/Money obtained +10%, Yukiko") },
            { 0x16D, new Tuple<string, string>("Mogari-Bue", "ATK 149, Ranged/Ag +3, Yukiko") },
            { 0x16E, new Tuple<string, string>("Fickle Madam", "ATK 158, Ranged/Low chance of Paralysis, Yukiko") },
            { 0x16F, new Tuple<string, string>("Treasure Fan", "ATK 167, Ranged/Ma +2/Ag +2, Yukiko") },
            { 0x170, new Tuple<string, string>("Pieta", "ATK 187, Ranged/Medium chance of Magic Bind, Yukiko") },
            { 0x171, new Tuple<string, string>("Yume-no-Hashi", "ATK 203, Ranged/Max SP +20%, Yukiko") },
            { 0x172, new Tuple<string, string>("Primordial Sea", "ATK 226, Ranged/Halves HP cost/Ma +7, Yukiko") },
            { 0x174, new Tuple<string, string>("Great Bird Fan", "ATK 72, Ranged/Light piercing damage, Yukiko") },
        };

        #endregion WeaponsYukiko

        #region WeaponsYusuke

        internal static Dictionary<int, Tuple<string, string>> WeaponsYusuke = new Dictionary<int, Tuple<string, string>>()
        {
            { 0, new Tuple<string, string>("EMPTY", "") },

            { 0x61, new Tuple<string, string>("Imitation Katana", "ATK 32, Yusuke") },
            { 0x62, new Tuple<string, string>("Unmarked Katana", "ATK 49, Accuracy +5%, Yusuke") },
            { 0x63, new Tuple<string, string>("Zambato", "ATK 58, Light splash damage, Yusuke") },
            { 0x64, new Tuple<string, string>("Meteorite Katana", "ATK 69, Max SP +10%, Yusuke") },
            { 0x65, new Tuple<string, string>("Kikuichimonji", "ATK 84, Yusuke") },
            { 0x67, new Tuple<string, string>("Type-98 Gunto", "ATK 111, Accuracy +10%, Yusuke") },
            { 0x68, new Tuple<string, string>("Jagato", "ATK 126, Low chance of Poison, Yusuke") },
            { 0x69, new Tuple<string, string>("Bizen Osafune", "ATK 139, Light piercing damage, Yusuke") },
            { 0x6A, new Tuple<string, string>("Imperial Sword", "ATK 144, En +2/Ag +2, Yusuke") },
            { 0x6B, new Tuple<string, string>("Shikomi Kiseru", "ATK 155, Critical rate +10%, Yusuke") },
            { 0x6C, new Tuple<string, string>("Toragozen", "ATK 166, Max SP +15%/Ma +2, Yusuke") },
            { 0x6D, new Tuple<string, string>("Nagamitsu", "ATK 172, Max HP +10%, Yusuke") },
            { 0x6E, new Tuple<string, string>("Monohoshizao", "ATK 185, Medium splash damage, Yusuke") },
            { 0x6F, new Tuple<string, string>("Kijintou", "ATK 192, St +5, Yusuke") },
            { 0x70, new Tuple<string, string>("Iai Katana", "ATK 201, Critical rate +15%, Yusuke") },
            { 0x71, new Tuple<string, string>("Fudo Masamune", "ATK 209, Max HP +10%/En +3, Yusuke") },
            { 0x72, new Tuple<string, string>("Samonji", "ATK 232, Medium splash damage, Yusuke") },
            { 0x73, new Tuple<string, string>("Red Demon Blade", "ATK 247, High chance of Hex, Yusuke") },
            { 0x74, new Tuple<string, string>("777 Katana", "ATK 272, Halves HP cost/St +2/Ag +7, Yusuke") },
            { 0x76, new Tuple<string, string>("Sakabatou", "ATK 40, Critical rate +5%, Yusuke") },
            { 0x77, new Tuple<string, string>("Ribcage Katana", "ATK 98, En +2, Yusuke") },
        };

        #endregion WeaponsYusuke

        #endregion Weapons
    }
}
