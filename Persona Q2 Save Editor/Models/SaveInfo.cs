﻿using System.Collections.ObjectModel;
using System.ComponentModel;

namespace Persona_Q2_Save_Editor.Models
{
    internal class SaveInfo : INotifyPropertyChanged
    {
        private Character character;
        private Character[] characters;
        private Character[] sortedCharacters;
        private byte[] enepedia, saveData;
        private Item[] inventory;
        private short teamGauge;
        private string saveFile;
        private bool saveLoaded;
        private Persona[] summonedPersonas;
        private int yen;

        public Character Character
        {
            get { return character; }
            set
            {
                if (character == value) return;
                character = value;
                NotifyPropertyChanged(nameof(Character));
            }
        }

        public Character[] Characters
        {
            get { return characters; }
            set
            {
                if (characters == value) return;
                characters = value;
                NotifyPropertyChanged(nameof(Characters));
            }
        }

        public Character[] SortedCharacters
        {
            get { return sortedCharacters; }
            set
            {
                if (sortedCharacters == value) return;
                sortedCharacters = value;
                NotifyPropertyChanged(nameof(sortedCharacters));
            }
        }

        public byte[] Enepedia
        {
            get { return enepedia; }
            set { enepedia = value; }
        }

        public Item[] Inventory
        {
            get { return inventory; }
            set
            {
                if (inventory == value) return;
                inventory = value;
                NotifyPropertyChanged(nameof(Inventory));
            }
        }

        public short TeamGauge
        {
            get { return teamGauge; }
            set
            {
                if (teamGauge == value) return;
                teamGauge = value;
                NotifyPropertyChanged(nameof(TeamGauge));
            }
        }

        public ObservableCollection<Persona> PersonaList { get; set; }

        public byte[] SaveData
        {
            get { return saveData; }
            set { saveData = value; }
        }

        public string SaveFile
        {
            get { return saveFile; }
            set
            {
                if (saveFile == value) return;
                saveFile = value;
                NotifyPropertyChanged(nameof(SaveFile));
            }
        }

        public bool SaveLoaded
        {
            get { return saveLoaded; }
            set
            {
                if (saveLoaded == value) return;
                saveLoaded = value;
                NotifyPropertyChanged(nameof(SaveLoaded));
            }
        }

        public Persona[] SummonedPersonas
        {
            get { return summonedPersonas; }
            set
            {
                if (summonedPersonas == value) return;
                summonedPersonas = value;
                NotifyPropertyChanged(nameof(SummonedPersonas));
            }
        }

        public int Yen
        {
            get { return yen; }
            set
            {
                if (yen == value) return;
                yen = value;
                NotifyPropertyChanged(nameof(Yen));
            }
        }

        #region INotifyPropertyChanged implementation

        public event PropertyChangedEventHandler PropertyChanged;

        protected void NotifyPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion INotifyPropertyChanged implementation
    }
}
