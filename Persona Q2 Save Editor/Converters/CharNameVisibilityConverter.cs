﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace Persona_Q2_Save_Editor.Converters
{
    internal class CharNameVisibilityConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is short)
            {
                switch ((short)value)
                {
                    case 0:
                    case 1:
                    case 18:
                    case 19:
                        return Visibility.Visible;
                    default:
                        return Visibility.Hidden;
                }
            }

            return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
