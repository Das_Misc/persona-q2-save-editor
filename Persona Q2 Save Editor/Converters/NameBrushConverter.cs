﻿using MahApps.Metro;
using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;
using System.Windows.Media;

namespace Persona_Q2_Save_Editor.Converters
{
    internal class NameBrushConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is int)
                switch ((int)value)
                {
                    case 0x4E2: // Omnipotent Orb Accessory

                    case 0x3B0: // Morgana Armour
                    case 0x3C6: // Koromaru Armour
                    case 0x3D9: // Aigis Armour
                    case 0x350: // P5 Armour
                    case 0x351: // Ryuji Armour
                    case 0x352: // Yusuke Armour
                    case 0x353: // Akechi Armour
                    case 0x354: // P4 Armour
                    case 0x355: // Yosuke Armour
                    case 0x356: // Kanji Armour
                    case 0x357: // Teddie Armour
                    case 0x358: // P3M Armour
                    case 0x359: // Junpei Armour
                    case 0x35A: // Akihiko Armour
                    case 0x35B: // Ken Armour
                    case 0x35C: // Shinjiro Armour
                    case 0x380: // Ann Armour
                    case 0x381: // P3F Armour
                    case 0x382: // Haru Armour
                    case 0x383: // Makoto Armour
                    case 0x384: // Chie Armour
                    case 0x385: // Yukiko Armour
                    case 0x386: // Naoto Armour
                    case 0x387: // Yukari Armour
                    case 0x388: // Mitsuru Armour

                    case 0x517: // Amrita Soda Consumable
                    case 0x50D: // Balm of Life Consumable
                    case 0x539: // Soma Consumable
                    case 0x512: // Purifying Water Consumable
                    case 0x513: // Purifying Salt Consumable
                    case 0x53B: // Goho-M More Consumable
                    case 0x53C: // Goba-K More Consumable
                    case 0x53E: // Repulse Horn Consumable
                    case 0x53F: // Treasure Renew Consumable

                    case 0x2AF: // Aigis Weapon
                    case 0x94: // Akechi Weapon
                    case 0x114: // Akihiko Weapon
                    case 0x54: // Ann Weapon
                    case 0x152: // Chie Weapon
                    case 0xF3: // Haru Weapon
                    case 0x133: // Junpei Weapon
                    case 0x1B2: // Kanji Weapon
                    case 0x270: // Ken Weapon
                    case 0x1F2: // Koromaru Weapon
                    case 0x113: // Makoto Weapon
                    case 0x250: // Mitsuru Weapon
                    case 0xB3: // Morgana Weapon
                    case 0x1D2: // Naoto Weapon
                    case 0xD3: // P3F Weapon
                    case 0x20F: // P3M Weapon
                    case 0x132: // P4 Weapon
                    case 0x14: // P5 Weapon
                    case 0x34: // Ryuji Weapon
                    case 0x290: // Shinjiro Weapon
                    case 0x192: // Teddie Weapon
                    case 0x1F1: // Yosuke Weapon
                    case 0x230: // Yukari Weapon
                    case 0x172: // Yukiko Weapon
                    case 0x74: // Yusuke Weapon
                        return new SolidColorBrush((Color)ThemeManager.GetResourceFromAppStyle(Application.Current.MainWindow, "AccentColor"));

                    case 0x314: // Unisex Armour
                    case 0x377: // Female Armour
                    case 0x345: // Male Armour
                    case 0x542: // Vanish Ball Consumable
                        return new SolidColorBrush((Color)ThemeManager.GetAccent("Pink").Resources["AccentColor"]);
                        
                    default:
                        return Brushes.White;
                }

            return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
