﻿using MahApps.Metro.Controls;
using Persona_Q2_Save_Editor.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Controls;

namespace Persona_Q2_Save_Editor.Views
{
    /// <summary>
    /// Interaction logic for SubPersonaWindow.xaml
    /// </summary>
    public partial class SubPersonaWindow : MetroWindow
    {
        public SubPersonaWindow(MainWindow mw)
        {
            DataContext = mw.DataContext;
            (DataContext as SaveInfo).PersonaList = new ObservableCollection<Persona>();
            (DataContext as SaveInfo).PersonaList.Add((DataContext as SaveInfo).Character.SubPersona);

            InitializeComponent();
        }

        private void Arcana_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (!(e.OriginalSource is TabControl))
                return;

            KeyValuePair<int, Tuple<string, string>>[] personaData;

            switch ((sender as TabControl).SelectedIndex)
            {
                case 0:
                    personaData = (from x in Lists.PersonasChariot
                                   select x).ToArray();
                    break;

                case 1:
                    personaData = (from x in Lists.PersonasDeath
                                   select x).ToArray();
                    break;

                case 2:
                    personaData = (from x in Lists.PersonasDevil
                                   select x).ToArray();
                    break;

                case 3:
                    personaData = (from x in Lists.PersonasEmperor
                                   select x).ToArray();
                    break;

                case 4:
                    personaData = (from x in Lists.PersonasEmpress
                                   select x).ToArray();
                    break;

                case 5:
                    personaData = (from x in Lists.PersonasFool
                                   select x).ToArray();
                    break;

                case 6:
                    personaData = (from x in Lists.PersonasFortune
                                   select x).ToArray();
                    break;

                case 7:
                    personaData = (from x in Lists.PersonasHangedMan
                                   select x).ToArray();
                    break;

                case 8:
                    personaData = (from x in Lists.PersonasHermit
                                   select x).ToArray();
                    break;

                case 9:
                    personaData = (from x in Lists.PersonasHierophant
                                   select x).ToArray();
                    break;

                case 10:
                    personaData = (from x in Lists.PersonasJudgement
                                   select x).ToArray();
                    break;

                case 11:
                    personaData = (from x in Lists.PersonasJustice
                                   select x).ToArray();
                    break;

                case 12:
                    personaData = (from x in Lists.PersonasLovers
                                   select x).ToArray();
                    break;

                case 13:
                    personaData = (from x in Lists.PersonasMagician
                                   select x).ToArray();
                    break;

                case 14:
                    personaData = (from x in Lists.PersonasMoon
                                   select x).ToArray();
                    break;

                case 15:
                    personaData = (from x in Lists.PersonasPriestess
                                   select x).ToArray();
                    break;

                case 16:
                    personaData = (from x in Lists.PersonasStar
                                   select x).ToArray();
                    break;

                case 17:
                    personaData = (from x in Lists.PersonasStrength
                                   select x).ToArray();
                    break;

                case 18:
                    personaData = (from x in Lists.PersonasSun
                                   select x).ToArray();
                    break;

                case 19:
                    personaData = (from x in Lists.PersonasTemperance
                                   select x).ToArray();
                    break;

                case 20:
                    personaData = (from x in Lists.PersonasTower
                                   select x).ToArray();
                    break;

                case 21:
                    personaData = (from x in Lists.PersonasWorld
                                   select x).ToArray();
                    break;

                default:
                    return;
            }

            UpdatePersonaList(personaData);

            e.Handled = true;
        }

        private void Skills_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            KeyValuePair<int, Tuple<string, string>>[] skillsData = null;

            switch ((sender as TabControl).SelectedIndex)
            {
                case 0:
                    skillsData = (from x in Lists.SkillsAilment
                                  select x).ToArray();
                    break;

                case 1:
                    skillsData = (from x in Lists.SkillsAlmighty
                                  select x).ToArray();
                    break;

                case 2:
                    skillsData = (from x in Lists.SkillsAuto
                                  select x).ToArray();
                    break;

                case 3:
                    skillsData = (from x in Lists.SkillsBless
                                  select x).ToArray();
                    break;

                case 4:
                    skillsData = (from x in Lists.SkillsBuff
                                  select x).ToArray();
                    break;

                case 5:
                    skillsData = (from x in Lists.SkillsCircle
                                  select x).ToArray();
                    break;

                case 6:
                    skillsData = (from x in Lists.SkillsCurse
                                  select x).ToArray();
                    break;

                case 7:
                    skillsData = (from x in Lists.SkillsElectric
                                  select x).ToArray();
                    break;

                case 8:
                    skillsData = (from x in Lists.SkillsEnemy
                                  select x).ToArray();
                    break;

                case 9:
                    skillsData = (from x in Lists.SkillsFire
                                  select x).ToArray();
                    break;

                case 10:
                    skillsData = (from x in Lists.SkillsHealing
                                  select x).ToArray();
                    break;

                case 11:
                    skillsData = (from x in Lists.SkillsIce
                                  select x).ToArray();
                    break;

                case 12:
                    skillsData = (from x in Lists.SkillsLinks
                                  select x).ToArray();
                    break;

                case 13:
                    skillsData = (from x in Lists.SkillsNaviBattle
                                  select x).ToArray();
                    break;

                case 14:
                    skillsData = (from x in Lists.SkillsNaviDungeon
                                  select x).ToArray();
                    break;

                case 15:
                    skillsData = (from x in Lists.SkillsNuclear
                                  select x).ToArray();
                    break;

                case 16:
                    skillsData = (from x in Lists.SkillsPhys
                                  select x).ToArray();
                    break;

                case 17:
                    skillsData = (from x in Lists.SkillsPsychochinesis
                                  select x).ToArray();
                    break;

                case 18:
                    skillsData = (from x in Lists.SkillsUnison
                        select x).ToArray();
                    break;

                case 19:
                    skillsData = (from x in Lists.SkillsWind
                                  select x).ToArray();
                    break;
            }

            UpdateSkillList(skillsData);

            e.Handled = true;
        }

        private void UpdatePersonaList(KeyValuePair<int, Tuple<string, string>>[] personaData)
        {
            var personas = new Persona[personaData.Length - 1];

            for (int i = 1; i <= personas.Length; i++) // Drop the EMPTY entry
            {
                personas[i - 1] = new Persona();
                personas[i - 1].Id = (short)personaData[i].Key;
            }

            Persona.List = personas.OrderBy(x => x.Name).ToArray(); // Alphabetical order

            for (int i = (DataContext as SaveInfo).PersonaList.Count - 1; i > 0; i--)
                (DataContext as SaveInfo).PersonaList.RemoveAt(i);

            foreach (Persona item in Persona.List)
                (DataContext as SaveInfo).PersonaList.Add(item);
        }

        private void UpdateSkillList(KeyValuePair<int, Tuple<string, string>>[] skillsData)
        {
            var skills = new HashSet<Skill>(new SkillComparer());

            for (int i = 0; i < skillsData.Length; i++)
            {
                var skill = new Skill();
                skill.ID = (short)skillsData[i].Key;
                skills.Add(skill);
            }

            Skill.List = skills.OrderBy(x => x.ID != 0) // Set EMPTY as the first entry
                               .ThenBy(x => x.Name).ToArray(); // Then sort the others
        }

        private class SkillComparer : EqualityComparer<Skill>
        {
            public override bool Equals(Skill x, Skill y)
            {
                return x != null &&
                       y != null &&
                       x.Name == y.Name;
            }

            public override int GetHashCode(Skill obj)
            {
                var hashCode = -1885141022;
                hashCode = hashCode * 29 + EqualityComparer<string>.Default.GetHashCode(obj.Name);
                return hashCode;
            }
        }
    }
}
