﻿using MahApps.Metro.Controls;
using Persona_Q2_Save_Editor.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Controls;

namespace Persona_Q2_Save_Editor.Views
{
    /// <summary>
    /// Interaction logic for SubPersonaWindow.xaml
    /// </summary>
    public partial class PersonaWindow : MetroWindow
    {
        public PersonaWindow(MainWindow mw)
        {
            DataContext = mw.DataContext;
            (DataContext as SaveInfo).PersonaList = new ObservableCollection<Persona>();
            (DataContext as SaveInfo).PersonaList.Add((DataContext as SaveInfo).Character.Persona);

            InitializeComponent();

            PersonaName.Text = (DataContext as SaveInfo).Character.Persona.Name;
        }

        private void Skills_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            KeyValuePair<int, Tuple<string, string>>[] skillsData = null;

            switch ((sender as TabControl).SelectedIndex)
            {
                case 0:
                    skillsData = (from x in Lists.SkillsAilment
                                  select x).ToArray();
                    break;

                case 1:
                    skillsData = (from x in Lists.SkillsAlmighty
                                  select x).ToArray();
                    break;

                case 2:
                    skillsData = (from x in Lists.SkillsAuto
                                  select x).ToArray();
                    break;

                case 3:
                    skillsData = (from x in Lists.SkillsBless
                                  select x).ToArray();
                    break;

                case 4:
                    skillsData = (from x in Lists.SkillsBuff
                                  select x).ToArray();
                    break;

                case 5:
                    skillsData = (from x in Lists.SkillsCircle
                                  select x).ToArray();
                    break;

                case 6:
                    skillsData = (from x in Lists.SkillsCurse
                                  select x).ToArray();
                    break;

                case 7:
                    skillsData = (from x in Lists.SkillsElectric
                                  select x).ToArray();
                    break;

                case 8:
                    skillsData = (from x in Lists.SkillsEnemy
                                  select x).ToArray();
                    break;

                case 9:
                    skillsData = (from x in Lists.SkillsFire
                                  select x).ToArray();
                    break;

                case 10:
                    skillsData = (from x in Lists.SkillsHealing
                                  select x).ToArray();
                    break;

                case 11:
                    skillsData = (from x in Lists.SkillsIce
                                  select x).ToArray();
                    break;

                case 12:
                    skillsData = (from x in Lists.SkillsLinks
                                  select x).ToArray();
                    break;

                case 13:
                    skillsData = (from x in Lists.SkillsNaviBattle
                                  select x).ToArray();
                    break;

                case 14:
                    skillsData = (from x in Lists.SkillsNaviDungeon
                                  select x).ToArray();
                    break;

                case 15:
                    skillsData = (from x in Lists.SkillsNuclear
                                  select x).ToArray();
                    break;

                case 16:
                    skillsData = (from x in Lists.SkillsPhys
                        select x).ToArray();
                    break;

                case 17:
                    skillsData = (from x in Lists.SkillsPsychochinesis
                        select x).ToArray();
                    break;

                case 18:
                    skillsData = (from x in Lists.SkillsUnison
                        select x).ToArray();
                    break;

                case 19:
                    skillsData = (from x in Lists.SkillsWind
                        select x).ToArray();
                    break;
            }

            UpdateSkillList(skillsData);

            e.Handled = true;
        }
        
        private void UpdateSkillList(KeyValuePair<int, Tuple<string, string>>[] skillsData)
        {
            var skills = new HashSet<Skill>(new SkillComparer());

            for (int i = 0; i < skillsData.Length; i++)
            {
                var skill = new Skill();
                skill.ID = (short)skillsData[i].Key;
                skills.Add(skill);
            }

            Skill.List = skills.OrderBy(x => x.ID != 0) // Set EMPTY as the first entry
                .ThenBy(x => x.Name).ToArray(); // Then sort the others
        }

        private class SkillComparer : EqualityComparer<Skill>
        {
            public override bool Equals(Skill x, Skill y)
            {
                return x != null &&
                       y != null &&
                       x.Name == y.Name;
            }

            public override int GetHashCode(Skill obj)
            {
                var hashCode = -1885141022;
                hashCode = hashCode * 29 + EqualityComparer<string>.Default.GetHashCode(obj.Name);
                return hashCode;
            }
        }
    }
}
