﻿using MahApps.Metro.Controls.Dialogs;
using Microsoft.Win32;
using Persona_Q2_Save_Editor.Models;
using Persona_Q2_Save_Editor.Views;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace Persona_Q2_Save_Editor
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow
    {
        internal SaveInfo si;

        public MainWindow()
        {
            si = new SaveInfo();

            DataContext = si;

            InitializeComponent();

            DebugLoad();
            //QuickFix();
        }

        private void About_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            var thisAssembly = Assembly.GetExecutingAssembly();

            string message = string.Format(Properties.Resources.AboutMessage, Properties.Resources.Title, thisAssembly.GetName().Version,
                FileVersionInfo.GetVersionInfo(Assembly.GetEntryAssembly().Location).CompanyName);

            DialogManager.ShowModalMessageExternal(this, Properties.Resources.Title, message);
        }

        private void BestEquipment_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            int[] weapIds =
            {
                0x132, 0x20F, 0x1F1, 0x152, 0x172, 0x2C2, 0x1B2, 0x1D2, 0x192, 0x230, 0x133, 0x114, 0x250, 0x2C3, 
                0x2AF, 0x270, 0x1F2, 0x290, 0xD3, 0x14, 0x34, 0x54, 0xB3, 0x74, 0x113, 0x2C1, 0xF3, 0x94
            };
            int[] armourIds =
            {
                0x354, 0x358, 0x355, 0x384, 0x385, 0x3F7, 0x356, 0x386, 0x357, 0x387, 0x359, 0x35A, 0x388, 0x3F8,
                0x3D9, 0x35B, 0x3C6, 0x35C, 0x381, 0x350, 0x351, 0x380, 0x3B0, 0x352, 0x383, 0x3F6, 0x382, 0x353
            };
            var accessory = new Item(0x4E2, Lists.GetItemData(0x4E2).Item1, Lists.GetItemData(0x4E2).Item2);

            for (int i = 0; i < si.Characters.Length; i++)
            {
                var data = Lists.GetItemData(weapIds[i]);
                si.Characters[i].EquippedWeapon = new Item(weapIds[i], data.Item1, data.Item2);

                data = Lists.GetItemData(armourIds[i]);
                si.Characters[i].EquippedArmour = new Item(armourIds[i], data.Item1, data.Item2);

                si.Characters[i].EquippedAccessory = accessory;
            }

            DialogManager.ShowModalMessageExternal(this, Properties.Resources.Title, Properties.Resources.BestEquipmentMessage);
        }

        private void ChangeName_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            var mds = new MetroDialogSettings();
            mds.DefaultText = si.Character.BaseName;
            var newName = DialogManager.ShowModalInputExternal(this, Properties.Resources.ChangeName, Properties.Resources.ChangeNameDescription, mds);

            var mds2 = new MetroDialogSettings();
            mds2.DefaultText = si.Character.LastName;
            var newLastName =
                DialogManager.ShowModalInputExternal(this, Properties.Resources.ChangeLastName, Properties.Resources.ChangeLastNameDescription, mds2);

            if (string.IsNullOrWhiteSpace(newName) || string.IsNullOrWhiteSpace(newLastName))
                return;

            if (Regex.IsMatch(newName, @"^.{1,7}$") && Regex.IsMatch(newLastName, @"^.{1,7}$"))
            {
                Lists.Characters[si.Character.Id] = newName + " " + newLastName;
                si.Character.NotifyPropertyChanged("Name");
                si.Character.BaseName = newName;
                si.Character.LastName = newLastName;
            }
            else
                this.ShowMessageAsync("Invalid name", "The character name " + newName + " " + newLastName + " is invalid and will not be used.");
        }

        private void CheckUpdates_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = File.Exists("Updater.exe");
        }

        private void CheckUpdates_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            var thisAssembly = Assembly.GetExecutingAssembly().GetName();
            Process.Start("Updater.exe", $"\"{thisAssembly.Name}\" \"{thisAssembly.Version}\" \"{Assembly.GetEntryAssembly()?.Location}\"");
        }

        [Conditional("DEBUG")]
        private void DebugLoad()
        {
            string appData = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);
            string saveData =
                @"Citra\sdmc\Nintendo 3DS\00000000000000000000000000000000\00000000000000000000000000000000\title\00040000\001d7100\data\00000001\001GAME";
            string saveFile = Path.Combine(appData, saveData);

            si.SaveFile = saveFile;
            si.SaveData = File.ReadAllBytes(saveFile);
            si.SummonedPersonas = LoadSummonedPersonasData();
            si.Characters = LoadCharacterData();
            si.Inventory = LoadInventoryData();
            LoadGeneralData();
            si.SaveLoaded = true;
        }

        
        private void EditPersona_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            var spw = new PersonaWindow(this);
            spw.Owner = this;
            spw.ShowDialog();
        }

        private void EditSubPersona_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            var spw = new SubPersonaWindow(this);
            spw.Owner = this;
            spw.ShowDialog();
        }

        private void FillEnepedia_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            si.Enepedia = Lists.Enepedia;

            DialogManager.ShowModalMessageExternal(this, Properties.Resources.Title, Properties.Resources.FillEnepediaMessage);
        }

        private void FixChecksum(byte[] contents, int position, long finalPosition)
        {
            uint checksum = 0;

            for (int i = position + sizeof(uint); i < finalPosition; i++)
                checksum += contents[i];

            byte[] cs = BitConverter.GetBytes(checksum);
            Buffer.BlockCopy(cs, 0, contents, position, cs.Length);
        }

        private void GenerateMap_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            using (var s = Assembly.GetExecutingAssembly().GetManifestResourceStream("Persona_Q2_Save_Editor.Resources.001MAP"))
            using (var fs = File.Open("001MAP", FileMode.Create, FileAccess.Write, FileShare.None))
            {
                int readBytes = 0;
                byte[] buffer = new byte[4096];

                while ((readBytes = s.Read(buffer, 0, buffer.Length)) > 0)
                    fs.Write(buffer, 0, readBytes);
            }

            DialogManager.ShowModalMessageExternal(this, Properties.Resources.Title, Properties.Resources.GenerateMapMessage);
        }

        private void Load_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            var ofd = new OpenFileDialog();
            ofd.FileName = "*GAME";
            ofd.Title = Properties.Resources.Title;

            if (ofd.ShowDialog().Value)
            {
                si.SaveFile = ofd.FileName;
                si.SaveData = File.ReadAllBytes(ofd.FileName);
                si.SummonedPersonas = LoadSummonedPersonasData();
                si.Characters = LoadCharacterData();
                si.Inventory = LoadInventoryData();
                LoadGeneralData();
                si.SaveLoaded = true;
            }
        }

        private Persona[] LoadSummonedPersonasData()
        {
            using (var ms = new MemoryStream(si.SaveData))
            using (var br = new BinaryReader(ms))
            {
                ms.Position = 0x79D2;

                var personas = new Persona[0x18];

                for (int i = 0; i < personas.Length; i++)
                {
                    personas[i] = new Persona();
                    personas[i].Active = br.ReadInt16();
                    byte[] data = br.ReadBytes(2);
                    byte lowByte = (byte)(data[1] & 0x0F);
                    personas[i].Id = BitConverter.ToInt16(new[] { data[0], (byte)(lowByte % 2) }, 0);
                    personas[i].Level = (short)(lowByte % 2 == 0 ? data[1] / 2 : (data[1] - 1) / 2);
                    personas[i].Exp = br.ReadInt32();
                    personas[i].Skills = new Skill[6];
                    for (int j = 0; j < personas[i].Skills.Length; j++)
                    {
                        personas[i].Skills[j] = new Skill();
                        personas[i].Skills[j].ID = br.ReadInt16();
                    }

                    ms.Position += 4;
                    byte hpLowByte = br.ReadByte();
                    byte dataHpHighSpLow = br.ReadByte();
                    byte spHighByte = br.ReadByte();
                    byte hpHighByte = (byte)((dataHpHighSpLow >> 0) & 0xF);
                    byte spLowByte = (byte)((dataHpHighSpLow >> 4) & 0xF);
                    personas[i].BonusHP = BitConverter.ToInt16(new[] { hpLowByte, hpHighByte }, 0);
                    personas[i].BonusSP = (short)((spHighByte << 4) | spLowByte);
                    ms.Position += 0x15;
                }

                return personas;
            }
        }

        private Character[] LoadCharacterData()
        {
            var characters = new List<Character>(Lists.Characters.Length);

            using (var ms = new MemoryStream(si.SaveData))
            using (var br = new BinaryReader(ms))
            {
                ms.Position = 0x7F56;

                for (int i = 0; i < Lists.Characters.Length; i++)
                {
                    ms.Position += 0x10;
                    short weaponID = br.ReadInt16();
                    var equippedWeapon = Lists.GetItemData(weaponID);

                    ms.Position += 0x16;
                    short armourID = br.ReadInt16();
                    var equippedArmour = Lists.GetItemData(armourID);

                    ms.Position += 0x16;
                    int accessoryID = br.ReadInt16();
                    var equippedAccessory = Lists.GetItemData(accessoryID);

                    var character = new Character((short)i);

                    if (i == 0 || i == 1 || i == 18 || i == 19)
                    {
                        long originalPosition = ms.Position;
                        if (i == 0)
                            ms.Position = 0x79A2;
                        if (i == 1)
                            ms.Position = 0x7992;
                        if (i == 18)
                            ms.Position = 0x79B2;
                        if (i == 19)
                            ms.Position = 0x79C2;
                        string lastName = Encoding.UTF8.GetString(br.ReadBytes(8)).Replace("\0", "");
                        string name = Encoding.UTF8.GetString(br.ReadBytes(8)).Replace("\0", "");
                        Lists.Characters[i] = name + " " + lastName;
                        ms.Position = originalPosition;
                        character.BaseName = name;
                        character.LastName = lastName;
                    }

                    if (equippedWeapon != null)
                        character.EquippedWeapon = new Item(weaponID, equippedWeapon.Item1, equippedWeapon.Item2);

                    if (equippedArmour != null)
                        character.EquippedArmour = new Item(armourID, equippedArmour.Item1, equippedArmour.Item2);

                    if (equippedAccessory != null)
                        character.EquippedAccessory = new Item(accessoryID, equippedAccessory.Item1, equippedAccessory.Item2);
                    
                    ms.Position += 0x30;
                    var persona = new Persona();
                    byte[] data = br.ReadBytes(2);
                    byte lowByte = (byte)(data[1] & 0x0F);
                    persona.Id = BitConverter.ToInt16(new[] { data[0], (byte)(lowByte % 2) }, 0);
                    character.Level = lowByte % 2 == 0 ? data[1] / 2 : (data[1] - 1) / 2;
                    character.Persona = persona;
                    character.Exp = br.ReadInt32();

                    character.Persona.Skills = new Skill[6];
                    for (int j = 0; j < character.Persona.Skills.Length; j++)
                    {
                        character.Persona.Skills[j] = new Skill();
                        character.Persona.Skills[j].ID = br.ReadInt16();
                    }

                    ms.Position += 0x4;
                    data = br.ReadBytes(3);
                    byte highByte = (byte)((data[1] & 0xF0) >> 4);
                    lowByte = (byte)(data[1] & 0x0F);
                    character.MaxHP = BitConverter.ToInt16(new[] { data[0], lowByte }, 0);
                    character.MaxSP = BitConverter.ToInt16(new[] { (byte)(((data[2] & 0x0F) << 4) + highByte), (byte)((data[2] & 0xF0) >> 4) }, 0);
                    character.St = br.ReadByte();
                    character.Ma = br.ReadByte();
                    character.En = br.ReadByte();
                    character.Ag = br.ReadByte();
                    character.Lu = br.ReadByte();

                    ms.Position += 0x10;
                    ushort subPersonaIndex = br.ReadUInt16();
                    if (subPersonaIndex != ushort.MaxValue)
                    {
                        character.SubPersona = si.SummonedPersonas[subPersonaIndex];
                        character.SubPersonaEquipped = true;
                    }
                    else
                        character.SubPersonaEquipped = false;

                    ms.Position += 0x2;
                    character.CurrentHP = br.ReadInt32();
                    character.CurrentSP = br.ReadInt32();

                    characters.Add(character);
                }
            }

            Character[] characs = new Character[characters.Count];

            characters.CopyTo(characs);
            characters.Sort((x, y) => string.Compare(x.Name, y.Name, true, CultureInfo.InvariantCulture));
            si.SortedCharacters = characters.ToArray();

            return characs;
        }

        private void LoadGeneralData()
        {
            using (var ms = new MemoryStream(si.SaveData))
            using (var br = new BinaryReader(ms))
            {
                ms.Position = 0x743A;
                si.Enepedia = br.ReadBytes(Lists.Enepedia.Length);

                ms.Position = 0x7986;
                si.Yen = br.ReadInt32();

                ms.Position = 0x798E;
                si.TeamGauge = br.ReadInt16();
            }

            characterCb.SelectedIndex = 0;
        }

        private Item[] LoadInventoryData()
        {
            var inventory = new Item[60]; // Inventory size in-game

            using (var ms = new MemoryStream(si.SaveData))
            using (var br = new BinaryReader(ms))
            {
                ms.Position = 0x9236;

                for (int i = 0; i < inventory.Length; i++)
                {
                    short id = br.ReadInt16();

                    var itemData = Lists.GetItemData(id);

                    inventory[i] = itemData != null ? new Item(id, itemData.Item1, itemData.Item2) : new Item(id, Properties.Resources.Unknown, string.Empty);

                    ms.Position += 0x16;
                }
            }

            return inventory;
        }

        [Conditional("DEBUG")]
        private void QuickFix()
        {
            DebugLoad();

            Save_Executed(null, null);

            Application.Current.Shutdown();
        }

        private void Save_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = si.SaveLoaded;
        }

        private void Save_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            SaveGeneralData();
            SaveSummonedPersonasData();
            SaveCharacterData();
            SaveInventoryData();

            FixChecksum(si.SaveData, 0xC, 0x8C);
            FixChecksum(si.SaveData, 0x8, 0x8C);
            FixChecksum(si.SaveData, 0x90, si.SaveData.Length);

            File.WriteAllBytes(si.SaveFile, si.SaveData);

            #if !DEBUG
            DialogManager.ShowModalMessageExternal(this, Properties.Resources.Title, Properties.Resources.Saved);
            #endif
        }

        private void SaveCharacterData()
        {
            using (var ms = new MemoryStream(si.SaveData))
            using (var bw = new BinaryWriter(ms))
            {
                ms.Position = 0x7F56;
                for (int i = 0; i < si.Characters.Length; i++)
                {
                    var character = si.Characters[i];

                    ms.Position += 0x10;
                    if (character.EquippedWeapon != null)
                        bw.Write(character.EquippedWeapon.ID);
                    else
                        ms.Position += sizeof(int);

                    ms.Position += 0x14;
                    if (character.EquippedArmour != null)
                        bw.Write(character.EquippedArmour.ID);
                    else
                        ms.Position += sizeof(int);

                    ms.Position += 0x14;
                    if (character.EquippedAccessory != null)
                        bw.Write(character.EquippedAccessory.ID);
                    else
                        ms.Position += sizeof(int);

                    ms.Position += 0x2E;

                    byte personaIdLowByte = (byte)(character.Persona.Id & 0x00FF);
                    byte personaIdHighByte = (byte)((character.Persona.Id & 0xFF00) >> 8);
                    byte characterLevel = (byte)(character.Level * 2);
                    bw.Write(new byte[] { personaIdLowByte, (byte)(personaIdHighByte + characterLevel) });

                    bw.Write(character.Exp);

                    for (int x = 0; x < character.Persona.Skills.Length; x++)
                        bw.Write(character.Persona.Skills[x].ID);

                    ms.Position += 0x4;
                    byte maxHpLowByte = (byte)(character.MaxHP & 0x00FF);
                    byte maxHpHighByte = (byte)((character.MaxHP & 0xFF00) >> 8);
                    byte maxSpLowByte = (byte)((character.MaxSP & 0x000F) << 4);
                    byte maxSpHighByte = (byte)((character.MaxSP & 0xFFF0) >> 4);
                    bw.Write(new byte[] { maxHpLowByte, (byte)(maxHpHighByte + maxSpLowByte), maxSpHighByte });

                    bw.Write(character.St);
                    bw.Write(character.Ma);
                    bw.Write(character.En);
                    bw.Write(character.Ag);
                    bw.Write(character.Lu);

                    ms.Position += 0x14;
                    bw.Write(character.CurrentHP);
                    bw.Write(character.CurrentSP);
                }
            }
        }

        private void SaveInventoryData()
        {
            using (var ms = new MemoryStream(si.SaveData))
            using (var bw = new BinaryWriter(ms))
            {
                ms.Position = 0x9236;

                for (int i = 0; i < si.Inventory.Length; i++)
                {
                    Item item = si.Inventory[i];
                    bw.Write((short)item.ID);

                    ms.Position += 0x16;
                }
            }
        }

        private void SaveGeneralData()
        {
            using (var ms = new MemoryStream(si.SaveData))
            using (var bw = new BinaryWriter(ms))
            {
                ms.Position = 0x743A;
                bw.Write(si.Enepedia);

                ms.Position = 0x7986;
                bw.Write(si.Yen);

                ms.Position = 0x798E;
                bw.Write(si.TeamGauge);

                ms.Position = 0x79A2;
                byte[] lastName = Encoding.UTF8.GetBytes(si.Characters[0].LastName);
                byte[] baseName = Encoding.UTF8.GetBytes(si.Characters[0].BaseName);
                int fillLastName = 8 - lastName.Length;
                int fillName = 8 - baseName.Length;
                bw.Write(lastName);
                bw.Write(new byte[fillLastName]);
                bw.Write(baseName);
                bw.Write(new byte[fillName]);

                ms.Position = 0x7992;
                lastName = Encoding.UTF8.GetBytes(si.Characters[1].LastName);
                baseName = Encoding.UTF8.GetBytes(si.Characters[1].BaseName);
                fillLastName = 8 - lastName.Length;
                fillName = 8 - baseName.Length;
                bw.Write(lastName);
                bw.Write(new byte[fillLastName]);
                bw.Write(baseName);
                bw.Write(new byte[fillName]);

                ms.Position = 0x79B2;
                lastName = Encoding.UTF8.GetBytes(si.Characters[18].LastName);
                baseName = Encoding.UTF8.GetBytes(si.Characters[18].BaseName);
                fillLastName = 8 - lastName.Length;
                fillName = 8 - baseName.Length;
                bw.Write(lastName);
                bw.Write(new byte[fillLastName]);
                bw.Write(baseName);
                bw.Write(new byte[fillName]);

                ms.Position = 0x79C2;
                lastName = Encoding.UTF8.GetBytes(si.Characters[19].LastName);
                baseName = Encoding.UTF8.GetBytes(si.Characters[19].BaseName);
                fillLastName = 8 - lastName.Length;
                fillName = 8 - baseName.Length;
                bw.Write(lastName);
                bw.Write(new byte[fillLastName]);
                bw.Write(baseName);
                bw.Write(new byte[fillName]);
            }
        }

        private void SaveSummonedPersonasData()
        {
            using (var ms = new MemoryStream(si.SaveData))
            using (var bw = new BinaryWriter(ms))
            {
                ms.Position = 0x79D2;
                for (int i = 0; i < si.SummonedPersonas.Length; i++)
                {
                    var persona = si.SummonedPersonas[i];

                    bw.Write(persona.Active);

                    byte high = (byte)((persona.Id >> 0) & 0xFF);
                    byte low = (byte)((persona.Id >> 8) & 0xF);
                    byte levelId = (byte)(low % 2 == 0 ? persona.Level * 2 : persona.Level * 2 + 1);
                    bw.Write(high);
                    bw.Write(levelId);
                    
                    bw.Write(persona.Exp);

                    for (int x = 0; x < persona.Skills.Length; x++)
                        bw.Write(persona.Skills[x].ID);

                    ms.Position += 4;
                    byte lowHp = (byte)((persona.BonusHP >> 0) & 0xFF);
                    byte highHpLowSp = (byte)(((persona.BonusSP >> 0) & 0xF) << 4 | ((persona.BonusHP >> 8) & 0xF));
                    byte highSp = (byte)((persona.BonusSP >> 4) & 0xFF);
                    byte[] bonusHpSp = { lowHp, highHpLowSp, highSp };
                    bw.Write(bonusHpSp);

                    ms.Position += 0x15;
                }
            }
        }

        private void Inventory_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (!(e.OriginalSource is TabControl))
                return;

            switch ((sender as TabControl).SelectedIndex)
            {
                case 0:
                    UpdateItemList((from x in Lists.Consumables
                        select x).ToArray());
                    break;

                case 1:
                    UpdateItemList((from x in Lists.Accessories
                        select x).ToArray());
                    break;

                default:
                    return;
            }
        }

        internal void UpdateItemList(KeyValuePair<int, Tuple<string, string>>[] itemData)
        {
            var items = new Item[itemData.Length];

            for (int i = 0; i < items.Length; i++)
                items[i] = new Item(itemData[i].Key, itemData[i].Value.Item1, itemData[i].Value.Item2);

            Item.List = items.OrderBy(x => x.ID != 0) // Set EMPTY as the first entry
                .ThenBy(x => x.Name).ToArray(); // Then sort the others
        }

        private void Armours_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            KeyValuePair<int, Tuple<string, string>>[] itemData = null;

            switch ((sender as TabControl).SelectedIndex)
            {
                case 0:
                    itemData = (from x in Lists.ArmourAigis
                        select x).ToArray();
                    break;

                case 1:
                    itemData = (from x in Lists.ArmourFemales
                        select x).ToArray();
                    break;

                case 2:
                    itemData = (from x in Lists.ArmourKoromaru
                        select x).ToArray();
                    break;

                case 3:
                    itemData = (from x in Lists.ArmourMales
                        select x).ToArray();
                    break;

                case 4:
                    itemData = (from x in Lists.ArmourMorgana
                        select x).ToArray();
                    break;

                case 5:
                    itemData = (from x in Lists.ArmourUnisex
                        select x).ToArray();
                    break;
            }

            UpdateItemList(itemData);

            e.Handled = true;
        }

        private void Weapons_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            KeyValuePair<int, Tuple<string, string>>[] itemData = null;

            switch ((sender as TabControl).SelectedIndex)
            {
                case 0:
                    itemData = (from x in Lists.WeaponsAigis
                                select x).ToArray();
                    break;

                case 1:
                    itemData = (from x in Lists.WeaponsAkihikoMakoto
                                select x).ToArray();
                    break;

                case 2:
                    itemData = (from x in Lists.WeaponsAnn
                        select x).ToArray();
                    break;

                case 3:
                    itemData = (from x in Lists.WeaponsChie
                                select x).ToArray();
                    break;

                case 4:
                    itemData = (from x in Lists.WeaponsAkechi
                        select x).ToArray();
                    break;

                case 5:
                    itemData = (from x in Lists.WeaponsHaru
                        select x).ToArray();
                    break;

                case 6:
                    itemData = (from x in Lists.WeaponsP4Junpei
                        select x).ToArray();
                    break;

                case 7:
                    itemData = (from x in Lists.WeaponsKanji
                                select x).ToArray();
                    break;

                case 8:
                    itemData = (from x in Lists.WeaponsKen
                                select x).ToArray();
                    break;

                case 9:
                    itemData = (from x in Lists.WeaponsYosukeKoromaru
                        select x).ToArray();
                    break;
                    
                case 10:
                    itemData = (from x in Lists.WeaponsMitsuru
                                select x).ToArray();
                    break;

                case 11:
                    itemData = (from x in Lists.WeaponsMorgana
                                select x).ToArray();
                    break;

                case 12:
                    itemData = (from x in Lists.WeaponsNaoto
                                select x).ToArray();
                    break;

                case 13:
                    itemData = (from x in Lists.WeaponsP3F
                                select x).ToArray();
                    break;

                case 14:
                    itemData = (from x in Lists.WeaponsP3M
                        select x).ToArray();
                    break;

                case 15:
                    itemData = (from x in Lists.WeaponsP5
                        select x).ToArray();
                    break;

                case 16:
                    itemData = (from x in Lists.WeaponsRyuji
                        select x).ToArray();
                    break;

                case 17:
                    itemData = (from x in Lists.WeaponsShinjiro
                                select x).ToArray();
                    break;

                case 18:
                    itemData = (from x in Lists.WeaponsTeddie
                                select x).ToArray();
                    break;

                case 19:
                    itemData = (from x in Lists.WeaponsYukari
                                select x).ToArray();
                    break;

                case 20:
                    itemData = (from x in Lists.WeaponsYukiko
                                select x).ToArray();
                    break;

                case 21:
                    itemData = (from x in Lists.WeaponsYusuke
                                select x).ToArray();
                    break;
            }

            UpdateItemList(itemData);

            e.Handled = true;
        }
    }
}